<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeGiftAnswerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_gift_answer_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('free_gift_answer_id', false, true);
            $table->integer('choice_id', false, true);
            $table->timestamps();

            $table->foreign('free_gift_answer_id')->references('id')->on('free_gift_answers');
            $table->foreign('choice_id')->references('id')->on('choises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_gift_answer_details');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',64);
            $table->string('name',128);
            $table->string('phone',32);
            $table->string('email',128)->nullable();
            $table->string('password',256);
            $table->text('address')->nullable();
            $table->string('status',32)->comment('active,pending,disabled');
            $table->string('country',16)->default('ID')->comment('ID,MY');
            $table->double('balance',10,2);
            $table->string('referral_code',16);
            $table->string('id_type',16)->nullable()->comment('ktp,sim');
            $table->string('id_number',128)->nullable();
            $table->string('id_image',256)->nullable();
            $table->date('id_submit_date')->nullable();
            $table->date('id_verified_date')->nullable();
            $table->string('id_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

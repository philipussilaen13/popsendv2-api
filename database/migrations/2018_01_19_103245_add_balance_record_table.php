<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBalanceRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,true);
            $table->integer('transaction_id',false,true);
            $table->double('credit',8,2)->default(0)->comment('Amount of Ingoing/Credit');
            $table->double('debit',8,2)->default(0)->comment('Amount of Outgoing/Debit');
            $table->double('previous_balance',8,2)->comment('Previous Balance before transaction occurred');
            $table->double('current_balance',8,2)->comment('Current Balance after transaction');
            $table->date('date');
            $table->time('time');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_records');
    }
}

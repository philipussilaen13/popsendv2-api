<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTableTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->float('total_price',8,2)->default(0)->change();
            $table->float('paid_amount',8,2)->default(0)->change();
            $table->float('promo_amount',8,2)->default(0)->change();
            $table->float('refund_amount',8,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('total_price',45)->nullable()->change();
            $table->string('paid_amount',45)->nullable()->change();
            $table->string('promo_amount',45)->nullable()->change();
            $table->string('refund_amount',45)->nullable()->change();
        });
    }
}

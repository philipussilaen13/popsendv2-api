<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCancleOnPopsafe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->boolean('is_cancel')->default(true)->nullable()->after('status');
            $table->boolean('is_expired')->default(true)->nullable()->after('is_cancel');
            $table->boolean('is_collect')->default(false)->nullable()->after('is_expired');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dropColumn('is_cancel');
            $table->dropColumn('is_expired');
            $table->dropColumn('is_collect');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnItemCategoryDescriptionPopsafe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable()->after('total_price');
            $table->string('description',512)->nullable()->after('category_id');

            $table->foreign('category_id')->references('id')->on('category_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('popsafes', function (Blueprint $table) {
            $table->dropColumn('category_id');
            $table->dropColumn('description');
        });
    }
}

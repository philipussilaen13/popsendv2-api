<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDestinationHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_destination_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_destination_id',false,true);
            $table->string('user',128)->nullable();
            $table->string('status',64);
            $table->text('remarks')->nullable();
            $table->timestamps();

            $table->foreign('delivery_destination_id')->references('id')->on('delivery_destinations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_destination_histories');
    }
}

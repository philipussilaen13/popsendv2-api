<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceToDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->float('total_price',8,2)->after('notes');
        });
        Schema::table('delivery_destinations', function (Blueprint $table) {
            $table->float('price',8,2)->after('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('total_price');
        });

        Schema::table('delivery_destinations', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}

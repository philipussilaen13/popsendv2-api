<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->string('url')->nullable();
            $table->enum('promo_type',['potongan','deposit'])->default('potongan');
            $table->enum('status', [1, 0])->default(0);
            $table->enum('type', ['fixed', 'percent'])->default('percent');
            $table->enum('category', ['fixed', 'cutback'])->default('fixed');
            $table->enum('voucher_required', [1, 0])->default(0);
            $table->enum('voucher_type', ['member', 'all', 'voucher'])->default('all')->nullable();
            $table->integer('amount');
            $table->integer('max_amount')->nullable();
            $table->integer('min_amount')->nullable();
            $table->integer('limit_usage')->nullable();
            $table->integer('priority')->nullable();
            $table->integer('created_user')->nullable();
            $table->integer('updated_user')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}

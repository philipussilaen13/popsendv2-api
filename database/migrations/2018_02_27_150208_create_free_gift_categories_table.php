<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeGiftCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_gift_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 225);
            $table->string('category_code', 225)->unique();
            $table->string('icon', 225);
            $table->string('avaliable_country', 225);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_gift_categories');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_usages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id', false, true);
            $table->integer('voucher_id', false, true);
            $table->integer('user_id', false, true)->nullable();
            $table->integer('transaction_id', false, true);
            $table->integer('transaction_amount');
            $table->integer('disc_amount');
            $table->integer('final_amount');
            $table->enum('status', [1, 0])->default(0)->nullable();
            $table->dateTime('time_limit');
            $table->timestamps();

            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_usages');
    }
}

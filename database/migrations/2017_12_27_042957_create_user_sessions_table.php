<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('api_token_id',false,true);
            $table->integer('user_id',false,true);
            $table->string('session_id',128);
            $table->string('ip_address',32)->nullable();
            $table->bigInteger('last_activity')->comment('Number of Second since epoch');
            $table->timestamps();

            $table->foreign('api_token_id')->references('id')->on('api_tokens');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sessions');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnLengthForPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balance_records', function (Blueprint $table) {
            $table->decimal('credit',16,2)->default(0)->change();
            $table->decimal('debit',16,2)->default(0)->change();
            $table->decimal('previous_balance',16,2)->change();
            $table->decimal('current_balance',16,2)->change();
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('total_price',16,2)->default(0)->change();
            $table->decimal('paid_amount',16,2)->default(0)->change();
            $table->decimal('promo_amount',16,2)->default(0)->change();
            $table->decimal('refund_amount',16,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance_records', function (Blueprint $table) {
            $table->decimal('credit',8,2)->default(0)->change();
            $table->decimal('debit',8,2)->default(0)->change();
            $table->decimal('previous_balance',8,2)->change();
            $table->decimal('current_balance',8,2)->change();
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('total_price',8,2)->default(0)->change();
            $table->decimal('paid_amount',8,2)->default(0)->change();
            $table->decimal('promo_amount',8,2)->default(0)->change();
            $table->decimal('refund_amount',8,2)->default(0)->change();
        });
    }
}

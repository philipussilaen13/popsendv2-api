<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeResponseColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->string('response',1024)->nullable()->change();
        });

        Schema::table('logs', function (Blueprint $table) {
            $table->longText('response')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->string('response',1024)->nullable()->change();
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->text('response')->nullable()->change();
        });
    }
}

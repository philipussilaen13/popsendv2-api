<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id');
            $table->string('user',256)->nullable();
            $table->text('description')->nullable();
            $table->decimal('total_price',12,2)->default(0);
            $table->string('status',32)->nullable();
            $table->decimal('paid_amount',12,2)->default(0);
            $table->decimal('promo_amount',12,2)->default(0);
            $table->decimal('refund_amount',12,2)->default(0);
            $table->text('remarks');
            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_histories');
    }
}

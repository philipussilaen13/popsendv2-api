<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('modules',256);
            $table->integer('user_id',false,true)->nullable();
            $table->integer('api_token_id',false,true);
            $table->string('session_id',128)->nullable();
            $table->text('request');
            $table->text('response')->nullable();
            $table->double('latency',8,2);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('api_token_id')->references('id')->on('api_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}

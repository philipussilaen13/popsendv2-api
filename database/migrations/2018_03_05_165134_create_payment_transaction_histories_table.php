<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transaction_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_transaction_id', false, true);
            $table->double('total_amount')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

            $table->foreign('payment_transaction_id')->references('id')->on('payment_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transaction_histories');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopsafeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popsafe_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('popsafe_id',false,true);
            $table->string('user',128)->nullable();
            $table->string('status',64);
            $table->text('remarks')->nullable();
            $table->timestamps();

            $table->foreign('popsafe_id')->references('id')->on('popsafes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popsafe_histories');
    }
}

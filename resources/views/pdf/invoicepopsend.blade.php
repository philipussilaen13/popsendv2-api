<style media="screen">

@font-face {
        font-family: DidactGothic;
        src: url("{{ storage_path('fonts/DidactGothic-Regular.ttf') }}");
    }
    html, body {
        height: 100%;
        font-family: DidactGothic;
        font-size: 12px;
    }
    div.notes{
        padding: 0px;
        height: 40px;
        overflow:hidden;
    }
    div.name{
        padding: 0px;
        height: 20px;
        overflow: hidden;
    }
    div.phone{
        padding: 0px;
        height: 20px;
        overflow: hidden;
    }

    div.address{
        padding: 0px;
        height: 80px;
        overflow: hidden;
    }
    .right {
        position: absolute;
        right: 0px;
    }
    img.c {
        vertical-align: text-bottom;
    }
    .border-left{
        border-width: 1px;
        border-top: solid;
        border-right: solid;
        border-bottom: solid;
        border-color: #F2F2F2;
    }
    .border-right{
        border-width: 1px;
        border-top: solid;
        border-left: solid;
        border-bottom: solid;
        border-color: #F2F2F2;
    }
    title{
        color:black;
        margin:25px;
    }
    .page-break {
        page-break-after: avoid;
        page-break-inside: avoid;
    }
    label{
        color:dimgrey;
        font-family: DidactGothic;
        font-size: 12px;
    }
    label.order{
        color:black;
        font-family: DidactGothic;
        font-size: 16px;
        margin-right: 20px;
        padding: 10px;
    }
    .spacing{
        margin-bottom: 10px;
    }
    .note {
        background: #f2f2f2;
        border: 2px solid #cbcbcb;
        border-style: dashed;
        width: 100%;
        padding: 5px;
        height: 40px;
        overflow:hidden;
    }
    .address {
        width: 100%;
        padding: 10px;
    }
    .caution {
        background: #f2f2f2;
        width: 100%;
    }
    .footer {
        width: 100%;
        padding: 10px;
        text-align: center;
    }
    .drop {
        width: auto;
        background: #ef617a;
        color: #ffffff;
        padding: 5px;
    }
    .header {
        background: #ef617a;
        padding: 10px;
    }
    .font12 {
        font-size: 12px;
    }
    .font10 {
        font-size: 10px;
    }
    .color-gray {
        color: dimgrey;
    }
    .master-invoice {
        color: #ffffff;
        padding-left: 1em;
        font-size: 11px;
    }
    .color-pink-invoice {
        color: #ef617a;
        font-weight: bold;
        font-size: 18px;
    }
    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
    .scissors{
        position: absolute;
        top:76px;
    }
    .cs{
        color:#EF617A;
    }
    span {
        font-family: DidactGothic;
    }
    @page { 
        size:auto; 
        margin:0px; 
    }
</style>

@if(is_array($data->destinations))
    @php
    $number = 1;
    $total = count($data->destinations);
    @endphp
    @foreach($data->destinations as $destination)
        <table cellspacing="0" cellpadding="10" width="100%">
            <tr class="header">
                <td align="left">
                    <img src="{{ url('/images/invoice/logo-popsend.png') }}" alt="Logo for PopSend"><br>
                    <span class="master-invoice">{{$data->invoice_id}} ({{ $number.'/'.$total }})</span>
                </td>
                <td align="right">
                    <img src="{{ url('/images/invoice/logo-popbox.png') }}" alt="Logo for Popbox"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table cellspacing="0" cellpadding="10" width="100%">
                        <tr>
                            <td width="75%" valign="top">
                                <label>ORDER DATE</label>
                                    <div style="margin-bottom:5px">{{date('l, d M Y H:i A', strtotime($data->pickup_date))}}</div>
                                <label>NOTE</label>
                                    <div style="margin: 5px 0px 10px" class="note">
                                <!-- Notes -->
                                    @if(isset($data->notes))
                                        @if( strlen($data->notes) <= 78 )
                                            <div>{{ $data->notes }}</div>
                                        @else
                                            <div>
                                                @php
                                                    $str = substr( $data->notes, 0, 77);
                                                    echo $str."...";
                                                @endphp
                                            </div>
                                        @endif
                                    @endif
                                <!-- Item Description -->
                                <div>
                                    Item Description:
                                    @if(isset($destination->notes))
                                        @if( strlen($destination->notes) < 65 )
                                            {{ $destination->notes }}
                                        @else
                                            @php
                                                $str = substr( $destination->notes, 0, 60);
                                                echo $str."...";
                                            @endphp
                                        @endif
                                    @endif
                                        </div>
                                </div>
                                @if( $data->pickup_type == "locker")
                                <label>DROP TIME EXPIRY</label><br>
                                    <div style="margin: 5px 0px; display:inline-block; font-weight:bold; font-size:16px" class="drop">
                                        {{date('l, d M Y H:i A', strtotime($data->pickup_date. ' + 3 days'))}}
                                    </div><br>
                                <span class="font12 color-gray">Please drop your parcel before the drop time expires.</span>
                                @endif
                            </td>
                            <td width="25%" align="center" valign="top">
                                <label>ORDER NUMBER</label><br>
                                <span class="color-pink-invoice">{{$destination->sub_invoice_id}}</span><br>
                                <img src="data:{{$destination->getContentType}};base64,{{$destination->QRcode}}" width="160" height="160">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="10" width="100%" style="min-height: 100px;">
            <tr>
                <!--========================================== ORIGIN=============================================-->
                <td width="50%" valign="top" class="border-left">
                    <div>
                        <!-- LOCKER -->
                        @if( $data->pickup_type == "locker")
                        <img class="c" src="{{ url('/images/invoice/icon-boxgreen.png') }}" alt="icon for box">
                        <label class="order">ORIGIN</label>
                        <span style="background-color:#39B54A; color:white; padding: 10px;">LOCKER</span>
                        @elseif( $data->pickup_type == "address")
                        <!-- ADDRESS  -->
                        <img class="c" src="{{ url('/images/invoice/icon-mapgreen.png') }}" alt="icon for box">
                        <label class="order">ORIGIN</label>
                        <span style="background-color:#39B54A; color:white; padding: 10px;">ADDRESS</span>
                        @endif
                    </div><br>
                    
                    <!-- INFO -->
                    <div style="padding-left: 10px;">
                        <label>SENDER NAME</label>
                            <div class="spacing name">{{ $data->sender_name }}</div>
                        <label>PHONE</label>
                            <div class="spacing phone">
                            <!-- dash line -->
                            @php
                                $arr = str_split($data->sender_phone);
                                for($i=0,$j=1; $i < count($arr); $i++,$j++){
                                    if($j%4==0 && $i!=count($arr)-1){
                                        echo $arr[$i]."-";
                                        $j=0;
                                    }
                                    else{
                                        echo $arr[$i];
                                    }
                                }
                            @endphp
                            </div>
                        <label>ADDRESS</label>
                            <div class="spacing address">
                            @if( strlen($data->pickup_address) <= 200 )
                                <div>{{ $data->pickup_address }}</div>
                            @else
                                <div>
                                    @php
                                        $str = substr( $data->pickup_address, 0, 199);
                                        echo $str."...";
                                    @endphp`
                                </div>
                            @endif
                            </div> 
                        <label>NOTE</label>
                            <div class="notes" style="color:#EF617A" >{{ $data->detail }}</div>
                    </div>
                </td>
                <!-- ======================================== DESTINATION ======================================= -->
                <td width="50%" valign="top" class="border-right">
                    <div>
                        <!-- ADDRESS -->
                        @if( $destination->destination_type == "address" )
                        <img class="c" src="{{ url('/images/invoice/icon-mapred.png') }}" alt="icon for box">
                        <label class="order">DESTINATION</label>
                        <span style="background-color:#EF617A; color:white; padding: 10px;">ADDRESS</span>
                        <!-- LOCKER -->
                        @elseif( $destination->destination_type == "locker" )
                        <img class="c" src="{{ url('/images/invoice/icon-boxred.png') }}" alt="icon for box">
                        <label class="order">DESTINATION</label>
                        <span style="background-color:#EF617A; color:white; padding: 10px;">LOCKER</span>
                        @endif
                    </div><br>

                    <!-- INFO -->
                    <div style="padding-left: 10px;">
                        <label>SENDER NAME</label>
                            <div class="spacing name">{{ $destination->name }}</div>
                        <label>PHONE</label>
                            <div class="spacing phone">
                            <!-- dash line -->
                            @php
                                $arr = str_split($destination->phone);
                                for($i=0,$j=1; $i < count($arr); $i++,$j++){
                                    if($j%4==0 && $i!=count($arr)-1){
                                        echo $arr[$i]."-";
                                        $j=0;
                                    }
                                    else{
                                        echo $arr[$i];
                                    }
                                }
                            @endphp
                             </div>
                        <label>ADDRESS</label>
                            <div class="spacing address">
                            @if( strlen( $destination->destination_address ) <= 200 )  
                                <div>{{ $destination->destination_address }}</div>
                            @else
                                <div>
                                    @php
                                        $str = substr( $destination->destination_address, 0, 199);
                                        echo $str."...";
                                    @endphp
                                </div>
                            @endif
                            </div>
                        <label>NOTE</label>
                            <div class="notes" style="color:#EF617A">{{ $destination->detail }}</div>
                    </div>
                </td>
            </tr>
        </table>
        <table cellspacing="0"  width="100%" style="background-color:#F2F2F2; border-bottom:2px dashed dimgrey;"> 
            <tr class="caution"> 
                <td width="10%" style="padding: 20px" >
                    <img src="{{ url('/images/invoice/icon-warning.png') }}" alt="insert image here">
                </td>
                <td width="90%" align="left" style="padding: 10px">
                    <div style="color:#EF617A;">
                        Applying Shipment Label 
                    </div>
                    <div style="color:dimgrey; font-size:12px">
                        Print and paste this invoice to your parcel. 
                        Displaying the shipment labels clearly is important in ensuring that your shipment moves swiftly through PopBox's network.
                        Fix the shipment label securely on the top surface of the shipment box.
                    </div>
                </td>
                <td>
                    <img class= "scissors" src="{{ url('/images/invoice/icon-scissors.png') }}" alt="insert image here">
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="10" width="100%">
            <tr>
                @if($data->pickup_type == 'locker')
                <td colspan="3" align="center" class="footer">
                    <table width="100%"> 
                        <tr>
                            <th colspan="3" style = "text-align:center; font-size:16px">HOW TO DROP LOCKER</th>
                        </tr>
                        <tr>
                            <td width="33.33%" valign="top" style="font-size:12px; text-align:center;">
                                <img src="{{ url('/images/invoice/howto-locker01.png') }}" alt="icon for box"><br>
                                Print invoice and stick it to Parcel
                            </td>
                            <td width="33.33%" valign="top" style="font-size:12px; text-align:center;">
                                <img src="{{ url('/images/invoice/howto-locker02.png') }}" alt="icon for box"><br>
                                Open "PARCEL DELIVERY" menu<br>of PopBox Locker
                            </td>
                            <td width="33.33%" valign="top" style="font-size:12px; text-align:center;">
                                <img src="{{ url('/images/invoice/howto-locker03.png') }}" alt="icon for box"><br>
                                Scan the QR order number
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" class="cs">
                                <div class="cs">Need help? CS:+62 21 2253 8719 | email:info@popbox.asia |  Live Chat PopBox App | Line: @popboxasia</div>
                            </td>
                        </tr>
                    </table>
                </td>
                @else
                <td colspan="3" class="footer">
                    <table width="100%"> 
                        <tr>
                            <th colspan="3" style = "text-align:center; font-size:16px">HOW TO PREPARE YOUR PARCEL</th>
                        </tr>
                        <tr>   
                            <td width="47.5%" style="font-size:12px;" valign="top" align="right">
                                <img src="{{ url('/images/invoice/howto-address01.png') }}" alt="icon for box"><br>
                                <span>Print invoice and stick it to Parcel</span>
                            </td>
                            <td width="5%"style="font-size:10px;"></td>
                            <td width="47.5%" style="font-size:12px;" valign="top" align="left">
                                <img src="{{ url('/images/invoice/howto-address02.png') }}" alt="icon for box"><br>
                                <span style="display:block; float:left; text-align:center;">Prepare your parcel and our courier<br>will pick it up</span>
                            </td>
                        </tr>
                    </table>
                </td><br>
                <tr>
                    <td colspan="3" align="center" class="cs">
                        Need help? CS:+62 21 2253 8719 | email:info@popbox.asia |  Live Chat PopBox App | Line: @popboxasia
                    </td>
                </tr>
                @endif
            </tr>
        </table>
        <div class="page-break"></div>
        @php
            $number++;
        @endphp
    @endforeach
@endif
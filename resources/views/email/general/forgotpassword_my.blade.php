<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password</title>

    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 2px;
        }
    </style>
</head>
<body>

<p>Hai {{$name}}</p>
<p>
    You have made a request for a password change on account {{$phone}},

    here is your Unique Code
</p>

<h3>{{ $code }}</h3>

<p>
    Input code above to change your password
</p>

<p>If you have any difficulties please contact info@popbox.asia or +60 0111 0606 011 </p>

<br><br>
<p>
    Warm regards, <br>
    <br>
    PopBox Asia <br>
    +60 0111 0606 011<br>
    +62 21 5329464<br>
    info@popbox.asia<br>
    www.popbox.asia <br>
    <a href="www.popbox.asia">www.popbox.asia</a>
</p>


</body>
</html>
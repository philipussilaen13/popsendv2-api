<html>
<body>
<h2>Failed Push Express for Sub Invoice {{ $subInvoiceCode }}</h2>

Parameter : {{ json_encode($parameter) }}
<hr>
<table>
    @foreach($parameter as $index => $value)
        @if(is_array($value))
            @php
                $data = $parameter['data'][0];
            @endphp
            @foreach($data as $dataIndex => $dataItem)
                <tr>
                    <td>{{ $dataIndex }}</td>
                    <td>
                        @if (!is_string($dataItem))
                            {{ json_encode($dataItem) }}
                        @else
                            {{ $dataItem }}
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td><strong>{{ $index }}</strong></td>
                <td>{{ $value }}</td>
            </tr>
        @endif

    @endforeach
</table>
<p>Error Message</p>
<p>{{ $errorMsg }}</p>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div style="font-family:'San Francisco',Arial,Helvetica,san-serif,serif;font-size:14px;margin:0px;box-sizing:border-box;color:black">
    <table style="width:100%;height:100%;max-width:650px;border-spacing:0px;border-collapse:collapse;margin:0px auto;background:rgb(242,242,242) none repeat scroll 0% 0%" align="center">
        <tbody>
        <tr>
            <td style="padding:20px">
                <table style="width:100%;height:100%;max-width:600px;border-spacing:0px;border-collapse:collapse;border-color:rgb(230,231,235);border-style:solid;border-width:1px;margin:0px auto" align="center">
                    <tbody>
                    <tr>
                        <td>
                            <table style="border-spacing:0px;border-collapse:collapse;width:100%" width="100%" align="center">
                                <thead>
                                <tr bgcolor="#EF4951">
                                    <th width="90%">
                                        <h2 style="margin:0px;padding:20px;text-align:left">
                                            <img src="https://gallery.mailchimp.com/423156a45dfd7e41866942f5e/images/87a75b9f-3164-487c-8e3f-a4f3cef04ce5.png" style="width:25%" alt="POPSEND by PopBox">
                                        </h2>
                                    </th>
                                    <th width="10%">
                                        <h2 style="padding:20px;margin:0px;box-sizing:border-box">
                                            <img src="https://gallery.mailchimp.com/423156a45dfd7e41866942f5e/images/75f046ea-55c1-4943-bef4-1ff82bb00083.png" alt="POPSEND by PopBox">
                                        </h2>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="white">
                        <td style="padding:20px 20px 10px">
                            <table style="border-spacing:0px;border-collapse:collapse;width:100%">
                                <tbody>
                                <tr>
                                    <td><span style="font-size:17px"> Hi <span style="font-weight:700">{{$data->name}},</span></span>
                                    </td></tr>
                                <tr>
                                    <td><p style="color:rgb(99,99,99)">
                                            Thank you for using POPSEND, we hope you enjoyed with us.
                                        </p>
                                    </td></tr>
                                <tr>
                                    <td><p style="font-size:11px;color:rgb(170,170,170);margin:10px 0px;display:block" class="m_4337856325182816225gmail-m_3708573186675606241amount-paid">Amount Paid</p>
                                        <p style="font-weight:600;font-size:32px;display:inline-block;padding:0px 10px 0px 0px;margin:0px;vertical-align:middle">
                                            Rp{{number_format($data->total_price)}}</p>

                                        {{--<p style="text-transform:uppercase;display:inline-block;border-radius:4px;padding:5px 10px;color:white;font-size:11px;letter-spacing:0.3px;vertical-align:middle;background:rgb(239, 73, 81) none repeat scroll 0% 0%;margin:0px">--}}
                                        {{--Popsend Balance--}}
                                        {{--</p>--}}
                                        {{--<p style="color:rgb(119,119,119);padding:10px 0px;border-bottom:1px solid rgb(230,231,235);margin:0px 0px 10px;font-size:13px" class="m_4337856325182816225gmail-m_3708573186675606241payment-tip">--}}
                                        {{--</p>--}}
                                    </td></tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>


                    <tr bgcolor="white">
                        <td>
                            <table style="border-spacing:0px;border-collapse:collapse" width="100%" align="center">
                                <tbody>
                                <tr>
                                    <td width="90%">
                                        <h2 style="font-size:14px;font-weight:600;margin:0px;padding:0px 20px" class="m_4337856325182816225gmail-m_3708573186675606241order-wrapper">
                                            <span style="color:rgb(170,170,170);font-size:11px;text-transform:uppercase;display:block;padding-bottom:7px">order details</span>
                                            {{ date('l, d M Y H:i A', strtotime($data->pickup_date))}}
                                        </h2>
                                        <h2 style="margin:0px;padding:12px 0px 0px 21px;font-weight:500">
                                            <span style="color:rgb(170,170,170);font-size:10px;text-transform:uppercase;display:block;padding-bottom:7px">order id: {{$data->invoice_id}}</span>
                                        </h2>
                                    </td>
                                    <td style="padding:0px 20px" width="10%">
                                        <img src="https://ci3.googleusercontent.com/proxy/UP0cF8Caz61TzMfYq5YuMFn5C0pQ3ll1eJQMhTiletdInDPHVjEM3BMb29PsY7P-dTXGK93b7UtWCby1iosCKtGLWXQh4UHHmzAweGGlJM3s-IM-5Aklsdk0DIU=s0-d-e1-ft#https://ops-invoice-service-staging.s3.amazonaws.com/order-details.png" alt="Order Details" style="float:right">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>


                    <tr bgcolor="white">
                        <td style="padding:20px 20px 0px">
                            <table style="border-spacing:0px;border-collapse:collapse" width="100%" align="center">
                                <tbody>
                                <tr>
                                    <td>
                                        <img src="https://gallery.mailchimp.com/423156a45dfd7e41866942f5e/images/da14e1ee-9ff5-4175-b1e5-15ff1d14f073.png" alt="pickup">
                                        <span style="color:rgb(23,143,252);text-transform:uppercase;font-size:11px;vertical-align:text-top;padding-left:8px">pick up <b style="color:rgb(51,51,51)"><span class="m_4337856325182816225gmail-m_3708573186675606241dot"></span></b></span>
                                        <p style="border-left:1px dotted rgb(204,204,204);padding:0px 22px;margin:10px 6px;font-size:13px;line-height:1.4">
                                            <span style="font-weight:bold;display:block;padding-bottom:10px;font-size:14px;line-height:1">
                                                <a href="#" target="_blank">{{$data->pickup_name}}</a>
                                            </span>
                                            <a href="#">{{$data->pickup_address}}</a>
                                        </p>
                                    </td>
                                </tr>
                                @if(is_array($data->destinations))
                                    @foreach($data->destinations as $key =>$destination)
                                        <tr>
                                            <td style="border-bottom:1px solid rgb(230,231,235)">
                                                <img src="https://gallery.mailchimp.com/423156a45dfd7e41866942f5e/images/27180dc7-bb3e-4da5-b712-a5ed01687d90.png" alt="drop">
                                                <span style="color:rgb(239,143,3);text-transform:uppercase;font-size:11px;vertical-align:text-top;padding-left:8px">
                                            destination
                                            <b style="color:rgb(51,51,51)">
                                                <span class="m_4337856325182816225gmail-m_3708573186675606241dot"></span>
                                            </b>
                                        </span>
                                                <p style="padding:0px 22px;margin:10px 6px;font-size:13px;line-height:1.4">
                                          <span style="font-weight:bold;display:block;padding-bottom:10px;font-size:14px;line-height:1">
                                              {{$destination->destination_name}}
                                          </span>
                                                    {{$destination->destination_address}}
                                                </p>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr bgcolor="white">
                        <td style="padding:20px 20px">
                            <table style="border-spacing:0px;border-collapse:collapse" width="100%" align="center">
                                <tbody>
                                @if(is_array($data->destinations))
                                    @foreach($data->destinations as $destination)
                                        <tr>
                                            <td style="font-size:13px;color:rgb(119,119,119);padding-bottom:18px">Price Invoice {{$destination->sub_invoice_id}}
                                            </td>
                                            <td style="font-size:14px;text-align:right;font-weight:500;padding-bottom:18px">
                                                {{number_format($destination->price)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                <tr>
                                    <td style="font-size:13px;color:rgb(119,119,119);padding-bottom:18px">
                                        Discount
                                        @if(!empty($data->promo_amount))
                                            <br> {{ $data->promo_name }}
                                        @endif
                                    </td>
                                    <td style="font-size:14px;text-align:right;font-weight:500;padding-bottom:18px;color:rgb(228,53,55)">
                                        @if(!empty($data->promo_amount))
                                            {{ number_format($data->promo_amount) }}
                                        @else
                                            -Rp 0
                                        @endif
                                    </td>
                                </tr>

                                <tr style="border-top:1px solid rgb(230,231,235)">
                                    <td style="font-size:12px;font-weight:bold;padding:15px 0px">TOTAL PAYMENT</td>
                                    <td style="font-size:16px;font-weight:bold;text-align:right;padding:15px 0px">
                                        {{number_format($data->total_price)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span style="font-size:11px;color:rgb(119,119,119);padding:0px 10px 10px 0px">Paid with</span>
                                        <p style="display:inline-block;text-transform:uppercase;border-radius:4px;padding:5px 10px;color:white;font-size:11px;letter-spacing:0.3px;background:rgb(239, 73, 81) none repeat scroll 0% 0%;margin:0px">
                                            PopSend Balance
                                        </p>
                                    </td>
                                </tr>
                                <tr bgcolor="white">
                                    <td style="padding:12px"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="https://popbox.asia" style="display:block" target="_blank">
                                <img class="m_4337856325182816225gmail-m_3708573186675606241promotion_banner"
                                     style="width:100%;margin:0px;padding:0px;border-color:currentcolor;border-style:none;border-width:0px;outline:0px none currentcolor;vertical-align:top"
                                     src="https://gallery.mailchimp.com/77570667abb610db978b66e82/images/f41838f7-d6c6-439e-86bf-1da7255e86b0.png" alt="Promotion Banner">
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <p style="font-size:11px;color:rgb(153,153,153);text-align:right;padding:5px 25px;font-weight:300">
                    Need help? Contact at: <span style="font-weight:500"><a href="mailto:admin@popbox.asia" target="_blank">admin@popbox.asia</a></span>
                </p>
            </td>
        </tr>

        <tr bgcolor="#E8E8E8">
            <td style="padding:0px 20px">
                <h2 style="text-align:center;margin:0px;padding:10px 0px 3px">
                    <img src="https://gallery.mailchimp.com/423156a45dfd7e41866942f5e/images/465eae9a-b127-4c7e-b0bd-b260917201b0.png" alt="popbox asia service" style="height:44px;">
                </h2>
                <p style="font-size:11px;color:rgb(153,153,153);text-align:center;padding:5px 0px">PT. PopBox Asia Services
                    Jl. Palmerah Utara III No.62 FGH
                    (samping SPBU Palmerah Utara)
                    Jakarta Barat 11480 </p>
                <h2 style="margin:0px;text-align:center;padding:8px 0px 18px">
                    <a href="https://www.facebook.com/pboxasia/" target="_blank" style="text-decoration:none">
                        <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-48.png" alt="facebook" class="" width="35" height="35">
                    </a>
                    <a href="https://twitter.com/popbox_asia" target="_blank" style="text-decoration:none">
                        <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-twitter-48.png" alt="twitter" class="" width="35" height="35">
                    </a>
                    <a href="https://www.instagram.com/popbox_asia/" target="_blank" style="text-decoration:none">
                        <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-instagram-48.png" alt="instagram" class="" width="35" height="35">
                    </a>
                    <a href="https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA" target="_blank" style="text-decoration:none">
                        <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-youtube-48.png" alt="youtube" class="" width="35" height="35">
                    </a>
                    <a href="https://www.linkedin.com/company/pt-popbox-asia-services/" target="_blank" style="text-decoration:none">
                        <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-linkedin-48.png" alt="linkedin" class="" width="35" height="35">
                    </a>
                    <a href="https://www.popbox.asia/" target="_blank" style="text-decoration:none">
                        <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-link-48.png" alt="website" class="" width="35" height="35">
                    </a>

                </h2>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
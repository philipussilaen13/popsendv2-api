<?php

namespace App\Jobs;


use App\Http\Library\ApiProx;
use App\Http\Models\Delivery;
use App\Http\Models\Logs;

class PushMalaysiaPopSendLocker extends Job
{
    private $delivery ;

    /**
     * PushMalaysiaPopSendLocker constructor.
     * @param Delivery $delivery
     *
     * @return void
     */
    public function __construct(Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get sender Name and Phone
        $deliveryData = $this->delivery;
        $deliveryId = $deliveryData->invoice_code;
        $senderName = $deliveryData->name_sender;
        $senderPhone = $deliveryData->phone_sender;
        $deliveryType = $deliveryData->pickup_type;
        $originName = $deliveryData->origin_name;
        $userData = $deliveryData->user;
        $country = $userData->country;

        Logs::logFile("","pushMyLockerPickup","$deliveryId - $deliveryType = $country");

        if ($deliveryType != 'locker'){
            Logs::logFile("","pushMyLockerPickup","$deliveryType Not Locker");
            return ;
        }

        // get destinations
        $destinations = $deliveryData->destinations;

        $proxApi = new ApiProx();

        foreach ($destinations as $destination){
            $destinationId = $destination->invoice_sub_code;
            $address = $destination->destination_name;
            $endAddress = "$originName ]==)> $address";

            Logs::logFile("","pushMyLockerPickup","Origin :  $originName. Invoice Sub Code : $destinationId $endAddress");


            $push = $proxApi->orderPopSend('MY',$senderPhone,$senderName,$destinationId,$originName,$endAddress);
            Logs::logFile("","pushMyLockerPickup","Response: ".json_encode($push));
        }
    }
}

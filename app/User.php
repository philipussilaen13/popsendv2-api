<?php

namespace App;

use App\Http\Library\Helper;
use App\Http\Models\PasswordReset;
use App\Http\Models\ReferralCampaign;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{

    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Insert New User
     * @param $name
     * @param $phone
     * @param $email
     * @param $password
     * @param string $country
     * @param float $balance
     * @param null $referralCode
     * @return \stdClass
     */
    public static function insertNewUser($name, $phone, $email,$password, $country = 'ID',$balance=0.0,$referralCode=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->memberId = null;

        // generate member ID
        $isExist = true;
        while ($isExist){
            $memberId = Helper::generateRandomString(10);
            $check = self::where('member_id',$memberId)->first();
            if (!$check) $isExist = false;
        }

        // hash password
        $hashedPassword = Hash::make($password);

        // clear double space
        $name = preg_replace('/\s+/',' ',$name);

        // generate referral code
        if (empty($referralCode)){
            $firstName = '';
            $words = explode(' ',$name);
            if (!empty($words)){
                $firstName = $words[0];
            }
            $firstName = strtoupper($firstName);
            $random = Helper::generateRandomString(3);
            $referralCode = $firstName.$random;
        }

        // save to DB
        $userDb = new self();
        $userDb->member_id = $memberId;
        $userDb->name = $name;
        $userDb->phone = $phone;
        $userDb->email = $email;
        $userDb->password = $hashedPassword;
        $userDb->status = 'ACTIVE';
        $userDb->country = $country;
        $userDb->referral_code = $referralCode;
        $userDb->balance = $balance;
        $userDb->save();

        $response->isSuccess = true;
        $response->memberId = $memberId;
        $response->id = $userDb->id;

        return $response;
    }

    /**
     * Get User Profile By Member ID
     * @param $memberId
     * @return \stdClass
     */
    public static function getUserProfileByMemberId($memberId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // find user based on member Id
        $userDb = self::where('member_id',$memberId)->first();
        if (!$userDb){
            $response->errorMsg = 'Member User Not Found';
            return $response;
        }

        $userId = $userDb->id;
        $sessionId = null;

        $userSessionDb = DB::table('user_sessions')
            ->where('user_id',$userId)
            ->orderBy('last_activity', 'DESC')
            ->first();

        // adding user session to response
        if ($userSessionDb) {
            $sessionId = $userSessionDb->session_id;
        }

        $data = new \stdClass();
        $data->member_id = $userDb->member_id;
        $data->name = $userDb->name;
        $data->favorite_locker = $userDb->favorite_locker;
        $data->phone = $userDb->phone;
        $data->email = $userDb->email;
        $data->address = $userDb->address;
        $data->status = $userDb->status;
        $data->country = $userDb->country;
        $data->balance = $userDb->balance;
        $data->referral_code = $userDb->referral_code;
        $data->id_number = empty($userDb->id_number) ? null : url($userDb->id_number);
        $data->id_image = empty($userDb->id_image) ? null : url($userDb->id_image);
        $data->avatar = $userDb->avatar;

        $referralDescription = null;
        $referralDescriptionToShared = null;
        // get active referral
        $referralModel = new ReferralCampaign();
        // get active referral campaign
        $activeReferralCampaign = $referralModel->getRegularActiveReferral();
        if ($activeReferralCampaign->isSuccess){
            $referralCampaign = $activeReferralCampaign->data;
            $referralDescription = $referralCampaign->description;
            $referralDescriptionToShared = $referralCampaign->description_to_shared;
        }

        $data->description = $referralDescription;
        $data->description_to_shared = $referralDescriptionToShared;

        // adding user session to response
        $data->sessionId = $sessionId;

        $response->isSuccess = true;
        $response->data = $data;

        return $response;
    }

    public static function updateUser($name,$member_id,$phone,$password,$favorite_locker,$avatar,$image_ktp) {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->memberId = null;

        // clear double space
        $name = preg_replace('/\s+/',' ',$name);

        $userDb = self::where('member_id',$member_id)->first();
        if (!$userDb){
            $response->errorMsg = 'Member User Not Found';
            return $response;
        }
        $userDb->name = $name;
        $userDb->phone = $phone;
        if($password !=''){
            // hash password
            $hashedPassword = Hash::make($password);
            $userDb->password = $hashedPassword;
        }
        $userDb->favorite_locker = $favorite_locker;
        $userDb->avatar = $avatar;
        $userDb->id_image = $image_ktp;
        $userDb->save();

        $response->isSuccess = true;
        $response->member_id = $member_id;

        return $response;
    }

    /**
     * Get Referral Code
     * @param $memberId
     * @return \stdClass
     */
    public static function getReferralCode($memberId) {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // find user based on member Id
        $userDb = self::where('member_id',$memberId)->first();
        if (!$userDb){
            $response->errorMsg = 'Member Not Found';
            return $response;
        }

        $data = new \stdClass();
        $data->member_id = $userDb->member_id;
        $data->name = $userDb->name;
        $data->avatar = $userDb->avatar;
        $data->referral_code = $userDb->referral_code;
        $data->description = null;
        $data->description_to_shared = null;

        // get referral active
        $nowDate = date('Y-m-d H:i:s');

        $referralCampaignDb = ReferralCampaign::where('status',1)
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->orderBy('created_at','desc')
            ->first();

        if ($referralCampaignDb){
            $data->description = $referralCampaignDb->description;
            $data->description_to_shared = $referralCampaignDb->description_to_shared;
        }

        $response->isSuccess = true;
        $response->data = $data;

        return $response;
    }

    /**
     * @param $userId
     * @param $password
     * @param $code
     * @return \stdClass
     */
    public static function updatePassword ($userId, $password, $code)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        //Upate Password User
        $user = self::find($userId);
        $user->password = Hash::make($password);
        $user->save();

        //Update Password Reset
        $passwordReset = PasswordReset::where('code','=',$code)->first();
        $passwordReset->reseted_at = date('Y-m-d H:i:s');
        $passwordReset->status = 1;
        $passwordReset->save();


        $response->isSuccess = true;
        $response->data = [
            'name' => $user->name,
            'phone' => $user->phone,
            'email' => $user->email
        ];

        return $response;
    }
}

<?php

namespace App\Console\Commands;

use App\Http\Models\ReferralTransaction;
use App\Http\Models\Logs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckReferralTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Unused Referral Transaction and check Rule Pass';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Begin Process Check Referral Transaction\n";
        $location = storage_path()."/logs/cron/";
        Logs::logFile($location,'referralTransaction',"Begin Process Check Referral Transaction CRON");

        $nowDateTime = date('Y-m-d H:i:s');

        $referralTransactionDb = ReferralTransaction::with('campaign')
            ->where('expired_date','>=',$nowDateTime)
            ->where('referral_transactions.status','PENDING')
            ->get();

        foreach ($referralTransactionDb as $transaction) {
            $id = $transaction->id;
            $code = $transaction->code;
            $fromUserId = $transaction->from_user_id;
            $toUserId = $transaction->to_user_id;
            $type = $transaction->campaign->type;
            $rule = $transaction->campaign->rule;
            $fromAmount = $transaction->campaign->from_amount;
            $toAmount = $transaction->campaign->to_amount;

            $message = "Check $id: $code, from:$fromUserId,$fromAmount to:$toUserId,$toAmount";
            echo "$message\n";
            Logs::logFile($location,'referralTransaction',$message);
            // initiate model
            $referralTransactionModel = new ReferralTransaction();
            // checking referral pass rule
            $check = $referralTransactionModel->checkRulePass($toUserId,$type,$rule);
            if (!$check->isSuccess){
                // check if failed
                if ($check->isFailed){
                    // update referral to Failed
                    $referralTransDb = ReferralTransaction::find($id);
                    $referralTransDb->status = 'FAILED';
                    $referralTransDb->save();

                    $message = $check->errorMsg." set FAILED";
                    echo "$message\n";
                    Logs::logFile($location,'referralTransaction',$message);
                    continue;
                } else {
                    $message = $check->errorMsg;
                    echo "$message\n";
                    Logs::logFile($location,'referralTransaction',$message);
                    continue;
                }
            }

            // process referral transaction
            DB::beginTransaction();

            $processReferral = $referralTransactionModel->processReferral($fromUserId,$toUserId,$fromAmount,$toAmount,$code,$id);
            if (!$processReferral->isSuccess){
                $message = $processReferral->errorMsg;
                echo "$message\n";
                Logs::logFile($location,'referralTransaction',$message);
                continue;
            }

            DB::commit();
            $message = "====Finish Commit $id: $code====";
            echo "$message\n";
            Logs::logFile($location,'referralTransaction',$message);
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Http\Models\PaymentTransaction;
use App\User;
use Illuminate\Console\Command;

class PaymentFixUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:fix-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Payment User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Get Payment with empty UserID\n";

        $paymentDb = PaymentTransaction::whereNull('user_id')->get();

        foreach ($paymentDb as $item) {
            $phone = $item->phone;

            echo "phone \n";

            // query
            $userDb = User::where('phone',$phone)->first();
            if (!$userDb){
                echo "User Not Found \n";
                continue;
            }

            $dataDb = PaymentTransaction::find($item->id);
            $dataDb->user_id = $userDb->id;
            $dataDb->save();
            echo "Save\n";
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Http\Library\ApiProx;
use App\Http\Models\Popsafe;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PopSafeCheckStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'popsafe:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and Change Status PopSafes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Process Disable Drop / Expired \n";
        $nowTime = time();
        $oldTime = date('Y-m-d H:i:s',strtotime("-5 days"));
        $popSafeDb = Popsafe::where('status','CREATED')->where('created_at','>',$oldTime)->get();
        foreach ($popSafeDb as $item) {
            DB::beginTransaction();
            $id = $item->id;
            $orderNumber = $item->invoice_code;
            $transactionDate = $item->transaction_date;
            $expiredDateTime = $item->expired_time;
            $expressId = $item->express_id;

            echo "ID: $id, $orderNumber, $expiredDateTime\n";

            if ($nowTime > strtotime($expiredDateTime)){
                $status = 'EXPIRED';
                // update is cancel
                $popSafeModel = new Popsafe();
                $update = $popSafeModel->updatePopsafeStatus($orderNumber,$status);

                // delete order ID
                $apiProx = new ApiProx();
                $deleteImported = $apiProx->deleteImport($expressId);
                if (empty($deleteImported)){
                    echo "Failed Delete\n";
                    DB::rollback();
                    continue;
                }
                if ($deleteImported->response->code != 200){
                    echo $deleteImported->response->message."\n";
                    DB::rollback();
                    continue;
                }

                echo "Finish update is cancel 0 and is_expired 0 set EXPIRED $orderNumber\n";
                DB::commit();
                continue;
            }
            DB::rollback();
        }

        echo "Process Overdue \n";
        $popSafeDb = Popsafe::where('status','IN STORE')->where('created_at','>',$oldTime)->get();

        foreach ($popSafeDb as $item) {
            DB::beginTransaction();
            $id = $item->id;
            $orderNumber = $item->invoice_code;
            $transactionDate = $item->transaction_date;
            $expiredDateTime = $item->expired_time;

            if ($nowTime > strtotime($item->expired_time)){
                $status = 'OVERDUE';

                $popSafeModel = new Popsafe();
                $update = $popSafeModel->updatePopsafeStatus($orderNumber,$status);
                echo "Finish set overdue $orderNumber\n";
                DB::commit();
                continue;
            }
            DB::rollback();
        }
        echo "FINISH\n";
    }
}

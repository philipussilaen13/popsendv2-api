<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Library\PaymentAPI;
use App\Http\Models\PaymentTransaction;
use App\Http\Library\Helper;

class GetPaymentReference extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PaymentTransaction:PaymentReference';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Payment Reference from Payment API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "START GET PAYMENT REFERENCE \n";
        $mytime = \Carbon\Carbon::now();
        
        $paymentTransactionDB = PaymentTransaction::where('payment_reference', null)->where('payment_defined', null)->orderBy('created_at', 'DESC')->get();
        
        $list = [];
        foreach ($paymentTransactionDB as $key => $transaction) {
            $paymentAPI = new PaymentAPI;
            $paymentParam['transaction_id'] = [$transaction->reference];
            $resultPayment = $paymentAPI->inquiryPayment($paymentParam);
            
            if ($resultPayment->response->code == 200) {
                $paymentId = $resultPayment->data[0]->payment_id;
                $transaction->payment_reference = $paymentId;
                $transaction->payment_defined = 1;
                $transaction->save();
                $list[] = $transaction->reference.'|'.$paymentId;
                echo "$transaction->reference|$paymentId \n";
            } else if ($resultPayment->response->code == 400) {
                $flag = explode(' ', $resultPayment->response->message);
                if ($flag[0] == 'Invalid') {
                    $transaction->payment_defined = 2;
                    $transaction->save();
                    $list[] = $transaction->reference.'|'.$flag[0];
                    echo "$transaction->reference|$flag[0] \n";
                }

            }
        }
        Helper::LogPayment($mytime->toDateTimeString().' - '. json_encode($list),'payment','payment_transaction.');
        
        echo "END GET PAYMENT REFERENCE \n";
    }
}

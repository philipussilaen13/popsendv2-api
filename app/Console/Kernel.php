<?php

namespace App\Console;

use App\Console\Commands\BackupDatabase;
use App\Console\Commands\ChangePermission;
use App\Console\Commands\CheckReferralTransaction;
use App\Console\Commands\DatabaseRemoveOldLogs;
use App\Console\Commands\PaymentFixUser;
use App\Console\Commands\PopSafeAutoExtend;
use App\Console\Commands\PopSafeCheckStatus;
use App\Console\Commands\PopSendMalaysiaStatus;
use App\Console\Commands\GetPaymentReference;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CheckReferralTransaction::class,
        ChangePermission::class,
        PopSafeCheckStatus::class,
        PopSafeAutoExtend::class,
        PaymentFixUser::class,
        PopSendMalaysiaStatus::class,
        BackupDatabase::class,
        PopSendMalaysiaStatus::class,
        DatabaseRemoveOldLogs::class,
        GetPaymentReference::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $nowDate = date('Y.m.d');
        $schedule->command('referral:transaction')->everyMinute();
        $schedule->command('PaymentTransaction:PaymentReference')->everyFiveMinutes();
        $schedule->command('popsafe:status')->everyMinute()->appendOutputTo(storage_path()."/logs/cron/popsafe-check.$nowDate.log");
        $schedule->command('popsend:malaysia-status')->everyFiveMinutes()->appendOutputTo(storage_path()."/logs/cron/popsend-my-status.$nowDate.log");
        $schedule->command('database:remove-old-log')->dailyAt('02:00')->appendOutputTo(storage_path()."/logs/cron/database-remove-old.$nowDate.log");
        // $schedule->command('permission:change')->dailyAt('00:59');
    }
}

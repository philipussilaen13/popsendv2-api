<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 28/12/2017
 * Time: 10.17
 */

namespace App\Http\Middleware;

use App\Http\Models\ApiToken;
use App\Http\Models\Logs;
use App\Http\Models\UserSession;
use Closure;
class ApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $startTime = microtime(true);
        // create default response
        $code = 400;
        $errorMsg = null;

        $module = $request->path();
        $method = $request->method();

        if (strpos($module, 'user/changePwd') !== false){
            $response = $next($request);
            return $response;
        }
        if (strpos($module, 'pages') !== false){
            $response = $next($request);
            return $response;
        }
        if ($module == 'delivery/expressCallback'){
            $response = $next($request);
            return $response;
        }

        // check header content type
        if (!$request->hasHeader('Content-Type') || $request->header('Content-Type')!='application/json'){
            $errorMsg = 'Invalid Content-Type';
            return $this->falseReturn($code,$errorMsg,$startTime);
        }

        // check token
        $token = $request->input('token',null);
        if (empty($token)){
            $errorMsg = 'Missing Token';
            return $this->falseReturn($code,$errorMsg,$startTime);
        }
        $checkApiToken = ApiToken::checkToken($token);
        if (!$checkApiToken->isSuccess){
            $errorMsg = $checkApiToken->errorMsg;
            return $this->falseReturn($code,$errorMsg,$startTime);
        }
        $tokenId = $checkApiToken->tokenId;
        $module = $request->path();

        // get user id by sessionId
        $sessionId = $request->input('session_id',null);
        $userId = null;
        if (!empty($sessionId)){
            $userSession = UserSession::getUserBySessionId($sessionId);
            if ($userSession->isSuccess){
                $userId = $userSession->userId;
            }
        }

        // insert to logs db
        $logId = Logs::insertLog($module,$tokenId,$sessionId,$request,$userId);

        $response = $next($request);

        if ($module == 'payment/callbackFixed') {
            Logs::updateResponseLog($logId,$response,0);
            return $response;
        }

        // add latency to response
        $endTime = microtime(true);
        $latency = $endTime - $startTime;

        $originalResponse = $response->original;
        if (!empty($originalResponse)){
            if (!isset($originalResponse->response->latency)){
                $originalResponse->response->latency = $latency;
                $originalResponse = json_encode($originalResponse);
                $response->setContent($originalResponse);
            }
        }

        $dataResponse = $response->getContent();
        if ($this->isJSON($dataResponse)){
            $dataArray = json_decode($dataResponse,true);
            $dataArray = $this->replaceNullWithEmptyString($dataArray);
            $dataJson = json_encode($dataArray);
            $response = $response->setContent($dataJson);
        }

        // update log
        Logs::updateResponseLog($logId,$response,$latency);

        return $response;
    }

    private function falseReturn($status = 400, $message="General Error",$startTime){
        $response = new \stdClass();
        $response->code = $status;
        $response->message = $message;

        $endTime = microtime(true);

        $latency = $endTime- $startTime;

        $result = new \stdClass();
        $response->code = $status;
        $response->message = $message;
        $response->latency = $latency;
        $result->response = $response;
        return response()->json($result);
    }

    private function replaceNullWithEmptyString($array)
    {
        foreach ($array as $key => $value)
        {
            if(is_array($value)){
                $array[$key] = $this->replaceNullWithEmptyString($value);
            }
            else
            {
                if (is_null($value)){
                    $array[$key] = "";
                }
            }
        }
        return $array;
    }

    private function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 28/12/2017
 * Time: 09.24
 */

namespace App\Http\Middleware;

use App\Http\Models\ApiToken;
use App\Http\Models\UserSession;
use Closure;
class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $startTime = microtime(true);
        // create default response
        $code = 400;
        $errorMsg = null;

        $sessionId = $request->input('session_id',null);

        if (empty($sessionId)){
            $errorMsg = 'Session ID not Found';
            return $this->falseReturn($code,$errorMsg,$startTime);
        }

        $userSession = UserSession::getUserBySessionId($sessionId);
        if (!$userSession->isSuccess){
            $code = 403;
            $errorMsg = 'Invalid Session ID';
            return $this->falseReturn($code,$errorMsg,$startTime);
        }

        return $next($request);
    }

    private function falseReturn($status = 400, $message="General Error",$startTime){
        $response = new \stdClass();
        $response->code = $status;
        $response->message = $message;

        $endTime = microtime(true);

        $latency = $endTime- $startTime;

        $result = new \stdClass();
        $response->code = $status;
        $response->message = $message;
        $response->latency = $latency;
        $result->response = $response;
        $result->data = [];
        return response()->json($result);
    }
}
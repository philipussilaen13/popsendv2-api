<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 27/12/2017
 * Time: 11.14
 */

namespace App\Http\Middleware;

use Closure;

class ApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uniqueId = uniqid();
        // set start time
        $startTime = microtime(true);
        // get ip
        $ip = $request->ip();
        // get module / url
        $module = $request->path();

        $param = json_encode($request->input());

        $this->log("$uniqueId - $ip URL: $module Request: $param");

        $response = $next($request);

        $endTime = microtime(true);
        $latency = $endTime - $startTime;
        $logResponse = $response->content();
        $this->log("$uniqueId latency $latency Response : $logResponse");

        return $response;
    }

    private function log($msg=''){
        $msg = " $msg\n";
        $f = fopen(storage_path().'/logs/access/'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 12/06/18
 * Time: 12.03
 */

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Library\Helper;
use App\Http\Models\Article\Articles;
use Illuminate\Http\Request;

class ArticleController extends Controller {

    public function getArticle(Request $request) {

        $errorMessage = null;
        $requiredInput = ['session_id'];

        //check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $article_id = $request->input('article_id',null);

        // create filter
        $filter = $request->except(['article_id']);

        $article = new Articles();
        $result = $article->getArticle($article_id, $filter);

        if (empty($result)) {
            $errorMessage = 'Article Not Found';
            return ApiHelper::buildResponse(200,$errorMessage);
        }

        return ApiHelper::buildResponse(200,null,$result);
    }

    public function getArticleLocker(Request $request) {

        $article_id = $request->input('article_id',null);

//        $request['type'] = 'locker';

        // create filter
        $filter = $request->except(['article_id']);

        $article = new Articles();
        $result = $article->getArticle($article_id, $filter);

        if (empty($result)) {
            $errorMessage = 'Article Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        return ApiHelper::buildResponse(200,null,$result);

    }

    public function getArticleWeb(Request $request) {

        $article_id = $request->input('article_id',null);

//        $request['type'] = 'web';

        // create filter
        $filter = $request->except(['article_id']);

        $article = new Articles();
        $result = $article->getArticle($article_id, $filter);

        if (empty($result)) {
            $errorMessage = 'Article Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        return ApiHelper::buildResponse(200,null,$result);
    }


    public function getArticleMobile(Request $request) {

        $article_id = $request->input('article_id',null);

//        $request['type'] = 'mobile';

        // create filter
        $filter = $request->except(['article_id']);

        $article = new Articles();
        $result = $article->getArticle($article_id, $filter);

        if (empty($result)) {
            $errorMessage = 'Article Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        return ApiHelper::buildResponse(200,null,$result);
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Library\ApiPopExpress;
use App\Http\Library\Google;
use App\Http\Library\Helper;
use App\Http\Models\BalanceRecord;
use App\Http\Models\City;
use App\Http\Models\Country;
use App\Http\Models\Delivery;
use App\Http\Models\DeliveryDestination;
use App\Http\Models\MalaysiaTariff;
use App\Http\Models\PopboxV1\PopSendTariff;
use App\Http\Models\PopExpress\Locker;
use App\Http\Models\Promo;
use App\Jobs\PushMalaysiaPopSendLocker;
use Illuminate\Support\Facades\Log;
use App\Http\Models\District;
use App\Http\Models\GoogleLocation;
use App\Http\Models\LockerLocation;
use App\Http\Models\Province;
use App\Http\Models\Transaction;
use App\Http\Models\UserSession;
use App\Jobs\ProcessOderPopexpress;
use App\Jobs\SendEmailOrder;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use Illuminate\Http\Request;
use PHPUnit\Framework\Constraint\Count;
use App\Http\Models\Logs;

class OrderController extends Controller
{
    /**
     * Update Data District
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDataDistrict(){
        $code = 400;
        $errorMessage = null;
        /*======================================Malaysia======================================*/
        // get country
        $countryDb = Country::where('country_name','Malaysia')->first();
        if (!$countryDb){
            $countryDb = new Country();
            $countryDb->country_name = 'Malaysia';
            $countryDb->save();
        }
        $countryId = $countryDb->id;

        // get malaysia locker
        $malaysiaLocker = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->where('provinces.country_id','=','2')
            ->get();

        foreach ($malaysiaLocker as $item) {
            $latitude = $item->latitude;
            $longitude = $item->longitude;

            // get address by reverse google
            $getAddress = $this->getAddressByReverseGoogle($latitude,$longitude,'MY');
            if (!$getAddress->isSuccess){
                continue;
            }
            $addressList = $getAddress->addressList;
            foreach ($addressList as $value) {
                $districtName = $value->district;
                $cityName = $value->city;
                $provinceName = $value->province;

                if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

                $countryDb = Country::where('country_name','Malaysia')->first();
                if (!$countryDb){
                    $countryDb = new Country();
                    $countryDb->country_name = 'Malaysia';
                    $countryDb->save();
                }
                $countryId = $countryDb->id;
                $provinceDb = Province::where('province_name',$provinceName)->where('country_id',$countryId)->first();
                if (!$provinceDb){
                    $provinceDb = new Province();
                    $provinceDb->province_name = $provinceName;
                    $provinceDb->country_id = $countryId;
                    $provinceDb->save();
                }
                $provinceId = $provinceDb->id;

                $cityDb = City::where('city_name',$cityName)->where('province_id',$provinceId)->first();
                if (!$cityDb){
                    $cityDb = new City();
                    $cityDb->city_name = $cityName;
                    $cityDb->province_id = $provinceId;
                    $cityDb->save();
                }
                $cityId = $cityDb->id;

                $districtDb = District::where('district_name',$districtName)->where('city_id',$cityId)->first();
                if (!$districtDb){
                    $districtDb = new District();
                    $districtCode = "MY".Helper::generateRandomString(3,'Numeric');
                    $districtDb->city_id = $cityId;
                    $districtDb->code = $districtCode;
                    $districtDb->district_name = $districtName;
                    $districtDb->save();
                }
            }
        }

        // get for malaysia tariff
        $popSendTariffDb = PopSendTariff::where('country','MY')->get();
        foreach ($popSendTariffDb as $item) {
            $origin = $item->origin;
            $province = $item->destination_prov;
            $city = $item->destination_city;
            $district = $item->destination_kec;

            $docPrice = $item->doc_price;
            $sPrice = $item->s_price;
            $mPrice = $item->m_price;
            $l_price = $item->l_price;

            $estDelivery = $item->est_delivery;

            // check on table
            $malaysiaTariff = MalaysiaTariff::where('origin',$origin)
                ->where('destination_province',$province)
                ->where('destination_city',$city)
                ->where('destination_district',$district)
                ->first();

            if (!$malaysiaTariff){
                $dataDb = new MalaysiaTariff();
                $dataDb->country = 'MY';
                $dataDb->origin = $origin;
                $dataDb->destination_province = $province;
                $dataDb->destination_city = $city;
                $dataDb->destination_district = $district;
                $dataDb->doc_price = $docPrice;
                $dataDb->s_price = $sPrice;
                $dataDb->m_price = $mPrice;
                $dataDb->l_price =$l_price;
                $dataDb->est_delivery = $estDelivery;
                $dataDb->save();
            }
        }

        /*==========================Indonesia==========================*/
        $countryDb = Country::where('country_name','Indonesia')->first();
        if (!$countryDb){
            $countryDb = new Country();
            $countryDb->country_name = 'Malaysia';
            $countryDb->save();
        }
        $countryId = $countryDb->id;

        $newArray = [];
        $dataDb = DB::connection('express')->table('international_codes')->get();
        foreach ($dataDb as $item) {
            $tmp = new \stdClass();
            $tmp->name = $item->district;
            $tmp->detail_code = $item->detail_code;
            $newArray[$item->province][$item->county][] = $tmp;
        }

        foreach ($newArray as $provinceName => $cityName){
            // check on province table
            $provinceDb = Province::where('province_name',$provinceName)->where('country_id',$countryId)->first();
            if (!$provinceDb){
                $provinceDb = new Province();
                $provinceDb->province_name = $provinceName;
                $provinceDb->country_id = $countryId;
                $provinceDb->save();
            }
            foreach ($cityName as $name => $districtList) {
                // check on city table
                $cityDb = City::where('province_id',$provinceDb->id)
                    ->where('city_name',$name)
                    ->first();
                if (!$cityDb){
                    $cityDb = new City();
                    $cityDb->province_id = $provinceDb->id;
                    $cityDb->city_name = $name;
                    $cityDb->save();
                }
                foreach ($districtList as $item) {
                    // check on district table
                    $districtName = $item->name;
                    $districtDb = District::where('city_id',$cityDb->id)->where('district_name',$districtName)->first();
                    if ($districtDb){
                        if ($item->detail_code == "-1") continue;
                       $dataDb = District::find($districtDb->id);
                       $dataDb->code = $item->detail_code;
                       $dataDb->save();
                    } else {
                        $dataDb = new District();
                        $dataDb->city_id = $cityDb->id;
                        $dataDb->code = $item->detail_code;
                        $dataDb->district_name = $districtName;
                        $dataDb->save();
                    }
                }
            }
        }

        $code = 200;
        $errorMessage = 'Success Update Data';
        return ApiHelper::buildResponse($code,$errorMessage);
    }

    /**
     * check Shipping Price by origin and destination
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popSendCalculate(Request $request){
        $errorMessage = null;
        $requiredInput = ['pickup_type','destinations'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // general rules for validation
        $rules = [
            'pickup_type' => 'required|in:locker,address',
            'destinations.*.type' => 'required|in:locker,address',
            'destinations.*.latitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.longitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.locker_id' => 'required_if:destination_data.*.type,locker',
            'destinations.*.weight' => 'nullable|numeric'
        ];

        $pickupType = $request->input('pickup_type');
        // rule validation based on  pickup type
        if ($pickupType == 'locker'){
            $rules['locker_id'] = 'required';
        } else {
            $rules['pickup_latitude'] = 'required';
            $rules['pickup_longitude'] = 'required';
        }

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->erroMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUserId->userId;
        $userDb = User::find($userId);
        $country = $userDb->country;

        $pickupName = null;
        $pickupAddress = null;
        $pickupLatitude = null;
        $pickupLongitude = null;
        $pickupCity = null;
        $pickupCode = null;
        $pickupDistrictId = null;

        // initiate model
        $lockerModel = new LockerLocation();
        $lockerExpressModel = new Locker();

        // if pickup type Address
        if ($pickupType == 'address'){
            $pickupLatitude = $request->input('pickup_latitude');
            $pickupLongitude = $request->input('pickup_longitude');

            // get address pickup by lat long
            $getPickupCity = $this->getAddressByReverseGoogle($pickupLatitude,$pickupLongitude,$country);
            if (!$getPickupCity->isSuccess){
                $errorMessage = 'Failed to Get Pickup Address. Please Try Again';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $pickupName = $getPickupCity->address;
            $pickupAddress = $getPickupCity->address;
            $addressList = $getPickupCity->addressList;
            // check based on list
            $checkPickup = $this->checkPickupAddress($addressList,$country);
            if (!$checkPickup->isSuccess){
                $errorMessage = $checkPickup->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $districtDb = District::with('city')
                ->where('id',$checkPickup->districtId)
                ->first();
            if ($districtDb) {
                $pickupCity = $districtDb->city->city_name;
                $pickupDistrictId = $districtDb->id;
            }
        }
        // if pickup type locker
        elseif ($pickupType == 'locker') {
            $lockerId = $request->input('locker_id');
            if ($country == 'MY'){
                $lockerData = $lockerModel->getLockerByLockerId($lockerId);
                if (empty($lockerData)){
                    $errorMessage = 'Locker Not Found';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $pickupName = $lockerData->name;
                $pickupAddress = "Locker $pickupName, $lockerData->address";
                $pickupLatitude = $lockerData->latitude;
                $pickupLongitude = $lockerData->longitude;
                $pickupCity = $lockerData->district;

                $districtName = $lockerData->kecamatan;
                $cityName = $lockerData->district;
                $provinceName = $lockerData->province;
            }
            else {
                // $lockerData = $lockerModel->getLockerByLockerId($lockerId);
                $lockerData = $lockerExpressModel->getLockerByLockerId($lockerId);
                if (empty($lockerData)){
                    $errorMessage = 'Locker Not Found';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $pickupName = $lockerData->name;
                $pickupAddress = "Locker $pickupName, $lockerData->address";
                $pickupLatitude = $lockerData->latitude;
                $pickupLongitude = $lockerData->longitude;
                $pickupCity = $lockerData->county;
                $districtName = $lockerData->district;
                $cityName = $lockerData->county;
                $provinceName = $lockerData->province;
            }

            // get district id
            $districtDb = District::with('city')
                ->where('district_name','LIKE',"$districtName")
                ->whereHas('city',function ($query) use ($cityName){
                    $query->where('city_name','LIKE',"%$cityName%");
                })->first();
            if ($districtDb){
                $pickupDistrictId = $districtDb->id;
            } else {
                // get data from gmaps based on latitude and longitude
                $getDestinationGoogle = $this->getAddressByReverseGoogle($lockerData->latitude,$lockerData->longitude,$country);
                if (!$getDestinationGoogle->isSuccess){
                    $errorMessage = 'Failed to Get Destination Locker Address. Please Try Again';
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $addressList = $getDestinationGoogle->addressList;
                $checkDestination = $this->checkDestinationAddress($addressList,$country);
                if (!$checkDestination->isSuccess){
                    $errorMessage = $checkDestination->errorMsg;
                    return ApiHelper::buildResponse(400,$errorMessage);
                }
                $pickupDistrictId = $checkDestination->districtId;
            }
        }
        else {
            $errorMessage = 'Unknown Pickup Type';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // check destination
        $destinationList = [];
        $destinationInput = $request->input('destinations');
        foreach ($destinationInput as $item){
            $item = (object)$item;
            $destinationType = $item->type;
            if ($destinationType == 'locker' && empty($item->locker_id)){
                $errorMessage = 'Locker Id Required for Destination Type Locker';
                return ApiHelper::buildResponse(400,$errorMessage);
            } elseif ($destinationType == 'address' && (empty($item->latitude) || empty($item->longitude))){
                $errorMessage = 'Latitude and Longitude Required for Destination Type Address';
                return ApiHelper::buildResponse(400,$errorMessage);
            } elseif (!in_array($destinationType,['locker','address'])) {
                $errorMessage = 'Unknown Destination Type';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $weight = 1;
            if (!empty($item->weight)) $weight = $item->weight;

            $tmp = new \stdClass();
            $tmp->destination_type = $item->type;
            $tmp->destination_name = null;
            $tmp->destination_address = null;
            $tmp->destination_latitude = null;
            $tmp->destination_longitude = null;
            $tmp->isValid = false;
            $tmp->error = null;
            $tmp->districtId = null;
            $tmp->weight = (double)$weight;

            if ($destinationType == 'address'){
                $addressList = [];
                $destinationLatitude = $item->latitude;
                $destinationLongitude = $item->longitude;
                $getDestinationGoogle = $this->getAddressByReverseGoogle($destinationLatitude,$destinationLongitude,$country);
                if (!$getDestinationGoogle->isSuccess){
                    $tmp->error = 'Failed to Get Destination Address. Please Try Again';
                    $destinationList[] = $tmp;
                    continue;
                }
                $addressList = $getDestinationGoogle->addressList;
                $tmp->destination_latitude = $destinationLatitude;
                $tmp->destination_longitude = $destinationLongitude;
                $tmp->destination_name = $getDestinationGoogle->address;
                $tmp->destination_address = $getDestinationGoogle->address;

                // check based on list
                if ($country == 'MY') {
                    $checkDestination = $this->saveDestinationMalaysia($addressList);
                }
                else {
                    $checkDestination = $this->checkDestinationAddress($addressList);
                }
                if (!$checkDestination->isSuccess){
                    $tmp->error = $checkDestination->errorMsg;
                    $destinationList[] = $tmp;
                    continue;
                }
                $tmp->isValid = true;
                $tmp->districtId = (string)$checkDestination->districtId;
                $destinationList[] = $tmp;
            }
            elseif ($destinationType == 'locker'){
                $lockerId = $item->locker_id;
                if ($country == 'MY'){
                    $lockerData = $lockerModel->getLockerByLockerId($lockerId);
                    $districtName = $lockerData->kecamatan;
                    $cityName = $lockerData->district;
                    $provinceName = $lockerData->province;
                }
                else {
                    $lockerData = $lockerExpressModel->getLockerByLockerId($lockerId);
                    if (empty($lockerData)){
                        $errorMessage = 'Locker Not Found';
                        return ApiHelper::buildResponse(400,$errorMessage);
                    }
                    $districtName = $lockerData->district;
                    $cityName = $lockerData->county;
                    $provinceName = $lockerData->province;
                }
                $tmp->destination_name = $lockerData->name;
                $tmp->destination_address = "Locker $lockerData->name, $lockerData->address";
                $tmp->destination_latitude = $lockerData->latitude;
                $tmp->destination_longitude = $lockerData->longitude;

                // get district id
                $districtDb = District::with('city')
                    ->where('district_name','LIKE',"$districtName")
                    ->whereHas('city',function ($query) use ($cityName){
                        $query->where('city_name','LIKE',"%$cityName%");
                    })->first();

                if ($districtDb){
                    $tmp->districtId = (string)$districtDb->id;
                }
                else {
                    // get data from gmaps based on latitude and longitude
                    $getDestinationGoogle = $this->getAddressByReverseGoogle($lockerData->latitude,$lockerData->longitude,$country);
                    if (!$getDestinationGoogle->isSuccess){
                        $tmp->error = 'Failed to Get Destination Locker Address. Please Try Again';
                        $destinationList[] = $tmp;
                        continue;
                    }
                    $addressList = $getDestinationGoogle->addressList;
                    // check based on list
                    if ($country == 'MY') {
                        $checkDestination = $this->saveDestinationMalaysia($addressList);
                    }
                    else {
                        $checkDestination = $this->checkDestinationAddress($addressList);
                    }
                    if (!$checkDestination->isSuccess){
                        $tmp->error = $checkDestination->errorMsg;
                        $destinationList[] = $tmp;
                        continue;
                    }
                    $tmp->districtId = (string)$checkDestination->districtId;
                }
                if (!empty($tmp->districtId)){
                    $tmp->isValid = true;
                }
                $destinationList[] = $tmp;
            }
        }
        if ($country == 'ID') {
            // check origin pickup
            $cityOrigin = Cache::get('express-city-origin',null);
            $popExpressAPI = new ApiPopExpress();
            if (empty($cityOrigin)){
                // get from API
                $getCity = $popExpressAPI->getCityOrigin();
                if (empty($getCity)){
                    $message = 'Failed Get City';
                    $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                    return response()->json($resp);
                }
                if ($getCity->response->code !=200){
                    $message = $getCity->response->message;
                    $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                    return response()->json($resp);
                }
                $cityOrigin = $getCity->data->origins;
                Cache::put('express-city-origin',$cityOrigin,3600);
            }
            if (empty($cityOrigin)){
                $errorMessage = 'Empty City Origin';
                return ApiHelper::buildResponse(400,$errorMessage);
            }

            $isOrigin = null;
            foreach ($cityOrigin as $item) {
                $tmpName = strtoupper($item->name);
                $tmpCity = strtoupper($pickupCity);
                if (strpos($tmpCity,$tmpName) !== false) {
                    $isOrigin = $item->name;
                    $pickupCode =$item->code;
                } elseif (strpos($tmpName,$tmpCity) !== false) {
                    $isOrigin = $item->name;
                    $pickupCode =$item->code;
                }
            }
            if (empty($isOrigin) && $pickupType == 'address'){
                $errorMessage = 'Origin Out of Coverage';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $pickupCity = $isOrigin;
        }

        // calculate tariff
        $original_price =0;
        $totalPrice = 0;
        $promoAmount = 0;
        $promoName = null;
        foreach ($destinationList as $item) {
            $item->regular_price = 0;
            $item->one_day_price = 0;
            $item->price = 0;
            $item->promo_amount = 0;


            if (!$item->isValid){
                continue;
            }
            $districtDb = District::with(['city','city.province'])->find($item->districtId);
            $districtCode = $districtDb->code;

            if ($country == 'MY'){
                $provinceDestination = $districtDb->city->province->province_name;
                $cityDestination = $districtDb->city->city_name;
                $districtDestination = $districtDb->district_name;
                // query from malaysia tariff
                $malaysiaTariffModel = new MalaysiaTariff();
                $calculate = $malaysiaTariffModel->calculateTariff($pickupCity,$provinceDestination,$cityDestination,$districtDestination,$item->weight);

                if (!$calculate->isSuccess){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Failed to Calculate Tariff';
                    }
                    continue;
                }


                $price = (float)$calculate->totalPrice;
                $promoPrice = 0;

                $item->price = $price;
                $item->promo_amount = 0;
                $item->regular_price = $price;
                $item->one_day_price = 0;
                $original_price +=$price;
                $totalPrice += $price;
                $promoAmount += $promoPrice;
            }
            else {
                $expressCalculate = $popExpressAPI->calculateByCode($pickupCity,$pickupCode,$item->destination_type,$districtCode,$item->destination_name);
                if (empty($expressCalculate)){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Failed to Calculate Tariff';
                    }
                    continue;
                }

                if ($expressCalculate->response->code != 200){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Failed to Calculate Tariff';
                    }
                    continue;
                }

                if (empty($expressCalculate->data)){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Out Of Coverage by PopExpress';
                    }
                    continue;
                }
                $tariffData = $expressCalculate->data->tariffs[0];

                // get regular tariff
                $regularPrice = 0;
                $oneDayPrice = 0;
                foreach ($tariffData as $tariffDatum) {
                    if ($tariffDatum->type == 'regular'){
                        $regularPrice = $tariffDatum->price;
                    }
                    if ($tariffDatum->type == 'one_day'){
                        $oneDayPrice = $tariffDatum->price;
                    }
                }
                // if regular price not found

                if (empty($regularPrice) && empty($oneDayPrice)){
                    $item->isValid = false;
                    if (empty($item->error)){
                        $item->error = 'Out Of Coverage Price by PopExpress';
                    }
                    continue;
                }
                $item->regular_price = $regularPrice;
                $item->one_day_price = $oneDayPrice;

                $selectedPrice = $regularPrice;
                $discountAmount = 0;
                $price = $item->weight * $selectedPrice;
                // calculate price and promo

                $item->promo_amount = $discountAmount;
                $original_price +=$price;
                $item->price = $price;
                $totalPrice += $price;
                $promoAmount += $discountAmount;
            }

        }

        /*---*/
        // ================ checking discount
        $promoCode = $request->input('promo_code');
        $phone = $userDb->phone;
        $params = [];
        $params['transaction_amount'] = $totalPrice;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['origin_locker_name'] = $pickupName;
        $params['item_amount'] = \count($destinationList) ;
        $params['promo_code'] = $promoCode;
        $params['user_phone'] = $phone;
        $params['transaction_type'] = 'popsend';
        $params['pickup_type'] = $pickupType;
        $params['origin_district_id'] = $pickupDistrictId;

        $campaignResp = Promo::checkPromo($phone,'potongan',$params);
        $campaignName = null;
        $discAmount = null;
        if ($campaignResp->isSuccess) {
            $totalPrice = $campaignResp->totalPrice;
            $promoName = $campaignResp->campaignName;
            $promoAmount = $campaignResp->totalDiscount;
        }
        // =============== end checking discount


        // create response
        $response = new \stdClass();
        $response->pickup_type = $pickupType;
        $response->pickup_name = $pickupName;
        $response->pickup_address = $pickupAddress;
        $response->pickup_latitude = $pickupLatitude;
        $response->pickup_longitude = $pickupLongitude;
        $response->districtId = (string)$pickupDistrictId;
        $response->original_price = $original_price;
        $response->total_price = $totalPrice;
        $response->promo_amount = $promoAmount;
        $response->promo_name = $promoName;
        $response->destinations = $destinationList;

        return ApiHelper::buildResponse(200,null,$response);
    }

    /**
     * Submit Order PopSend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function popSendSubmit(Request $request){
        $location = "";
        $errorMessage = null;
        $requiredInput = ['pickup_type','destinations'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        Logs::logFile($location,"popSendSubmit","Begin Order PopSend");

        // general rules for validation
        $rules = [
            'pickup_type' => 'required|in:locker,address',
            'sender_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'sender_phone' => 'required|numeric|digits_between:10,15',
            'pickup_notes' => 'nullable|string',
            'item_photo' => 'required',
            'pickup_date_time' => 'required|date',
            'destinations.*.type' => 'required|in:locker,address',
            'destinations.*.latitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.longitude' => 'required_if:destination_data.*.type,address',
            'destinations.*.locker_id' => 'required_if:destination_data.*.type,locker',
            'destinations.*.weight' => 'required|numeric',
            'destinations.*.recipient_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'destinations.*.recipient_phone' => 'required|numeric|digits_between:10,15',
            'destinations.*.category' => 'nullable|string',
            'destinations.*.notes' => 'nullable|string',
            'promo_code' => 'nullable|string'
        ];

        $pickupType = $request->input('pickup_type');
        $destinationInput = $request->input('destinations');

        // rule validation based on  pickup type
        if ($pickupType == 'locker'){
            $rules['locker_id'] = 'required';
        } else {
            $rules['pickup_latitude'] = 'required';
            $rules['pickup_longitude'] = 'required';
        }

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->erroMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUserId->userId;
        $userDb = User::find($userId);
        $country = $userDb->country;

        // validate input
        $pickupDateTime = $request->input('pickup_date_time');

        // validate pickup Date
        $nowDateTime = time();
        if (strtotime($pickupDateTime) < $nowDateTime){
            $errorMessage = 'Pickup Date Time must bigger than now';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // calculate price
        $calculate = $this->popSendCalculate($request)->getOriginalContent();

        if ($calculate->response->code!=200){
            $errorMessage = $calculate->response->message;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if (empty($calculate->data[0])){
            $errorMessage = 'Failed to Calculate Data';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $calculateData = $calculate->data[0];

        // get user based on session
        $sessionId = $request->input('session_id');
        $sessionDb = UserSession::where('session_id',$sessionId)->first();
        if (!$sessionDb){
            $errorMessage = 'Invalid Session User';
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $sessionDb->user_id;
        $user = User::find($userId);

        // DB Begin Transaction
        DB::beginTransaction();

        // create delivery order
        $deliveryModel = new Delivery();
        $createDelivery = $deliveryModel->createOrder($userId,$calculateData,$request);
        if (!$createDelivery->isSuccess){
            DB::rollback();
            $errorMessage = $createDelivery->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $deliveryId = $createDelivery->deliveryId;

        // save photo
        $pathPhoto = $createDelivery->pathPhoto;
        $itemPhoto = $request->input('item_photo');

        if ($pathPhoto){
            $structure = 'media_/popsend_/'.date('Y-M');
            if(!is_dir($structure)){
                mkdir($structure, 0777, true);
            }
            $decode = base64_decode($itemPhoto);
            file_put_contents($pathPhoto,$decode);
        }

        $params = [];
        $params['transaction_amount'] = $calculate->data[0]->original_price;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['origin_locker_name'] = $calculate->data[0]->pickup_name;
        $params['item_amount'] = \count($calculate->data[0]->destinations);
        $params['promo_code'] = $request->input('promo_code');
        $params['user_phone'] = $user->phone;
        $params['pickup_type'] = $pickupType;
        $params['transaction_type'] = 'popsend';
        $params['origin_district_id'] = $calculate->data[0]->districtId;
        $params['pickup_type'] = $request->input('pickup_type');

        $campaignResp = Promo::checkPromo($user->phone,'potongan',$params);
        $campaignName = null;
        $discAmount = null;

        $totalPrice = $calculateData->original_price;
        $promoName = null;
        $promoAmount = 0;

        if ($campaignResp->isSuccess) {
            $totalPrice = $campaignResp->totalPrice;
            $promoName = $campaignResp->campaignName;
            $promoAmount = $campaignResp->totalDiscount;
        }


        // submit transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createDeliveryTransaction($userId,$deliveryId,$promoAmount);

        if (!$createTransaction->isSuccess){
            DB::rollback();
            $errorMessage = $createTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $transactionId = $createTransaction->transactionId;

        // get transaction by Id
        $transactionDb = $transactionModel->find($transactionId);

        $paidAmount = $transactionDb->paid_amount;
        
        // debit transaction
        $balanceModel = new BalanceRecord();
        $debitBalance =$balanceModel->debitDeposit($userId,$paidAmount,$transactionId);

        if (!$debitBalance->isSuccess){
            DB::rollback();
            $errorMessage = $debitBalance->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $deliveryData = $deliveryModel->find($deliveryId);
        $destinations = $deliveryData->destinations;

        $params = [];
        $params['transaction_amount'] = $calculate->data[0]->original_price;
        $params['transaction_datetime'] = date('Y-m-d H:i:s');
        $params['locker_name'] = $calculate->data[0]->pickup_name;
        $params['item_amount'] = 1;
        $params['promo_code'] = $request->input('promo_code');
        $params['user_phone'] = $user->phone;
        $params['pickup_type'] = $request->input('pickup_type');

        $campaignResp = Promo::bookPromo($user->phone,'potongan',$transactionId,'',$params);

        $campaignResp = Promo::claimPromo($user->phone,$transactionId);

        // create response
        $destinationList = [];
        foreach ($destinations as $destination) {
            $tmp = new \stdClass();
            $qrCode = new QrCode();
            $qrCode
                ->setText($destination->invoice_sub_code)
                ->setSize(200)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(13)
                ->setImageType(QrCode::IMAGE_TYPE_PNG);

            $tmp->destination_type = $destination->destination_type;
            $tmp->sub_invoice_id = $destination->invoice_sub_code;
            $tmp->destination_name = $destination->destination_name;
            $tmp->destination_address = $destination->destination_address;
            $tmp->destination_latitude = $destination->destination_latitude;
            $tmp->destination_longitude = $destination->destination_longitude;
            $tmp->status = $destination->status;
            $tmp->isValid = true;
            $tmp->error = "";
            $tmp->districtId = $destination->district_id;
            $tmp->weight = (float)$destination->weight;
            $tmp->actual_weight = (float)$destination->actual_weight;
            $tmp->regular_price = 0;
            $tmp->one_day_price = 0;
            $tmp->price = $destination->price;
            $tmp->promo_amount = 0;
            $tmp->getContentType = $qrCode->getContentType();
            $tmp->QRcode = $qrCode->generate();

            $destinationList[] = $tmp;
        }

        // update calculate data
        $calculateData->pickup_date = $deliveryData->pickup_date;
        $calculateData->invoice_id = $deliveryData->invoice_code;
        $calculateData->destinations = $destinationList;
        //Commit Transaction After Success
        DB::commit();

//        dd($deliveryData);

        /*Send Email And SMS*/
        if ($country!='ID'){
            // process to prox
            $this->dispatch(new PushMalaysiaPopSendLocker($deliveryData));
        } else {
            Logs::logFile($location,"popSendSubmit","Begin dispatch PopExpress");
            $this->dispatch((new ProcessOderPopexpress($deliveryData,$destinations)));
        }

        Logs::logFile($location,"popSendSubmit","Begin dispatch sms");
        $this->dispatch((new SendEmailOrder($calculateData,$userId)));

        foreach ($calculateData->destinations as $destination) {
            unset($destination->getContentType);
            unset($destination->QRcode);
        }
        return ApiHelper::buildResponse(200,null,[$calculateData]);
    }

    /**
     * Get History Delivery
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popSendHistory (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['page'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'page' => 'required|integer'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //Get UserId from session_id
        $user = Helper::getUserId($request->input('session_id'));
        $userId = $user->user_id;
        //call query history data
        $deliveryModel = new Delivery();
        $popsendDb = $deliveryModel->historyDelivery($userId);

        if (!$popsendDb->isSuccess){
            $errorMessage = $popsendDb->errorMsg;
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }

        $data = $popsendDb->data;
        foreach ($data as $datum) {
            unset($datum->id);
            foreach ($datum->destinations as $item) {
                unset($item->id);
                unset($item->delivery_id);
                $item->weight = (float)$item->weight;
                $item->actual_weight = (float)$item->actual_weight;
            }
        }

        return ApiHelper::buildResponsePagination(200,null,$popsendDb->pagination,$popsendDb->data);
    }

    /**
     * Get History Detail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function popSendHistoryDetail (Request $request){
        $errorMessage = null;
        $requiredInput = ['invoice_id'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'invoice_id' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //Get UserId from session_id
        $user = Helper::getUserId($request->input('session_id'));
        $userId = $user->user_id;

        $invoiceCode = $request->input('invoice_id');
        //call query history data
        $deliveryModel = new Delivery();

        // get history
        $historyDetail = $deliveryModel->getHistoryDeliveryDetail($userId,$invoiceCode);
        if (!$historyDetail->isSuccess){
            $errorMessage = $historyDetail->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $historiesData = $historyDetail->data;
        unset($historiesData->id);
        unset($historiesData->user_id);
        unset($historiesData->district_id);
        // unset histories
        foreach ($historiesData->histories as $history) {
            unset($history->id);
            unset($history->delivery_id);
        }
        // unset destinations
        foreach ($historiesData->destinations as $destination) {
            unset($destination->id);
            unset($destination->delivery_id);
            unset($destination->district_id);
            unset($destination->express_url);
            unset($destination->express_parameter);
            // unset histories
            foreach ($destination->histories as $history) {
                unset($history->id);
                unset($history->delivery_destination_id);
            }
        }

        return ApiHelper::buildResponse(200,null,$historiesData);
    }

    /**
     * Callback for PopExpress
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function expressCallback(Request $request){
        $location = "";
        $errorMessage = null;
        $requiredInput = ['customer_order_no'];
        $response = new \stdClass();
        $response->code = 400;
        $response->message = $errorMessage;

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            $response->message = $errorMessage;
            return response()->json($response);
        }

        $subInvoiceCode = $request->input('customer_order_no');
        $awbNumber = $request->input('invnNo');
        $status = $request->input('status');
        $description = $request->input('description');
        $expressDate = $request->input('express_date');

        Logs::logFile($location,"expressCallback","Begin Express Callback");

        Logs::logFile($location,"expressCallback","Params: ".json_encode($request->input()));

        DB::beginTransaction();
        // update with sub invoice
        $destinationModel = new DeliveryDestination();
        $update = $destinationModel->updateDeliveryStatus($subInvoiceCode,$status,$description, $expressDate);
        if (!$update->isSuccess){
            DB::rollback();
            $errorMessage = $update->errorMsg;
            $response->message = $errorMessage;
            return response()->json($response);
        }

        DB::commit();
        $response->code = 200;
        $response->message = 'success';
        return response()->json($response);
    }


    /*=================Private Function=================*/

    /**
     * Get Address based on Latitude Longitude
     * @param $latitude
     * @param $longitude
     * @param string $country
     * @return \stdClass
     */
    private function getAddressByReverseGoogle($latitude, $longitude,$country = 'ID'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->address = null;
        $response->addressList = [];

        // reverse geocode
        $googleApi = new Google();
        $reverseGeocode = $googleApi->reverseGeocode($latitude,$longitude);
        if (empty($reverseGeocode)){
            $response->errorMsg = 'Failed to Get Geocode';
            return $response;
        }
        if ($reverseGeocode->status!='OK'){
            $response->errorMsg = 'Failed OK to Get Geocode';
            return $response;
        }
        $results = $reverseGeocode->results;

        $addressResult = $results[0];

        // get address results
        foreach ($results as $result) {
            $types = $result->types;
            if (in_array('street_address',$types)){
                $addressResult = $result;
            }
        }

        // get formatted address
        $addressName = $addressResult->formatted_address;

        $addressList = [];

        // get district, city, province name list
        foreach ($results as $result) {
            $addressComponent = $result->address_components;
            $tmp = new \stdClass();
            $tmp->district = null;
            $tmp->city = null;
            $tmp->province = null;

            foreach ($addressComponent as $value) {
                $types = $value->types;
                $name = $value->long_name;
                if ($country == 'MY') {
                    // insert into district
                    if (in_array('sublocality',$types)){
                        $tmp->district = $name;
                    }
                    // insert into city
                    if (in_array('locality',$types)){
                        $tmp->city = $name;
                    }
                    // insert into provinces
                    if (in_array('administrative_area_level_1',$types)){
                        $tmp->province = $name;
                    }
                } else {
                    // insert into district
                    if (in_array('administrative_area_level_3',$types)){
                        $tmp->district = $name;
                    }
                    // insert into city
                    if (in_array('administrative_area_level_2',$types)){
                        $tmp->city = $name;
                    }
                    // insert into provinces
                    if (in_array('administrative_area_level_1',$types)){
                        $tmp->province = $name;
                    }
                }
            }
            $addressList[] = $tmp;
        }

        $response->isSuccess = true;
        $response->address = $addressName;
        $response->addressList = $addressList;
        return $response;
    }

    /**
     * Get Pickup Address
     * @param array $addressList
     * @param string $country
     * @return \stdClass
     */
    private function checkPickupAddress($addressList = [],$country='ID'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->districtId = null;
        $response->districtCode = null;
        $response->cityId = null;

        $districtId = null;
        if ($country == 'MY'){
            foreach ($addressList as $item) {
                $districtName = $item->district;
                $cityName = $item->city;
                $provinceName = $item->province;
                if (empty($cityName) || empty($provinceName)) continue;

                $checkAddress = City::with('province')
                    ->where('city_name',$cityName)
                    ->whereHas('province',function ($query) use($provinceName){
                        $query->where('province_name',$provinceName);
                    })->first();
                if ($checkAddress){
                    $districtId = $checkAddress->id;
                    break;
                }
            }
        }
        else {
            foreach ($addressList as $item) {
                $districtName = $item->district;
                $cityName = $item->city;
                $provinceName = $item->province;
                if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

                // check on existing db
                $checkAddress = District::with('city','city.province')
                    ->where('district_name',$districtName)
                    ->whereHas('city',function ($query) use ($cityName){
                        $query->where('city_name',$cityName);
                    })->whereHas('city.province',function ($query) use ($provinceName){
                        $query->where('province_name',$provinceName);
                    })->first();

                if ($checkAddress){
                    $districtId = $checkAddress->id;
                    break;
                }

                // query on  google location existing database
                $checkGoogleDb = GoogleLocation::where('district',$districtName)
                    ->where('city',$cityName)
                    ->where('province',$provinceName)
                    ->first();
                if ($checkGoogleDb){
                    if (!empty($checkGoogleDb->district_id)){
                        $districtId = $checkGoogleDb->district_id;
                        break;
                    } else {
                        break;
                    }
                }

                // check based on city
                $tmpCity = $cityName;
                if (strpos($tmpCity, 'Jakarta') !== false) {
                    $tmpCity = 'Jakarta';
                }
                if (strpos($tmpCity, 'Kota ') !== false) {
                    $tmpCity = str_replace('Kota ','',$tmpCity);
                }

                if (strpos($tmpCity, 'Kabupaten ') !== false) {
                    $tmpCity = str_replace('Kabupaten ','',$tmpCity);
                }

                if (strpos($tmpCity, ' City') !== false) {
                    $tmpCity = str_replace(' City','',$tmpCity);
                }

                if (strpos($tmpCity, ' Regency') !== false) {
                    $tmpCity = str_replace(' Regency','',$tmpCity);
                }

                // check district
                $tmpDistrict = $districtName;
                if (strpos($tmpDistrict,'Kecamatan') !== false){
                    $tmpDistrict = str_replace('Kecamatan ','',$tmpDistrict);
                }
                if (strpos($tmpDistrict,'tanahabang') !== false && $tmpCity == 'Jakarta'){
                    $tmpDistrict = 'Tanah Abang';
                }
                if (strpos($tmpDistrict,'Grogol petamburan') !== false && $tmpCity == 'Jakarta'){
                    $tmpDistrict = 'Grogol';
                }

                $districtDb = District::with('city')
                    ->where('district_name','LIKE',$tmpDistrict)
                    ->whereHas('city',function ($query) use ($tmpCity){
                        $query->where('city_name','LIKE',"%$tmpCity%");
                    })
                    ->get();
                if (count($districtDb) == 1){
                    $districtDb = $districtDb->first();
                    $districtId = $districtDb->id;
                }

                // insert into db
                $googleDb = new GoogleLocation();
                $googleDb->district = $districtName;
                $googleDb->city = $cityName;
                $googleDb->province = $provinceName;
                $googleDb->district_id = $districtId;
                $googleDb->save();
            }
        }


        if (empty($districtId)){
            $response->errorMsg = 'Out Of Coverage';
            return $response;
        }

        // check if active pickup Delivery
        $districtDb = District::with('city','city.province')
            ->find($districtId);

        $districtPickup = $districtDb->pickup_available;
        $cityPickup = $districtDb->city->pickup_available;
        $provincePickup = $districtDb->city->province->pickup_available;
        if (!empty($districtPickup)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
            return $response;
        }
        if (!empty($cityPickup)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
            return $response;
        }
        if (!empty($provincePickup)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
            return $response;
        }

        $response->errorMsg = 'Out of Coverage';
        return $response;
    }

    /**
     * Check Destination Address
     * @param array $addressList
     * @param string $country
     * @return \stdClass
     */
    private function checkDestinationAddress($addressList = [],$country='ID'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->districtId = null;

        $districtId = null;
        if ($country == 'MY'){
            foreach ($addressList as $item) {
                $districtName = $item->district;
                $cityName = $item->city;
                $provinceName = $item->province;
                if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

                // check on existing db
                $checkAddress = District::with('city','city.province')
                    ->where('district_name',$districtName)
                    ->whereHas('city',function ($query) use ($cityName){
                        $query->where('city_name',$cityName);
                    })->whereHas('city.province',function ($query) use ($provinceName){
                        $query->where('province_name',$provinceName);
                    })->first();

                if ($checkAddress){
                    $districtId = $checkAddress->id;
                    break;
                }

                // query on  google location existing database
                $checkGoogleDb = GoogleLocation::where('district',$districtName)
                    ->where('city',$cityName)
                    ->where('province',$provinceName)
                    ->first();
                if ($checkGoogleDb){
                    if (!empty($checkGoogleDb->district_id)){
                        $districtId = $checkGoogleDb->district_id;
                        break;
                    } else {
                        break;
                    }
                }

                // check based on city
                $tmpCity = $cityName;

                $districtDb = District::with('city')
                    ->where('district_name','LIKE',$districtName)
                    ->whereHas('city',function ($query) use ($tmpCity){
                        $query->where('city_name','LIKE',"%$tmpCity%");
                    })
                    ->get();
                if (count($districtDb) == 1){
                    $districtDb = $districtDb->first();
                    $districtId = $districtDb->id;
                }

                // insert into db
                $googleDb = new GoogleLocation();
                $googleDb->district = $districtName;
                $googleDb->city = $cityName;
                $googleDb->province = $provinceName;
                $googleDb->district_id = $districtId;
                $googleDb->save();
            }
        }
        // indonesia
        else {
            foreach ($addressList as $item) {
                $districtName = $item->district;
                $cityName = $item->city;
                $provinceName = $item->province;
                if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

                // check on existing db
                $checkAddress = District::with('city','city.province')
                    ->where('district_name',$districtName)
                    ->whereHas('city',function ($query) use ($cityName){
                        $query->where('city_name',$cityName);
                    })->whereHas('city.province',function ($query) use ($provinceName){
                        $query->where('province_name',$provinceName);
                    })->first();

                if ($checkAddress){
                    $districtId = $checkAddress->id;
                    break;
                }

                // query on  google location existing database
                $checkGoogleDb = GoogleLocation::where('district',$districtName)
                    ->where('city',$cityName)
                    ->where('province',$provinceName)
                    ->first();
                if ($checkGoogleDb){
                    if (!empty($checkGoogleDb->district_id)){
                        $districtId = $checkGoogleDb->district_id;
                        break;
                    } else {
                        break;
                    }
                }

                // check based on city
                $tmpCity = $cityName;
                if (strpos($tmpCity, 'Jakarta') !== false) {
                    $tmpCity = 'Jakarta';
                }
                if (strpos($tmpCity, 'Kota ') !== false) {
                    $tmpCity = str_replace('Kota ','',$tmpCity);
                }

                if (strpos($tmpCity, 'Kabupaten ') !== false) {
                    $tmpCity = str_replace('Kabupaten ','',$tmpCity);
                }

                if (strpos($tmpCity, ' City') !== false) {
                    $tmpCity = str_replace(' City','',$tmpCity);
                }

                if (strpos($tmpCity, ' Regency') !== false) {
                    $tmpCity = str_replace(' Regency','',$tmpCity);
                }

                // check district
                $tmpDistrict = $districtName;
                if (strpos($tmpDistrict,'Kecamatan') !== false){
                    $tmpDistrict = str_replace('Kecamatan ','',$tmpDistrict);
                }
                if (strpos($tmpDistrict,'tanahabang') !== false && $tmpCity == 'Jakarta'){
                    $tmpDistrict = 'Tanah Abang';
                }
                if (strpos($tmpDistrict,'Grogol petamburan') !== false && $tmpCity == 'Jakarta'){
                    $tmpDistrict = 'Grogol';
                }

                $districtDb = District::with('city')
                    ->where('district_name','LIKE',$tmpDistrict)
                    ->whereHas('city',function ($query) use ($tmpCity){
                        $query->where('city_name','LIKE',"%$tmpCity%");
                    })
                    ->get();
                if (count($districtDb) == 1){
                    $districtDb = $districtDb->first();
                    $districtId = $districtDb->id;
                }

                // insert into db
                $googleDb = new GoogleLocation();
                $googleDb->district = $districtName;
                $googleDb->city = $cityName;
                $googleDb->province = $provinceName;
                $googleDb->district_id = $districtId;
                $googleDb->save();
            }
        }
        if (empty($districtId)){
            $response->errorMsg = 'Out Of Coverage';
            return $response;
        }

        // check if active pickup Delivery
        $districtDb = District::with('city','city.province')
            ->find($districtId);

        $districtDelivery = $districtDb->delivery_available;
        $cityDelivery = $districtDb->city->delivery_available;
        $provinceDelivery = $districtDb->city->province->delivery_available;

        if (!empty($districtDelivery)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
            return $response;
        }
        if (!empty($cityDelivery)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
            return $response;
        }
        if (!empty($provinceDelivery)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
            return $response;
        }

        $response->errorMsg = 'Out of Coverage';
        return $response;
    }

    /**
     * @param $addressList
     * @return \stdClass
     */
    private function saveDestinationMalaysia($addressList){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->districtId = null;

        foreach ($addressList as $value) {
            $districtName = $value->district;
            $cityName = $value->city;
            $provinceName = $value->province;

            if (empty($districtName) || empty($cityName) || empty($provinceName)) continue;

            $countryDb = Country::where('country_name','Malaysia')->first();
            if (!$countryDb){
                $countryDb = new Country();
                $countryDb->country_name = 'Malaysia';
                $countryDb->save();
            }
            $countryId = $countryDb->id;
            $provinceDb = Province::where('province_name',$provinceName)->where('country_id',$countryId)->first();
            if (!$provinceDb){
                $provinceDb = new Province();
                $provinceDb->province_name = $provinceName;
                $provinceDb->country_id = $countryId;
                $provinceDb->save();
            }
            $provinceId = $provinceDb->id;

            $cityDb = City::where('city_name',$cityName)->where('province_id',$provinceId)->first();
            if (!$cityDb){
                $cityDb = new City();
                $cityDb->city_name = $cityName;
                $cityDb->province_id = $provinceId;
                $cityDb->save();
            }
            $cityId = $cityDb->id;

            $districtDb = District::where('district_name',$districtName)->where('city_id',$cityId)->first();
            if (!$districtDb){
                $districtDb = new District();
                $districtCode = "MY".Helper::generateRandomString(3,'Numeric');
                $districtDb->city_id = $cityId;
                $districtDb->code = $districtCode;
                $districtDb->district_name = $districtName;
                $districtDb->save();
            }

            $districtId = $districtDb->id;
        }
        if (!empty($districtId)){
            $response->isSuccess = true;
            $response->districtId = $districtId;
        }

        return $response;
    }
}

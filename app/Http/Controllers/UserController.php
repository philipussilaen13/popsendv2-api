<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 27/12/2017
 * Time: 10.59
 */

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Library\CurlHelper;
use App\Http\Library\Helper;
use App\Http\Models\DeliveryDestination;
use App\Http\Models\PopboxV1\LockerActivity;
use App\Http\Models\PopboxV1\LockerReturn;
use App\Http\Models\PopboxV1\Member;
use App\Http\Models\PopboxV1\MemberPickup;
use App\Http\Models\PopboxV1\Order;
use App\Http\Models\ReferralTransaction;
use App\Http\Models\UserDevice;
use App\Http\Models\UserSession;
use App\Http\Models\PasswordReset;
use App\Http\Models\UserSocial;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Check Member exist or not
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCheckMember(Request $request){
        $requiredInput = ['phone','email'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $phone = $request->input('phone');
        $email = $request->input('email');

        $rules = [];
        $rules['email'] = 'required|email';
        $rules['phone'] = 'required|numeric|digits_between:10,15';

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $isExist = false;

        $phoneValid = User::where('phone',$phone)
            ->first();

        if ($phoneValid){
           $isExist = true;
        }

        $emailValid = User::where('email',$email)
            ->first();

        if ($emailValid){
            $isExist = true;
        }

        if (!$isExist){
            $errorMessage = 'Member Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if ($phoneValid){
            $memberId = $phoneValid->member_id;
        } else {
            $memberId = $emailValid->member_id;
        }

        // get member Profile
        $userDb = User::getUserProfileByMemberId($memberId);
        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $userDb->data;
        return ApiHelper::buildResponse(200,'Phone and Email already Exist',$data);
    }

    /**
     * Post Register Agent
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegister(Request $request){
        $requiredInput = ['name','phone','email','password','re_password','country'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create validation
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|min:3',
            'phone' => 'required|numeric|digits_between:10,15',
            'email' => 'required|email',
            'password' => 'required|string|min:6',
            're_password' => 'required|same:password',
            'country' => 'required|in:ID,MY'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $password = $request->input('password');
        $country = $request->input('country','ID');
        $referralCode = $request->input('referral_code',null);

        // check on DB first
        $userDb = User::where('phone',$phone)
            ->first();

        if ($userDb){
            $errorMessage = 'Phone already Taken';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userDb = User::where('email',$email)
            ->first();
        if ($userDb){
            $errorMessage = 'Email Already Taken';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // insert to DB
        DB::beginTransaction();

        // check referral if user inputted referral code
        $referralCampaignId = null;
        if (!empty($referralCode)){
            #initiate model referral transaction
            $referralTransactionModel = new ReferralTransaction();


            // check active referral
            $activeReferral = $referralTransactionModel->checkActiveCampaign($referralCode);
            if (!$activeReferral->isSuccess){
                DB::rollback();
                $errorMessage = $activeReferral->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $referralCampaignId = $activeReferral->campaignId;
        }

        $userDb = User::insertNewUser($name,$phone,$email,$password,$country);
        if (!$userDb->isSuccess){
            DB::rollback();
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $memberId = $userDb->memberId;

        // create referral transaction if referral campaign id is not null
        if (!empty($referralCampaignId)){
            $createReferralTransaction = $referralTransactionModel->createReferralTransaction($referralCode,$referralCampaignId,$memberId);
            if (!$createReferralTransaction->isSuccess){
                $errorMessage = $createReferralTransaction->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }
        
        DB::commit();

        // get member Profile
        $userDb = User::getUserProfileByMemberId($memberId);
        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $userDb->data;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Login
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogin(Request $request){
        $errorMessage = null;
        $requiredInput = ['phone_email','password','device_type','gcm_token','device_id','device_name'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userInput = $request->input('phone_email');

        $field = filter_var($userInput,FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $rules = [
            'password' => 'required|string|min:6',
            'device_type' => 'required|in:android,ios',
            'gcm_token' => 'required',
            'device_id' => 'required',
            'device_name' => 'required'
        ];
        if ($field == 'email'){
            $rules['email'] = 'required|email';
        }
        else {
            $rules['phone'] = 'required|numeric|digits_between:10,15';
        }

        $input = $request->except('phone_email');
        $input[$field] = $userInput;

        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $password = $request->input('password');
        $token = $request->input('token');
        $deviceType = $request->input('device_type');
        $gcmToken = $request->input('gcm_token');
        $deviceId = $request->input('device_id');
        $deviceName = $request->input('device_name');

        // get user db
        $userDb = User::where($field,$userInput)->first();
        DB::beginTransaction();
        DB::connection('popbox_db')->beginTransaction();
        if (!$userDb){
            // Check On Member PopSend V1
            $memberModel = new Member();
            $checkMember = $memberModel->checkMember($field,$userInput,$password);
            if (!$checkMember->isSuccess){
                $errorMessage = $checkMember->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $oldMemberId = $checkMember->memberId;

            /*Process Migrate All Data From V1 to V2 with new Hash Encryption Password*/
            $migrateMember = $memberModel->migrateMember($oldMemberId,$password);
            if (!$migrateMember->isSuccess){
                $errorMessage = $migrateMember->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $memberId = $migrateMember->memberId;
            $userDb = User::where('member_id',$memberId)->first();
            if (!$userDb){
                $errorMessage = 'Invalid User M';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }

        if ($userDb->status != 'ACTIVE'){
            $errorMessage = 'Your Account is '.$userDb->status;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // check password
        if (!Hash::check($password,$userDb->password)){
            $errorMessage = 'Invalid Credential';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $memberId = $userDb->member_id;

        # create user session
        $userSessionDb = UserSession::createUserSession($token,$memberId);
        if (!$userSessionDb->isSuccess){
            DB::rollback();
            $errorMessage = $userSessionDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $sessionId = $userSessionDb->sessionId;

        # save user devices
        $userDeviceDb = UserDevice::insertUserDevices($memberId,$deviceType,$gcmToken,$deviceId,$deviceName);
        if (!$userDeviceDb->isSuccess){
            DB::rollback();
            $errorMessage = $userSessionDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::commit();
        DB::connection('popbox_db')->commit();


        // get member Profile
        $userDb = User::getUserProfileByMemberId($memberId);
        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $userDb->data;
        $data->session_id = $sessionId;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Edit Member Profile
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEditProfile(Request $request){
        $requiredInput = ['name','phone','member_id','password','image_ktp','favorite_locker'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create validation
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|min:3',
            'phone' => 'required|numeric|digits_between:10,15',
            'image_ktp' => 'base64'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        
        $path_avatar_name = '';
        if($request->input('avatar') !=''){
            $output_file_name = "avatar_".date('y-m-d').time()."_.jpg";
            $structure = 'media_/members/avatar/'.date('Y-M');
            if(!is_dir($structure)){
                mkdir($structure, 0777, true);
            }

            $path_avatar_name = $structure."/".$output_file_name;

            $decode = base64_decode($request->input('avatar'));
            file_put_contents($path_avatar_name,$decode);
        }

        $path_ktp_name = '';
        if($request->input('image_ktp') !=''){
            $output_file_name = "ktp_".date('y-m-d').time()."_.jpg";
            $structure = 'media_/members/ktp/'.date('Y-M');
            if(!is_dir($structure)){
                mkdir($structure, 0777, true);
            }

            $path_ktp_name = $structure."/".$output_file_name;

            $decode = base64_decode($request->input('image_ktp'));
            file_put_contents($path_ktp_name,$decode);
        }

        $name = $request->input('name');
        $member_id = $request->input('member_id');
        $phone = $request->input('phone');
        $password = $request->input('password');
        $favorite_locker = $request->input('favorite_locker');
        $avatar = $path_avatar_name;
        $image_ktp= $path_ktp_name;

        $userDb = User::updateUser($name,$member_id,$phone,$password,$favorite_locker,$avatar,$image_ktp);

        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $memberId = $userDb->member_id;

        // get member Profile
        $userDb = User::getUserProfileByMemberId($memberId);
        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $userDb->data;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Get Referral Code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReferralCode(Request $request) {
        $user = User::getReferralCode($request->member_id);
        if (!$user->isSuccess){
            $errorMessage = $user->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $user->data;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Forgot Password by Email or Phone Number
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['phone_email'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userInput = $request->input('phone_email');

        $field = filter_var($userInput,FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        if ($field == 'email'){
            $rules['email'] = 'required|email';
        }
        else {
            $rules['phone'] = 'required|numeric|digits_between:10,15';
        }

        $input = $request->except('phone_email');
        $input[$field] = $userInput;

        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // get user db
        $userDb = User::where($field,$userInput)->first();
        // generate string password
        $stringPassword = Helper::generateRandomString(6);

        DB::beginTransaction();
        DB::connection('popbox_db')->beginTransaction();

        if (!$userDb){
            $memberModel = new Member();
            $checkMember = $memberModel->checkMember($field,$userInput);
            if (!$checkMember->isSuccess){
                $errorMessage = $checkMember->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $oldMemberId = $checkMember->memberId;

            /*Process Migrate All Data From V1 to V2 with new Hash Encryption Password*/
            $migrateMember = $memberModel->migrateMember($oldMemberId,$stringPassword);
            if (!$migrateMember->isSuccess){
                $errorMessage = $migrateMember->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $memberId = $migrateMember->memberId;
            $userDb = User::where('member_id',$memberId)->first();
            if (!$userDb){
                $errorMessage = 'Invalid User M';
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }

        if ($userDb->status != 'ACTIVE'){
            $errorMessage = 'Your Account is '.$userDb->status;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //generate and save code before send to user
        $code = Helper::generateRandomString(6);
        $resetCodeDb = PasswordReset::insertCode($code,$userDb);

        if (!$resetCodeDb->isSuccess){
            $errorMessage = 'Please Try Again';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $url = url('user/changePwd')."/".$resetCodeDb->urlCode;
        $userName = $userDb->name;
        $stringName = substr($userName,0,20);

        $data = [
            'url' => $url,
            'urlCode' => $resetCodeDb->urlCode,
            'code' => $resetCodeDb->code,
            'email' => $userDb->email,
            'name' => $userDb->name,
            'phone' => $userDb->phone
        ];

        DB::commit();
        DB::connection('popbox_db')->commit();

        if($userDb->country == 'ID'){
            //send email code verification
            Mail::send('email.general.forgotpassword',$data, function($message) use ($data){
                $message->subject('Forgot Password');
                $message->to( $data['email'],$data['name']);
                $message->from('admin@popbox.asia','POPBOX ASIA');
            });

            //Send SMS
            $endpintApiSms = env('URL_INTERNAL').'/sms/send';
            $dataSms = [
                'to' => $userDb->phone,
                'message' => 'Hi '.$stringName.', berikut kode unik untuk ubah kata sandi Anda : ' .$data['code']. " valid sampai 2 jam.",
                'token' => env('TOKEN_INTERNAL')
            ];
        }else {
            //send email code verification
            Mail::send('email.general.forgotpassword_my',$data, function($message) use ($data){
                $message->subject('Forgot Password');
                $message->to( $data['email'],$data['name']);
                $message->from('admin@popbox.asia','POPBOX ASIA');
            });

            //Send SMS
            $endpintApiSms = env('URL_INTERNAL').'/sms/send';
            $dataSms = [
                'to' => $userDb->phone,
                'message' => 'Hi '.$stringName.', here unique code to change your password: ' .$data['code']." valid until 2 hours.",
                'token' => env('TOKEN_INTERNAL')
            ];
        }

        CurlHelper::callCurl($endpintApiSms,$dataSms,'POST');

        return ApiHelper::buildResponse(200,'success');
    }

    /**
     * Web Version of Get Update Password page
     * @param Request $request
     * @param null $urlCode
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function getUpdatePassword(Request $request,$urlCode=null){
        $data = [];
        $data['secret'] = '';
        $data['error'] = null;
        if (empty($urlCode)) {
            $data['error'] = 'Invalid';
            return view('user.reset',$data);
        }

        // check code on password reset
        $passwordResetDb =  PasswordReset::where('url_code',$urlCode)->first();
        if (!$passwordResetDb){
            $data['error'] = 'Invalid Code';
            return view('user.reset',$data);
        }
        $status = $passwordResetDb->status;
        if ($status != 0){
            $data['error'] = 'Invalid Active Code';
            return view('user.reset',$data);
        }

        // create secret
        $secret = encrypt($passwordResetDb->code."|".$urlCode);

        $data = [];
        $data['code'] = $passwordResetDb->code;
        $data['urlCode'] = $urlCode;
        $data['secret'] = $secret;

        return view('user.reset',$data);
    }

    /**
     * Web Version of Post Update Password Page
     * @param Request $request
     * @param null $urlCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdatePassword(Request $request, $urlCode = null){
        $data = [];
        $data['secret'] = '';
        $data['error'] = null;

        if (empty($urlCode)) {
            $data['error'] = 'Invalid';
            return view('user.reset',$data);
        }
        // check code on password reset
        $passwordResetDb =  PasswordReset::where('url_code',$urlCode)->first();
        if (!$passwordResetDb){
            $data['error'] = 'Invalid Code';
            return view('user.reset',$data);
        }
        $status = $passwordResetDb->status;
        if ($status != 0){
            $data['error'] = 'Invalid Active Code';
            return view('user.reset',$data);
        }

        // get secret
        $secret = $request->input('secret');

        // decrypt secret
        $secret = decrypt($secret);
        list($code,$secretUrl) = explode('|',$secret);

        if ($urlCode != $secretUrl) {
            $data['error'] = 'Failed Codes';
            return view('user.reset',$data);
        }

        if ($code != $passwordResetDb->code){
            $data['error'] = 'Failed Code';
            return view('user.reset',$data);
        }

        // create param from update password
        $param = [];
        $param['token'] = env('API_TOKEN');
        $param['code'] = $code;
        $param['password'] = $request->input('password');
        $param['re_password'] =  $request->input('rePassword');

        $url = url('user/update-password');

        // post to API
        $post = CurlHelper::callCurl($url,$param,'POST');
        if (empty($post)){
            $data['error'] = 'Failed Reset Password';
            return view('user.reset',$data);
        }

        if ($post->response->code!=200){
            $data['error'] = $post->response->message;
            return view('user.reset',$data);
        }

        $data['success'] ='Success Reset Password';
        return view('user.reset',$data);
    }

    /**
     * UpDate Password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['code','password','re_password'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create validation
        $rules = [
            'code' => 'required|string|size:6',
            'password' => 'required|string|min:6',
            're_password' => 'required|same:password',
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //check code
        $code = $request->input('code');
        $checkCode = PasswordReset::checkCode($code);
        if(!$checkCode->isSuccess){
            $errorMessage = $checkCode->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //update password
        $userId = $checkCode->user_id;
        $password = $request->input('password');
        $userDb = User::updatePassword($userId,$password,$code);
        if(!$userDb->isSuccess){
            $errorMessage = 'Update Password Failed';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        return ApiHelper::buildResponse(200,'update password success',[$userDb->data]);
    }

    /**
     * Login Social Media
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function loginSocialMedia (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['provider_token','provider','device_type','gcm_token','device_id','device_name'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create validation
        $rules = [
            'provider_token' => 'required',
            'provider' => 'required|in:facebook,google',
            'device_type' => 'required|in:android,ios',
            'gcm_token' => 'required',
            'device_id' => 'required',
            'device_name' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $provider = $request->input('provider');
        $providerToken = $request->input('provider_token');
        $token = $request->input('token');
        $deviceType = $request->input('device_type');
        $gcmToken = $request->input('gcm_token');
        $deviceId = $request->input('device_id');
        $deviceName = $request->input('device_name');

        if ($provider == 'facebook') {
            $fb = Helper::facebookSdk();
            try {
                $response = $fb->get(
                    '/me',
                    $providerToken);
            }  catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                $errorMessage =  'Graph returned an error: ' . $e->getMessage();
                return ApiHelper::buildResponse(400,$errorMessage);
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                $errorMessage =  'Facebook SDK returned an error: ' . $e->getMessage();
                return ApiHelper::buildResponse(400,$errorMessage);
            }

            // Convert the response to a `Facebook/GraphNodes/GraphUser` collection

            $facebook_user = $response->getGraphUser();
            $socialMediaId = $facebook_user->getId();
            $socialMediaName = $facebook_user->getName();
            $socialMediaEmail = $facebook_user->getEmail();
        }else{
            $CLIENT_ID = env('GOOGLE_OAUTH_ID');
            $client = new \Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
            $payload = $client->verifyIdToken($providerToken);
            if (!$payload) {
                $errorMessage = 'Invalid Google Token';
                return ApiHelper::buildResponse(400,$errorMessage);
            }

            $socialMediaId = $payload['sub'];
            $socialMediaName = $payload['name'];
            $socialMediaEmail = $payload['email'];
        }

        //check existing id social media
        $userSosial = new UserSocial();
        $userIdSocialExist = $userSosial->checkExistingUser($provider,$socialMediaId);
        //if social id not found return to client to prepare register
        if (!$userIdSocialExist->isFound) {
            $me[] = [
                'member_id' => "",
                'name' => $socialMediaName,
                'phone' => "",
                'email' => $socialMediaEmail,
                'address' => "",
                'status' => "",
                'country' => "",
                'balance' => 0,
                'referral_code' => "",
                'id_number' => "",
                'description' => "",
                'description_to_shared' => "",
                'provider' => $provider,
                'id_provider' => $socialMediaId,
                'session_id' => ""
            ];
            return ApiHelper::buildResponse(200,null,$me);
        }

        //check table user
        $userDb = User::find($userIdSocialExist->user_id);
        if (!$userDb){
            $errorMessage = 'User Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        if ($userDb->status != 'ACTIVE'){
            $errorMessage = 'Your Account is '.$userDb->status;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $memberId = $userDb->member_id;
        //if social id found, login function
        # create user session
        DB::beginTransaction();
        $userSessionDb = UserSession::createUserSession($token,$memberId);
        if (!$userSessionDb->isSuccess){
            DB::rollback();
            $errorMessage = $userSessionDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $sessionId = $userSessionDb->sessionId;

        # save user devices
        $userDeviceDb = UserDevice::insertUserDevices($memberId,$deviceType,$gcmToken,$deviceId,$deviceName);
        if (!$userDeviceDb->isSuccess){
            DB::rollback();
            $errorMessage = $userSessionDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::commit();

        // get member Profile
        $userDb = User::getUserProfileByMemberId($memberId);
        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $userDb->data;
        $data->provider = "";
        $data->id_provider = "";
        $data->session_id = $sessionId;
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Register Social Media
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerSocialMedia (Request $request)
    {
        $requiredInput = ['provider_id','provider_name','name','phone','email','country'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create validation
        $rules = [
            'provider_id' => 'required|min:14',
            'provider_name' => 'required|in:facebook,google',
            'name' => 'required|regex:/^[\pL\s\-]+$/u|min:3',
            'phone' => 'required|numeric|digits_between:10,15',
            'email' => 'required|email',
            'country' => 'required|in:ID,MY'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $provider_id = $request->input('provider_id');
        $provider_name = $request->input('provider_name');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $password = null;
        $country = $request->input('country','ID');
        $referralCode = $request->input('referral_code',null);

        // check on DB first
        $userDb = User::where('phone',$phone)
            ->first();

        if ($userDb){
            $errorMessage = 'Phone already Taken';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $userDb = User::where('email',$email)
            ->first();
        if ($userDb){
            $errorMessage = 'Email Already Taken';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // insert to DB
        DB::beginTransaction();

        // check referral if user inputted referral code
        $referralCampaignId = null;
        if (!empty($referralCode)){
            #initiate model referral transaction
            $referralTransactionModel = new ReferralTransaction();

            // check active referral
            $activeReferral = $referralTransactionModel->checkActiveCampaign($referralCode);
            if (!$activeReferral->isSuccess){
                DB::rollback();
                $errorMessage = $activeReferral->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
            $referralCampaignId = $activeReferral->campaignId;
        }

        $userDb = User::insertNewUser($name,$phone,$email,$password,$country);
        if (!$userDb->isSuccess){
            DB::rollback();
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $memberId = $userDb->memberId;
        $userId = $userDb->id;

        // create referral transaction if referral campaign id is not null
        if (!empty($referralCampaignId)){
            $createReferralTransaction = $referralTransactionModel->createReferralTransaction($referralCode,$referralCampaignId,$memberId);
            if (!$createReferralTransaction->isSuccess){
                $errorMessage = $createReferralTransaction->errorMsg;
                return ApiHelper::buildResponse(400,$errorMessage);
            }
        }

        //insert user social id
        $userMediaDb = UserSocial::insertSocialMedia($userId,$provider_id,$provider_name);

        if (!$userMediaDb->isSuccess){
            DB::rollback();
            $errorMessage = $createReferralTransaction->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        DB::commit();

        // get member Profile
        $userDb = User::getUserProfileByMemberId($memberId);
        if (!$userDb->isSuccess){
            $errorMessage = $userDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $userDb->data;
        return ApiHelper::buildResponse(200,null,$data);

    }

    /**
     * Check FCM Code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkFcmCode (Request $request)
    {
        $requiredInput = ['fcm'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //check avaliable fcm in UserDevice
        $userSession = Helper::getUserId($request->input('session_id'));
        $fcm = $request->input('fcm');
        $userFcmDb = UserDevice::where('user_id','=',$userSession->user_id)->orderBy('updated_at','desc')->first();

        if (!$userFcmDb) {
            return ApiHelper::buildResponse(400,'Token Not Found');
        }

        if (!empty($fcm)) {
            // update with new fcm token
            $userFcmDb->gcm_token = $fcm;
            $userFcmDb->save();
        }
        $data[] = [
            'fcm_token' => $userFcmDb->gcm_token,
            'last_update' => date("Y-m-d H:i:s",strtotime($userFcmDb->updated_at)),
            'device_name' => $userFcmDb->name
        ];
        return ApiHelper::buildResponse(200,null,$data);
    }

    /**
     * Parcel For You
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function orderHistory(Request $request){
        $errorMessage = null;
        $requiredInput = ['session_id'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $pageNumber = $request->input('page',1);
        $status = $request->input('status',null);
        $isComplete = $request->input('is_complete',false);

        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->erroMsg;
            return ApiHelper::buildResponsePagination(400,$errorMessage);
        }
        $userId = $getUserId->userId;
        $userDb = User::find($userId);
        $userPhone = $userDb->phone;

        $statusList = ['ON PROCESS','READY FOR PICKUP','COMPLETED','OVERDUE'];

        if (!empty($status)){
            if (!in_array($status,$statusList)){
                $errorMessage = 'Invalid Status';
                return ApiHelper::buildResponsePagination(400,$errorMessage);
            }
        }

        $data = [];
        $orderNumber = [];
        // get PopSend v1
        $memberPickupModel = new MemberPickup();
        $orderList = $memberPickupModel->getRecipientOrder($userPhone);

        $orderData = $orderList->data;
        foreach ($orderData as $item){
            $data[] = $item;
            $orderNumber[] = $item->order_number;
        }

        // get PopSend v2
        $deliveryDestinationModel =  new DeliveryDestination();
        $deliveryList = $deliveryDestinationModel->getList($userPhone,$orderNumber);
        $deliveryData = $deliveryList->data;
        foreach ($deliveryData as $deliveryDatum) {
            $data[] = $deliveryDatum;
            $orderNumber[] = $deliveryDatum->order_number;
        }

        // get PopShop Order
        $popShopModel = new Order();
        $popShopList = $popShopModel->getMemberOrder($userPhone);
        $popShopData = $popShopList->data;
        foreach ($popShopData as $item){
            $data[] = $item;
            $orderNumber[] = $item->order_number;
        }

        // get locker activity
        $lockerActivityModel = new LockerActivity();
        $activityList = $lockerActivityModel->getListOrder($userPhone,$orderNumber);
        $activityData = $activityList->data;
        foreach ($activityData as $activityDatum) {
            $data[] = $activityDatum;
            $orderNumber[] = $activityDatum->order_number;
        }

        // get locker return
        $lockerReturnModel = new LockerReturn();
        $returnList = $lockerReturnModel->getList($userPhone,$orderNumber);
        $returnData = $returnList->data;
        foreach ($returnData as $returnDatum) {
            $data[] = $returnDatum;
            $orderNumber[] = $returnDatum->order_number;
        }

        $data = collect($data);
        $sorted = $data->sortByDesc(function ($value,$key){
            return strtotime($value->last_update);
        });
        $data = $sorted->values()->all();
        $data = collect($data);

        if ($status){
            $data = $data->filter(function ($value,$key) use ($status){
               $isTrue = $value->status == $status;
               return $isTrue;
            });
            $data = $data->values()->all();
            $data = collect($data);
        } else {
            if ($isComplete){
                $data = $data->filter(function ($value,$key) use ($status){
                    $isTrue = $value->status == 'COMPLETED';
                    return $isTrue;
                });
                $data = $data->values()->all();
                $data = collect($data);
            } else {
                $data = $data->filter(function ($value,$key) use ($status){
                    $isTrue = $value->status != 'COMPLETED';
                    return $isTrue;
                });
                $data = $data->values()->all();
                $data = collect($data);
            }
        }

        // create count all data
        $totalData = count($data);
        // create page list
        $pageList = $data->chunk(10);
        $pages = [];
        foreach ($pageList as $key => $item){
            $pages[] = $key+1;
        }

        // create count and data for current page only
        $chunk = $data->forPage($pageNumber,10);
        $data = $chunk->values()->all();
        $totalView = count($data);

        return ApiHelper::buildResponsePagination(200,$errorMessage,count($pages),$data);
    }

    /**
     * Get History Detail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderHistoryDetail(Request $request){
        $errorMessage = null;
        $requiredInput = ['session_id','order_id'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $sessionId = $request->input('session_id');
        $orderId = $request->input('order_id');

        $getUserId = UserSession::getUserBySessionId($sessionId);
        if (!$getUserId->isSuccess){
            $errorMessage = $getUserId->erroMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $userId = $getUserId->userId;
        $userDb = User::find($userId);

        $isFind = false;
        $id = $invoiceId = $type = $lockerName = $status = $lastUpdate =$address = $latitude = $longitude = $sender = $item = $history = $pin = $textShare = $overdueDate = null;
        $type = null;
        $merchantName = null;

        // get popsend v1 invoice
        $popsendRecipient = MemberPickup::join('locker_activities_pickup','locker_activities_pickup.tracking_no','=','tb_member_pickup.invoice_id')
            ->where('tb_member_pickup.invoice_id',$orderId)
            ->first();

        if ($popsendRecipient){
            $isFind = true;
            $id = (string)$popsendRecipient->id_pickup;
            $invoiceId = $popsendRecipient->invoice_id;
            $type = 'PopSend';
            $merchantName = 'PopBox';
            if (!empty($popsendRecipient->recipient_locker_name)){
                $lockerName = $popsendRecipient->recipient_locker_name;
            }
            $status = $popsendRecipient->status;
            $lastUpdate = $popsendRecipient->storetime;
            $pin = null;
            // generate status based on popsend type
            $popsendType = substr($invoiceId,0,3);

            // check on process status ON PROCESS
            if ($popsendRecipient->status == 'IN_STORE') {
                $resp=['response' => ['code' => '400','message' =>"Not Found"],'data' => []];
                return response()->json($resp);
            }
            $history = [];
            if ($popsendRecipient->status == 'COURIER_TAKEN' || $popsendRecipient->status == 'OPERATOR_TAKEN') {
                $status = 'ON PROCESS';
                $lastUpdate = $popsendRecipient->taketime;
                $tmp = new \stdClass();
                $tmp->status = $status;
                $tmp->updated = $lastUpdate;
                $history[] = $tmp;
            }

            if ($popsendType=='PLL'){
                // check on locker activities
                $lockerActivitiesDb = LockerActivity::where('barcode',$invoiceId)
                    ->first();
                if ($lockerActivitiesDb){
                    $pin = $lockerActivitiesDb->validatecode;
                    $overdueDate = $lockerActivitiesDb->overduetime;
                    if ($lockerActivitiesDb->status=='IN_STORE'){
                        $status = 'READY FOR PICKUP';
                        $lastUpdate = $lockerActivitiesDb->storetime;
                        $tmp = new \stdClass();
                        $tmp->status = $status;
                        $tmp->updated = $lastUpdate;
                        $history[] = $tmp;
                    } elseif ($lockerActivitiesDb->status == 'OPERATOR_TAKEN' || $lockerActivitiesDb->status == 'COURIER_TAKEN') {
                        $status = 'OVERDUE';
                        $lastUpdate = $lockerActivitiesDb->overduetime;
                        $tmp = new \stdClass();
                        $tmp->status = $status;
                        $tmp->updated = $lastUpdate;
                        $history[] = $tmp;
                    } elseif ($lockerActivitiesDb->status=='CUSTOMER_TAKEN'){
                        $status = 'READY FOR PICKUP';
                        $lastUpdate = $lockerActivitiesDb->storetime;
                        $tmp = new \stdClass();
                        $tmp->status = $status;
                        $tmp->updated = $lastUpdate;
                        $history[] = $tmp;
                    }
                    $nowDateTime = date('Y-m-d H:i:s');
                    if ($nowDateTime > $lockerActivitiesDb->overduetime && $lockerActivitiesDb->status=='IN_STORE'){
                        $status = 'OVERDUE';
                        $lastUpdate = $lockerActivitiesDb->overduetime;
                        $tmp = new \stdClass();
                        $tmp->status = $status;
                        $tmp->updated = $lastUpdate;
                        $history[] = $tmp;
                    }
                }
            }

            // check status on return statuses COMPLETED
            $returnStatusDb = DB::connection('popbox_db')->table('return_statuses')
                ->where('number',$invoiceId)
                ->first();
            if ($returnStatusDb){
                if (!empty($returnStatusDb->receiver_name)){
                    $status = 'COMPLETED';
                    $lastUpdate = $returnStatusDb->updated_at;
                    $tmp = new \stdClass();
                    $tmp->status = $status;
                    $tmp->updated = $lastUpdate;
                    $history[] = $tmp;
                }
            }

            // create address
            if (empty($lockerName)){
                $address = $popsendRecipient->recipient_address;
            }
            else {
                $lockerLocationDb = DB::connection('popbox_db')->table('locker_locations')
                    ->where('name',$lockerName)
                    ->first();
                if ($lockerLocationDb){
                    $address = $lockerLocationDb->address." ".$lockerLocationDb->address_2.". \nOperational Hour: ".$lockerLocationDb->operational_hours;
                    $latitude = $lockerLocationDb->latitude;
                    $longitude = $lockerLocationDb->longitude;
                }
            }
            // create sender
            $memberDb = DB::connection('popbox_db')->table('tb_member')
                ->where('phone',$popsendRecipient->phone)
                ->first();
            if ($memberDb){
                $sender = $memberDb->member_name." - ".$memberDb->phone;
            }
            // item
            $item = $popsendRecipient->item_description;
        }

        // get popsend v2 invoice
        if (!$isFind){
            $deliveryDestination = DeliveryDestination::where('invoice_sub_code',$orderId)->first();
            if ($deliveryDestination) {
                $isFind = true;
                $id = $deliveryDestination->invoice_sub_code;
                $invoiceId = $deliveryDestination->invoice_sub_code;
                $type = 'PopSend';
                $merchantName = 'PopBox';
                // check on locker location for locker name
                $lockerLocationDb = DB::connection('popbox_db')->table('locker_locations')
                    ->where('name',$deliveryDestination->destination_name)
                    ->first();
                if ($lockerLocationDb){
                    $lockerName = $lockerLocationDb->name;
                    $address = $lockerLocationDb->address." ".$lockerLocationDb->address_2.". \nOperational Hour: ".$lockerLocationDb->operational_hours;
                    $latitude = $lockerLocationDb->latitude;
                    $longitude = $lockerLocationDb->longitude;
                } else {
                    $address = $deliveryDestination->address;
                    $latitude = $deliveryDestination->destination_latitude;
                    $longitude = $deliveryDestination->destination_longitude;
                }
                $sender = $deliveryDestination->delivery->name_sender;
                $item = $deliveryDestination->category;
                $status = $deliveryDestination->status;
                $history = [];
            }
        }

        // search on popshop
        if (!$isFind){
            $order = Order::where('invoice_id',$orderId)->first();
            if ($order) {
                $isFind = true;
                $id = $order->invoice_id;
                $invoiceId = $order->invoice_id;
                $type = 'PopShop';
                $merchantName = 'PopBox';
                // check on locker location for locker name
                $lockerLocationDb = DB::connection('popbox_db')->table('locker_locations')
                    ->where('name',$order->address)
                    ->first();
                if ($lockerLocationDb){
                    $lockerName = $lockerLocationDb->name;
                    $address = $lockerLocationDb->address." ".$lockerLocationDb->address_2.". \nOperational Hour: ".$lockerLocationDb->operational_hours;
                    $latitude = $lockerLocationDb->latitude;
                    $longitude = $lockerLocationDb->longitude;
                } else {
                    $address = $order->address;
                }
                $sender = 'PopBox';
                // create item
                $dimoItems = DB::connection('popbox_db')->table('order_products')
                    ->join('dimo_products','order_products.sku','=','dimo_products.sku')
                    ->where('order_products.invoice_id',$orderId)
                    ->get();

                $item = '';
                foreach ($dimoItems as $dimoItem) {
                    $item.=$dimoItem->name;
                    $item.= " ";
                }
                $history = [];
                if ($order->deliver_status==0){
                    $status = 'ON PROCESS';
                    $lastUpdate = $order->completed_date;
                    $tmp = new \stdClass();
                    $tmp->status = $status;
                    $tmp->updated = $lastUpdate;
                    $history[] = $tmp;
                }
                elseif ($order->deliver_status ==1){
                    // query to locker activities
                    $lockerActivitiesDb = DB::connection('popbox_db')->table('locker_activities')
                        ->where('barcode',$invoiceId)
                        ->first();
                    if ($lockerActivitiesDb){
                        if ($lockerActivitiesDb->status == 'IN_STORE'){
                            $pin = $lockerActivitiesDb->validatecode;
                            $overdueDate = $lockerActivitiesDb->overduetime;
                            $status = 'READY FOR PICKUP';
                            $lastUpdate = $order->deliver_date;
                            $tmp = new \stdClass();
                            $tmp->status = $status;
                            $tmp->updated = $lastUpdate;
                            $history[] = $tmp;
                        } elseif ($lockerActivitiesDb->status == 'CUSTOMER_TAKEN') {
                            $pin = $lockerActivitiesDb->validatecode;
                            $overdueDate = $lockerActivitiesDb->overduetime;
                            $status = 'READY FOR PICKUP';
                            $lastUpdate = $order->deliver_date;
                            $tmp = new \stdClass();
                            $tmp->status = $status;
                            $tmp->updated = $lastUpdate;
                            $history[] = $tmp;

                            $status = 'COMPLETED';
                            $lastUpdate = $order->deliver_date;
                            $tmp = new \stdClass();
                            $tmp->status = $status;
                            $tmp->updated = $lastUpdate;
                            $history[] = $tmp;
                        }
                    }
                }
            }
        }

        // search on locker activities return
        if (!$isFind){
            $lockerReturn = DB::connection('popbox_db')->table('locker_activities_return')->where('id',$orderId)->first();
            if ($lockerReturn){
                $isFind = true;
                $id = $lockerReturn->id;
                $invoiceId = $lockerReturn->tracking_no;
                $type = 'return';
                $merchantName = $lockerReturn->merchant_name;
                $lockerName = $lockerReturn->locker_name;
                $status = 'ON PROCESS';
                $lastUpdate = $lockerReturn->storetime;
                $sender = $lockerReturn->phone_number;
                $lockerLocationDb = DB::connection('popbox_db')->table('locker_locations')
                    ->where('name',$lockerName)
                    ->first();
                if ($lockerLocationDb){
                    $address = $lockerLocationDb->address." ".$lockerLocationDb->address_2.". \nOperational Hour: ".$lockerLocationDb->operational_hours;
                    $latitude = $lockerLocationDb->latitude;
                    $longitude = $lockerLocationDb->longitude;
                }
                if ($lockerName=='Jakarta TestStation'){
                    $address = 'PopBox Test Station';
                    $latitude = '-6.200748';
                    $longitude = '106.798385';
                }

                $history = [];
                $tmp = new \stdClass();
                $tmp->status = 'ON PROCESS';
                $tmp->updated = $lockerReturn->storetime;
                $history[] = $tmp;
                if ($lockerReturn->status =='COMPLETED'){
                    $lastUpdate = $lockerReturn->taketime;
                    $status = 'COMPLETED';
                    $tmp = new \stdClass();
                    $tmp->status = $status;
                    $tmp->updated = $lockerReturn->taketime;
                    $history[] = $tmp;
                }
            }
        }

        // search on locker activities
        if (!$isFind){
            // find on locker activities and merchant pickup
            $lockerActivitiesDb = DB::connection('popbox_db')->table('locker_activities')
                ->leftJoin('tb_merchant_pickup','tb_merchant_pickup.order_number','=','locker_activities.barcode')
                ->where('id',$orderId)
                ->select('locker_activities.*','tb_merchant_pickup.merchant_name','tb_merchant_pickup.order_number','tb_merchant_pickup.detail_items')
                ->first();

            if ($lockerActivitiesDb){
                $isFind = true;
                $id = $lockerActivitiesDb->id;
                $invoiceId = $lockerActivitiesDb->barcode;
                $type = 'delivery';
                $merchantName = 'PopBox';
                $sender = 'PopBox';
                if (!empty($lockerActivitiesDb->merchant_name)){
                    $type = 'merchant';
                    $sender = $lockerActivitiesDb->merchant_name;
                    $merchantName = $lockerActivitiesDb->merchant_name;
                }
                $lockerName = $lockerActivitiesDb->locker_name;
                $status = $lockerActivitiesDb->status;
                $lastUpdate = null;
                if ($lockerActivitiesDb->status == 'IN_STORE'){
                    $status = 'READY FOR PICKUP';
                    $lastUpdate = $lockerActivitiesDb->storetime;
                } elseif ($lockerActivitiesDb->status == 'CUSTOMER_TAKEN'){
                    $status = 'COMPLETED';
                    $lastUpdate = $lockerActivitiesDb->taketime;
                }

                $nowDateTime = date('Y-m-d H:i:s');
                if ($nowDateTime > $lockerActivitiesDb->overduetime && $lockerActivitiesDb->status == 'IN_STORE'){
                    $status = 'OVERDUE';
                    $lastUpdate = $lockerActivitiesDb->storetime;
                }

                $pin = $lockerActivitiesDb->validatecode;
                $overdueDate = $lockerActivitiesDb->overduetime;
                $item = $lockerActivitiesDb->detail_items;
                // find locker address
                $address = null;
                $latitude = null;
                $longitude = null;
                $lockerLocationDb = DB::connection('popbox_db')->table('locker_locations')
                    ->where('name',$lockerName)
                    ->first();
                if ($lockerLocationDb){
                    $address = $lockerLocationDb->address." ".$lockerLocationDb->address_2.". \nOperational Hour: ".$lockerLocationDb->operational_hours;
                    $latitude = $lockerLocationDb->latitude;
                    $longitude = $lockerLocationDb->longitude;
                }
                if ($lockerName=='Jakarta TestStation'){
                    $address = 'PopBox Test Station';
                    $latitude = '-6.200748';
                    $longitude = '106.798385';
                }
                // create history
                $history = [];
                if ($lockerActivitiesDb->status == 'IN_STORE'){
                    $tmp = new \stdClass();
                    $tmp->status = 'READY FOR PICKUP';
                    $tmp->updated = $lockerActivitiesDb->storetime;
                    $history[]= $tmp;
                } elseif ($lockerActivitiesDb->status == 'CUSTOMER_TAKEN'){
                    $tmp = new \stdClass();
                    $tmp->status = 'READY FOR PICKUP';
                    $tmp->updated = $lockerActivitiesDb->storetime;
                    $history[]= $tmp;
                    $tmp = new \stdClass();
                    $tmp->status = 'COMPLETED';
                    $tmp->updated = $lockerActivitiesDb->taketime;
                    $history[]= $tmp;
                } elseif ($lockerActivitiesDb->status == 'OPERATOR_TAKEN' || $lockerActivitiesDb->status == 'COURIER_TAKEN') {
                    $status = 'OVERDUE';
                    $lastUpdate = $lockerActivitiesDb->overduetime;
                    $tmp = new \stdClass();
                    $tmp->status = $status;
                    $tmp->updated = $lastUpdate;
                    $history[] = $tmp;
                }
                if ($status=='OVERDUE'){
                    $tmp = new \stdClass();
                    $tmp->status = 'OVERDUE';
                    $tmp->updated = $lockerActivitiesDb->overduetime;
                    $history[]= $tmp;
                }
            }
        }

        if ($isFind){
            $history = collect($history);
            $sorted = $history->sortByDesc(function ($value,$key){
               return strtotime($value->updated);
            });
            $history = $sorted->values()->all();

            if (!empty($lockerName) && !empty($pin) && !empty($overdueDate)){
                $date = date("d/m/y H:i",strtotime($overdueDate));
                $tmpLockerName = "PopBox @ $lockerName";
                if (strpos($lockerName, '@') !== false) {
                    $tmpLockerName = $lockerName;
                }
                $textShare = "PIN Code: $pin can be used to take order $invoiceId at $tmpLockerName. Please be taken before $date";
            }

            $tmp = new \stdClass();
            $tmp->id = $id;
            $tmp->order_number = $invoiceId;
            $tmp->type = $type;
            $tmp->merchant = $merchantName;
            $tmp->locker = $lockerName;
            $tmp->status = $status;
            $tmp->last_update = $lastUpdate;
            $tmp->address = $address;
            $tmp->latitude = $latitude;
            $tmp->longitude = $longitude;
            $tmp->sender = $sender;
            $tmp->item = $item;
            $tmp->pin = $pin;
            $tmp->text_share = $textShare;
            $tmp->history = $history;

            return ApiHelper::buildResponse(200,$errorMessage,$tmp);

        }

        $errorMessage = 'Data Not Found';
        return ApiHelper::buildResponse(400,$errorMessage);

    }

    public function apiPushNotificationFCM(Request $request) {

        $fcmToken = $request->input('fcm', null);
        $title = $request->input('title', null);
        $body = $request->input('body', null);
        $id = $request->input('id', null);
        $type = $request->input('type', null);


        $to = $fcmToken;

        #prep the bundle
        $notification_body = array (
            'title'	=> $title,
            'body' 	=> $body,
            'icon'	=> 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $data_body = array (
            'id'=> $id,
            'timestamp'=> '',
            'img_url'=> '',
            'type'=> $type
        );

        $fields = array (
            'to'		=> $to,
            'notification'	=> $notification_body,
            'data' => $data_body
        );

        $response = Helper::pushNotificationToSubscriberFCM($fields);
        return $response;
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Models\ReferralCampaign;
use App\Http\Models\ReferralTransaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReferralController extends Controller
{
    /**
     * Check Referral Code before Register
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkReferralCode(Request $request){
        $requiredInput = ['phone','email','referral_code'];

        $errorMessage = null;
        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // validation
        // create validation
        $rules = [
            'phone' => 'required|numeric|digits_between:10,15',
            'email' => 'required|email',
            'referral_code' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $referralCode = $request->input('referral_code');
        $phone = $request->input('phone');
        $email = $request->input('email');

        // checking user first
        $userDb = User::where('phone',$phone)
            ->orWhere('email',$email)
            ->first();
        if ($userDb){
            $errorMessage = 'Member Already Exist';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // checking active campaign
        $referralTransactionModel = new ReferralTransaction();
        // check
        $activeCampaign = $referralTransactionModel->checkActiveCampaign($referralCode);
        if (!$activeCampaign->isSuccess){
            $errorMessage = $activeCampaign->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        $activeCampaignId = $activeCampaign->campaignId;

        // get model from referral campaign
        $referralCampaignDb = ReferralCampaign::find($activeCampaignId);
        if (!$referralCampaignDb){
            $errorMessage = 'Invalid Campaign';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        // create response
        $response = new \stdClass();
        $response->name = $referralCampaignDb->campaign_name;
        $response->expired = $referralCampaignDb->expired;
        $response->description_to_shared = $referralCampaignDb->description_to_shared;

        return ApiHelper::buildResponse(200,$errorMessage,$response);
    }
}

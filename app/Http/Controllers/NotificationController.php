<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 12/06/18
 * Time: 17.09
 */

namespace App\Http\Controllers;


use App\Http\Library\ApiHelper;
use Illuminate\Http\Request;

class NotificationController extends Controller {

    public function sendPushNotification(Request $request) {

        $fcm_token = $request->input('fcm_token',null);

        #API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAAUYtjWQ0:APA91bGwW1TobozTaxsGAKILcoWzQEJC2hnw2o43wNzdc0z4tC5DoeRmE_UxB_O-Wt8OV90ZWf9ToY9ziBF6L6LPP9FWT9T92VYcfwboi8r19lwqrf1ahu9Lx3vc-vwa3GqIeAh7bUzACfT9JIXSq_xcGbdHZAhkZA' );
        $registrationIds = $fcm_token;

        // Angga Dev
        //eJ9kETrAxZA:APA91bFqjSnp0DvnnsFlO3HzvIJtiG6AS2O0DYrlbwdoIUiHR7Yty7pXBEcCmY3OgVCQxua0aT37w11u55Q1TLBYaQsmFfYVYxb2BiV2kkwGeoV1lHTu7YTRPo1-Jv8H15wznQn-XDr8

        #prep the bundle
        $notification_body = array (
            'body' 	=> 'Body  Of Notification',
            'title'	=> 'Title Of Notification',
            'icon'	=> 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $data_body = array (
            'body' 	=> 'Body  Of Notification',
            'title'	=> 'Title Of Notification',
            'id'=> '114',
            'timestamp'=> '',
            'img_url'=> '',
            'type'=> 'promo'
        );

        $fields = array (
            'to'		=> $registrationIds,
            'notification'	=> $notification_body,
            'data' => $data_body
        );


        $headers = array (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        return ApiHelper::buildResponse(200, 'Success', json_decode($result, false));
    }

    public function sendEmailNotification() {

    }

    public function sendSMSNotification() {

    }

    public function sendChatAppNotification() {

    }
}
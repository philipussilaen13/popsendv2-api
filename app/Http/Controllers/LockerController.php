<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelper;
use App\Http\Models\LockerLocation;
use App\Http\Models\Popsafe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LockerController extends Controller
{
    public function getLockerLocation(Request $request){
        // create validation
        $rules = [
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $latitude = $request->input('latitude',null);
        $longitude = $request->input('longitude',null);
        $countryName = $request->input('country_name',null);
        $provinceName = $request->input('province_name',null);
        $cityName = $request->input('city_name',null);

        // initiate model
        $lockerLocationModel = new LockerLocation();

        // create filter
        $filter = $request->except(['latitude','longitude']);

        $getNearest = $lockerLocationModel->getNearestLocker($latitude,$longitude,null,$filter);

        if (!empty($latitude) && !empty($longitude)){
            $distanceLimit = 10;
            $startLimit = 2;
            if (empty($getNearest)){
                while (empty($getNearest)){
                    $getNearest = $lockerLocationModel->getNearestLocker($latitude,$longitude,$startLimit,$filter);
                    $startLimit += 2;
                    if ($startLimit >= $distanceLimit) break;
                }
            }
        }

        if (empty($getNearest)){
            $errorMessage = 'Locker Not Found';
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        return ApiHelper::buildResponse(200,null,$getNearest);
    }

    public function getLockerByLockerId (Request $request)
    {
        $rules = [
            'locker_id' => 'required'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $locker_id = $request->input('locker_id',null);

        $lockerLocationModel = new LockerLocation();
        $getLockerByLockerId =  $lockerLocationModel->getLockerByLockerId($locker_id);
        if (!$getLockerByLockerId) {
            return ApiHelper::buildResponse(400,'Locker Id Not Found');
        }

        unset($getLockerByLockerId->id);
        unset($getLockerByLockerId->vendor_id);
        $getLockerByLockerId->address_detail = $getLockerByLockerId->address_2;
        $getLockerByLockerId->image_url = $getLockerByLockerId->image;
        unset($getLockerByLockerId->address_2);
        unset($getLockerByLockerId->image);
        unset($getLockerByLockerId->province_id);
        unset($getLockerByLockerId->country_id);
        unset($getLockerByLockerId->district_id);
        unset($getLockerByLockerId->building_type_id);
        unset($getLockerByLockerId->zip_code);
        unset($getLockerByLockerId->created_user);
        unset($getLockerByLockerId->created_date);
        unset($getLockerByLockerId->updated_user);
        unset($getLockerByLockerId->updated_date);

        return ApiHelper::buildResponse(200,null,$getLockerByLockerId);
    }

    public function blockUnBlockParcel(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $express_number = $request->input('expressNumber');
        $code_pin = $request->input('validateCode');
        $parcel = Popsafe::where('invoice_code', $express_number)->first();
        
        try {
            $parcel->code_pin = $code_pin;
            $parcel->save();
            $response->isSuccess = true;
            $response->message = 'Data Has been blocked';
            return ApiHelper::buildResponse(200,null,$response);
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return ApiHelper::buildResponse(200,null,$response);
        }
    }

    public function getLockerDetail(Request $request) {
        
        $locker_id = $request->input('locker_id',null);
        $latitude = $request->input('latitude',null);
        $longitude = $request->input('longitude',null);
        $countryName = $request->input('country_name',null);
        $provinceName = $request->input('province_name',null);
        $cityName = $request->input('city_name',null);
        $building_type = $request->input('building_type',null);
        $online_status = $request->input('online_status',null);

        // create filter
        $filter = $request->except(['locker_id']);

        $locker = new LockerLocation();
        $lockerResult = $locker->getDetail($locker_id, $filter);

        if (empty($lockerResult)) {
            return ApiHelper::buildResponse(400,'Locker Not Found');
        }

        return ApiHelper::buildResponse(200,null, $lockerResult);
    }
}

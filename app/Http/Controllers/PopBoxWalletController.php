<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Library\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Models\Transaction;
use App\Http\Models\BalanceRecord;

class PopBoxWalletController extends Controller{
    
    public function deduct(Request $request){
        $errorMessage = null;
        $requiredInput = ['email', 'phone', 'password', 'nominal']; //[email', phone','password','device_type','gcm_token','device_id','device_name'];
        
        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }
        
        $phone = $request->input('phone');
        $email = $request->input('email');
        $password = $request->input('password');
        
        $code = 200;
                
        try{
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                
                $userDb = User::where('phone',$phone)->where('email',$email)->first();
                
                if(!empty($userDb) && Hash::check($password,$userDb->password)){
                    
                    $transaction = self::process($userDb, $request);
                    
                    if($transaction->isSuccess){
                        // get member Profile
                        $userDb = User::getUserProfileByMemberId($userDb->member_id);
                        if (!$userDb->isSuccess){
                            $errorMessage = $userDb->errorMsg;
                            $code = 400;
                        }
                        $userDb->data->transaction_reference = $transaction->transaction_reference;
                        $data = $userDb->data;
                        return ApiHelper::buildResponse($code, null, $data);
                    } else {
                        $errorMessage = $transaction->errorMsg;
                        $code = 400;
                    }
                    
                } else {
                    $errorMessage = 'Invalid Credential';
                    $code = 400;
                }
            } else {
                $errorMessage = "Invalid email address";
                $code = 400;
            }
        } catch (\Exception $e){
            return ApiHelper::buildResponse(400,$e->getMessage());
        }
        return ApiHelper::buildResponse($code, $errorMessage);
    }
    
    private function process($userDb, $request){
        
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transaction_reference = null;
        try{
            $userId = $userDb->id;
            $nominal = $request->input('nominal');
            $remarks = 'Transaction with popbox wallet by '.$userDb->phone. ' with nominal '.$nominal.'. ';
            
            DB::beginTransaction();
            
            // create transaction
            $transactionModel = new Transaction();
            $transaction = $transactionModel->createTransaction('deduct', $userDb, $remarks, $nominal);
            if (!$transaction->isSuccess){
                $response->errorMsg = 'Failed to create transaction';
                return $response;
            }
            
            $response->transaction_reference = $transaction->transactionRef;
            // debit balance
            $balanceModel =  new BalanceRecord();
            $credit = $balanceModel->debitDeposit($userId, $nominal, $transaction->transactionId);
            if (!$credit->isSuccess){
                DB::rollback();
                $response->errorMsg = $credit->errorMsg;
                return $response;
            }
            
            DB::commit();
        } catch (\Exception $e){
            DB::rollback();
            $response->errorMsg = $e->getMessage();
            return $response;
        }
        
        $response->isSuccess = true;
        return $response;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Library\Helper;
use App\Http\Models\Transaction;
use Illuminate\Support\Facades\Validator;
use App\Http\Library\ApiHelper;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Get History Transactions
     * @param Request $request
     * @return mixed
     */
    public function HistoryTransaction (Request $request)
    {
        $errorMessage = null;
        $requiredInput = ['transaction_type','page'];

        // check required
        $checkRequired = ApiHelper::checkRequiredParam($requiredInput,$request);
        if (!$checkRequired->isSuccess){
            $errorMessage = $checkRequired->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        /*Validate value from Input*/
        $rules = [
            'transaction_type' => 'required|in:delivery,popsafe',
            'page' => 'required|integer'
        ];

        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $errorMessage = implode(' ',$errors);
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        //Get UserId from session_id
        $user = Helper::getUserId($request->input('session_id'));
        $userId = $user->user_id;
        $transaction_type = $request->input('transaction_type');

        $deliveryTransaction = new Transaction();
        $popsendDb = $deliveryTransaction->getHistory($userId,$transaction_type);

        if (!$popsendDb->isSuccess){
            $errorMessage = $popsendDb->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $transactionData = $popsendDb->data;

        // create response
        foreach ($transactionData as $transactionDatum) {
            // unset variable
            unset($transactionDatum->id);
            unset($transactionDatum->user_id);
            unset($transactionDatum->transaction_id_reference);
            foreach ($transactionDatum->transactionItems as $transaction_item) {
                unset($transaction_item->id);
                unset($transaction_item->transaction_id);
            }
        }

        return ApiHelper::buildResponsePagination(200,null,$popsendDb->pagination,$popsendDb->data);
    }
}

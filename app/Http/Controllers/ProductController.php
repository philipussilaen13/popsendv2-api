<?php

namespace App\Http\Controllers;
use App\Http\Models\Product;
use App\Http\Library\ApiHelper;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    // Get product dengan status 1 yang artinya 1 = Enable dan 0 Disable
    public function index() {
        $products = Product::getEnableProduct();
        if (!$products->isSuccess){
            $errorMessage = $products->errorMsg;
            return ApiHelper::buildResponse(400,$errorMessage);
        }

        $data = $products->data;
        return ApiHelper::buildResponse(200,null,$data);
    }
}

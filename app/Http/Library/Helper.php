<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 27/12/2017
 * Time: 11.09
 */

namespace App\Http\Library;


use App\Http\Models\Users;
use App\Http\Models\UserSession;
use http\Env\Request;

class Helper
{
    /**
     * Generate Random String
     * Type : AlphaNumUpper, AlphaNumLower, AlphaNumMix, Numeric
     * @param string $type
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10,$type='AlphaNumUpper') {

        $typeList = [
            'AlphaNumUpper' => '23456789ABCDEFGHJKLMNPQRSTUVWXYZ',
            'AlphaNumLower' => '23456789abcdefghjkmnpqrstuvxyz',
            'AlphaNumMix' => '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz',
            'Numeric' => '0123456789'
        ];

        $selectedType = $typeList['AlphaNumUpper'];
        if (array_key_exists($type,$typeList)){
            $selectedType = $typeList[$type];
        }

        $characters = $selectedType;
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Generate Invoice Code Popsend
     * @param string $serviceType
     * @param string $type
     * @param string $destination_type
     * @return string
     */
    public static function generateInvoiceCode($originType = '', $type = '', $destination_type = '',$country='ID')
    {
        if ($originType != '' & $type != '') {
            $leng = strlen($originType);
            $originType = strtoupper(substr($originType,-$leng,1));
            if ($type == 'origin') {
                if ($country == 'ID') {
                    $invoiceCode = 'P'.$originType.Helper::generateRandomString(8);
                }
                $invoiceCode = 'P'.$originType.$country.Helper::generateRandomString(8);
                return $invoiceCode;
            }else if ($type == 'destinations') {
                $leng = strlen($destination_type);
                $destination_type =  strtoupper(substr($destination_type,-$leng,1));
                if ($country == 'ID') {
                    $invoiceCode = 'P'.$originType.$destination_type.Helper::generateRandomString(7);
                }
                $invoiceCode = 'P'.$originType.$destination_type.$country.Helper::generateRandomString(7);
                return $invoiceCode;
            };
        }
    }

    /**
     *
     * @param string $session_id
     * @return mixed
     */
    public static function getUserId ($session_id = '')
    {
        $UserSession = UserSession::where('session_id',$session_id)->first();
        if ($UserSession) {
            return $UserSession;
        }
    }

    public static function getUserCountry($session_id = '') {

        $r = null;

        $userId = Helper::getUserId($session_id);

        if (empty($userId)) {
            return $r;
        }

        $user = Users::where('id', $userId->user_id)->first();

        if (empty($user)) {
            return $r;
        }

        $country = null;

        if (strtolower($user->country) == 'id') {
            $country = 'Indonesia';
        } else if (strtolower($user->country) == 'my') {
            $country = 'Malaysia';
        }

        return $country;
    }

    /**s
     * @return \Facebook\Facebook
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public static function facebookSdk ()
    {
        $fb = new \Facebook\Facebook([
            'app_id' => env('FACEBOOK_CLIENT_ID'),
            'app_secret' => env('FACEBOOK_CLIENT_SECRET'),
            'default_graph_version' => 'v2.10',
        ]);

        return $fb;
    }

    /**
     * @param $message
     * @param string $folderName
     * @param string $filename
     * @param null $sessionId
     */
    public static function LogPayment($message,$folderName='',$filename='payment',$sessionId=null){
        $path = storage_path().'/logs/'.$folderName;
        if (!is_dir($path)){
            mkdir($path);
        }

        $message = "$sessionId > $message\n";
        $f = fopen($path.'/'.$filename.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }

    /**
     * get Topup Value
     * @param $amount
     * @param $disc
     * @return float|int
     */
    public static function getTopupValue($amount, $disc)
    {
        $total = $amount + ($disc * $amount);
        return $total;
    }

    /**
     * Send SMS
     * @param $phone
     * @param $message
     */
    public static function sendSms($phone,$message){
        //send SMS
        $url = env('URL_INTERNAL','https://internalapi.popbox.asia').'/sms/send';
        $dataSms = [
            'to' => $phone,
            'message' => $message,
            'token' => env('TOKEN_INTERNAL','0weWRasJL234wdf1URfwWxxXse304')
        ];
        CurlHelper::callCurl($url,$dataSms,'POST');
    }

    public static function sendPushNotificationFCM($fcmToken, $title, $body, $id, $type) {

//        $topicDev = '/topics/topicDevelopment';
        $to = $fcmToken;

        #prep the bundle
        $notification_body = array (
            'title'	=> $title,
            'body' 	=> $body,
            'icon'	=> 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $data_body = array (
            'id'=> $id,
            'timestamp'=> '',
            'img_url'=> '',
            'type'=> $type
        );

        $fields = array (
            'to'		=> $to,
            'notification'	=> $notification_body,
            'data' => $data_body
        );

        $response = Helper::pushNotificationToSubscriberFCM($fields);
        return $response;
    }

    public static function pushNotificationToSubscriberFCM($fields) {

        #API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAAUYtjWQ0:APA91bGwW1TobozTaxsGAKILcoWzQEJC2hnw2o43wNzdc0z4tC5DoeRmE_UxB_O-Wt8OV90ZWf9ToY9ziBF6L6LPP9FWT9T92VYcfwboi8r19lwqrf1ahu9Lx3vc-vwa3GqIeAh7bUzACfT9JIXSq_xcGbdHZAhkZA' );

        // Angga Dev
        $angga_dev = 'eJ9kETrAxZA:APA91bFqjSnp0DvnnsFlO3HzvIJtiG6AS2O0DYrlbwdoIUiHR7Yty7pXBEcCmY3OgVCQxua0aT37w11u55Q1TLBYaQsmFfYVYxb2BiV2kkwGeoV1lHTu7YTRPo1-Jv8H15wznQn-XDr8';

        $headers = array (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

//        return ApiHelper::buildResponse(200, 'Success', json_decode($result, false));
        return $result;
    }
}
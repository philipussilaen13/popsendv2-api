<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 23/01/2018
 * Time: 16.22
 */

namespace App\Http\Library;


use App\Http\Models\ApiResponse;

class Google
{
    private $id = null;
    private $apiKey = '';
    private $mapsApiUrl = 'https://maps.googleapis.com/maps/api/';

    public function __construct()
    {
        $this->id = uniqid();
        $this->apiKey = env('GOOGLE_API_KEY');
    }

    /**
     * File Get Content
     * @param $request
     * @param array $param
     * @return bool|string
     */
    private function fileGetContent($request,$param=[]){
        $unique = $this->id;
        $param['key'] = $this->apiKey;

        $paramQuery = http_build_query($param);
        $url = $request.$paramQuery;

        // check on DB
        $logApi = ApiResponse::where('api_url',$url)->orderBy('id','desc')->first();
        if ($logApi){
            $dataString = $logApi->response;
            $tmp = json_decode($dataString);
            if ($tmp->status == 'OK'){
                return $dataString;
            }
        }

        // log to file
        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url\n";
        $user = get_current_user();
        $f = fopen(storage_path().'/logs/api/google.'.$date.'.'.$user.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $dataString = file_get_contents($url,false,stream_context_create($arrContextOptions));

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $dataString\n";
        $user = get_current_user();
        $f = fopen(storage_path().'/logs/api/google.'.$date.'.'.$user.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        // log to DB
        $logApi = new ApiResponse();
        $logApi->api_url = $url;
        $logApi->response = $dataString;
        $logApi->save();

        return $dataString;
    }

    /**
     * Reverse Geocode
     * @param $latitude
     * @param $longitude
     * @return bool|mixed|string
     */
    public function reverseGeocode($latitude,$longitude){
        $param = [];
        $param['latlng'] = "$latitude,$longitude";
        $request = $this->mapsApiUrl.'geocode/json?';
        $response = $this->fileGetContent($request,$param);
        $response = json_decode($response);
        return $response;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 25/01/2018
 * Time: 12.38
 */

namespace App\Http\Library;


use App\Http\Models\ApiResponse;

class ApiPopExpress
{
    private $id = null;
    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;

        $host = env('POP_EXPRESS_URL');
        $token = env('POP_EXPRESS_KEY');
        $header = [];
        $header[] = 'Content-Type:application/json';
        $header[] = 'api-key:'.$token;

        $url = $host.'/'.$request;
        $param['api_key'] = $token;
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path().'/logs/api/express.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path().'/logs/api/express.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        // log to DB
        $logApi = new ApiResponse();
        $logApi->api_url = $url;
        $logApi->request = $json;
        $logApi->response = $output;
        $logApi->save();

        return $output;
    }

    /**
     * Get City Origin
     * @return mixed
     */
    public function getCityOrigin(){
        $url = 'list/origin';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Calculate Tariff by Code
     * @param $cityOrigin
     * @param $cityCode
     * @param $destinationType
     * @param $destinationCode
     * @param null $lockerName
     * @return mixed
     */
    public function calculateByCode($cityOrigin,$cityCode,$destinationType,$destinationCode,$lockerName = null){
        $url = 'tariff_by_district_code';
        $param = [];
        $param['origin'] = $cityOrigin;
        $param['origin_code'] = $cityCode;
        $param['destination_type'] =$destinationType;
        $param['destination_code'] = $destinationCode;
        $param['locker_name'] =$lockerName;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Pickup MarketPlace v1
     * @param $pickup
     * @param $destinations
     * @return mixed
     */
    public function pickupRequestMarketplace($pickup,$destinations)
    {
        $url = 'pickup_request/marketplace/create';
        $param = [];
        $param['account_name'] = 'Popsend';
        $param['pickup_date'] = $pickup['pickup_date'];
        $param['pickup_time'] = $pickup['pickup_time'];
        $param['pickup_location'] = $pickup['pickup_location'];
        $param['pickup_alternative_location'] = $pickup['pickup_alternative_location'];
        $param['pickup_type'] = $pickup['pickup_type'];
        $param['origin'] = $pickup['origin'];
        $param['origin_code'] = $pickup['origin_code'];
        $param['data'] = $destinations;
        $param['reseller'] = $pickup['sender_name'];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;

    }

    /**
     * Pickup Request v2
     * @param array $param
     * @return mixed
     */
    public function pickupRequestMarketplaceV2($param=[]){
        $url = 'pickup_request/marketplace_2/create';
        $param['account_name'] = 'Popsend';

        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

}
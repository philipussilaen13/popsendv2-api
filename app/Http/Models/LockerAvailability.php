<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 10/06/18
 * Time: 17.32
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LockerAvailability extends Model {

    protected $connection = 'popbox_db';
    protected $table = 'locker_availability';

}
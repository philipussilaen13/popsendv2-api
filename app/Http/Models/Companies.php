<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    // table 
    protected $table = 'companies';

    public function checkClientAvailable($codeClient)
    {
        $res = new \stdClass();
        $res->isSuccess = false;
        $res->data = null;
        $res->message = "Client Undefined";

        $code = substr($codeClient, 0, 3);
        $data = Companies::where('code', 'like', $code .'%' )->first();

        if (!$data) {
            return $res;
        } else if ($data->status == 'NOT_ACTIVE') {
            $res->message = "Client already NOT ACTIVE";
            return $res;
        }

        $res->isSuccess = true;
        $res->data = $data;
        $res->message = "Client ACTIVE";
        return $res;
    }

}

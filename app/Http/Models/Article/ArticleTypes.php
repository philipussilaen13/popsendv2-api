<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 12/06/18
 * Time: 12.08
 */

namespace App\Http\Models\Article;


use Illuminate\Database\Eloquent\Model;

class ArticleTypes extends Model {

    protected $connection = 'popsendv2';
    protected $table = 'article_types';
}
<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LockerLocation extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_locations';

    /**
     * Get Locker List by Nearest or All
     * @param string $latitude
     * @param string $longitude
     * @param int $distance
     * @param array $filter
     * @return array
     */
    public function getNearestLocker($latitude='',$longitude='',$distance=2,$filter=[]){
        $countryName = empty($filter['country_name']) ? null : $filter['country_name'];
        $provinceName = empty($filter['province_name']) ? null : $filter['province_name'];
        $cityName = empty($filter['city_name']) ? null : $filter['city_name'];

        // Hardcoded pencarian loker hanya jabodetabek
        if($cityName == 'jabodetabek'){
            $lockerLocationDb = self::join('districts','districts.id','=','locker_locations.district_id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->leftJoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->leftJoin('tb_locker_availability_report', 'tb_locker_availability_report.locker_id', '=', 'locker_locations.locker_id')
            ->when($countryName,function ($query) use ($countryName){
                $query->where('countries.country',$countryName);
            })->when($provinceName,function ($query) use ($provinceName){
                $query->where('provinces.province',$provinceName);
            })->when($cityName, function ($query) use ($cityName){
                $query->where('districts.district','jakarta barat')
                    ->orWhere('districts.district', 'jakarta utara')
                    ->orWhere('districts.district', 'jakarta timur')
                    ->orWhere('districts.district', 'jakarta selatan')
                    ->orWhere('districts.district', 'bogor')
                    ->orWhere('districts.district', 'depok')
                    ->orWhere('districts.district', 'tangerang')
                    ->orWhere('districts.district', 'bekasi');
            })->when($latitude,function ($query) use ($latitude,$longitude,$distance){
                $query->select(DB::raw("*, (6371 * acos(cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians(longitude) - radians($longitude) )+ sin( radians($latitude) ) * sin(radians(latitude)) ) ) AS distance "));
                if (empty($distance)) $distance = 2;
                $query->having('distance','<=',$distance);
            })
            ->addSelect('locker_locations.*', 'countries.country', 'provinces.province', 'districts.district', 'tb_locker_availability_report.status', 'buildingtypes.building_type')
            ->where('locker_locations.isPublished', '=', 1)
            ->whereRaw('locker_locations.latitude != "" ')
            ->get();

        }else{
            $lockerLocationDb = self::join('districts','districts.id','=','locker_locations.district_id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->leftJoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->leftJoin('tb_locker_availability_report', 'tb_locker_availability_report.locker_id', '=', 'locker_locations.locker_id')
            ->when($countryName,function ($query) use ($countryName){
                $query->where('countries.country',$countryName);
            })->when($provinceName,function ($query) use ($provinceName){
                $query->where('provinces.province',$provinceName);
            })->when($cityName, function ($query) use ($cityName){
                $query->where('districts.district',$cityName);
            })->when($latitude,function ($query) use ($latitude,$longitude,$distance){
                $query->select(DB::raw("*, (6371 * acos(cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians(longitude) - radians($longitude) )+ sin( radians($latitude) ) * sin(radians(latitude)) ) ) AS distance "));
                if (empty($distance)) $distance = 2;
                $query->having('distance','<=',$distance);
            })
            ->addSelect('locker_locations.*', 'countries.country', 'provinces.province', 'districts.district', 'tb_locker_availability_report.status', 'buildingtypes.building_type')
            ->where('locker_locations.isPublished', '=', 1)
            ->whereRaw('locker_locations.latitude != "" ')
            ->get();

        }

        $lockerList = [];
        $urlDashboard = env('LOCKER_IMAGE_URL','http://dashboard.popbox.asia/uploads/images/locker/');
        foreach ($lockerLocationDb as $item) {
            $tmp = new \stdClass();
            $tmp->locker_id = $item->locker_id;
            $tmp->name = $item->name;
            $tmp->address = $item->address;
            $tmp->address_detail = $item->address_2;
            $tmp->province = $item->province;
            $tmp->city = $item->district;
            $tmp->district = $item->kecamatan;
            $tmp->latitude = $item->latitude;
            $tmp->longitude = $item->longitude;
            $tmp->operational_hour = $item->operational_hours;
            $tmp->building_type = $item->building_type;
            $tmp->locker_capacity = $item->locker_capacity;
            $tmp->distance = 0;
            $tmp->country = $item->country;
            $tmp->zip_code = $item->zip_code;

            // Adding If Locker is Online
            $status = false;
            if ($item->status == 'ENABLE') {
                $status = true;
            } else if (empty($item->status) || is_null($item->status)) {
                $status = true;
            }
            $tmp->online_status = $status;

            $tmp->image_url = empty($item->image) ? "" :$urlDashboard.$item->image;
            $tmp->image_floormap =  empty($item->floor_map) ? "" :$urlDashboard.$item->floor_map;
            if (isset($item->distance)) $tmp->distance = $item->distance;
            $lockerList[] = $tmp;
        }

        return $lockerList;
    }

    /**
     * getLocker By Locker Id
     * @param null $lockerId
     * @return null
     */
    public function getLockerByLockerId($lockerId=null){
        $lockerData = null;
        if (empty($lockerId)){
            return $lockerData;
        }

        $lockerDb = self::join('districts','districts.id','=','locker_locations.district_id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->leftJoin('tb_locker_availability_report', 'tb_locker_availability_report.locker_id', '=', 'locker_locations.locker_id')
            ->where('locker_locations.locker_id',$lockerId)
            ->select('locker_locations.*', 'districts.district', 'provinces.province', 'tb_locker_availability_report.status')
            ->first();

        if (!empty($lockerDb)) {

            if ($lockerDb->status == 'ENABLE') {
                $lockerDb->online_status = true;
            } else {
                $lockerDb->online_status = false;
            }
            unset($lockerDb->status);
        }

        return $lockerDb;
    }

    public function getDetail($lockerId = null, $filter=[]) {
//        $lockerData = null;
//        if (empty($lockerId) && empty($filter['country_name']) && empty($filter['province_name']) && empty($filter['district_name']) && empty($filter['kecamatan'])){
//            return $lockerData;
//        }

        if (!empty($lockerId)) {
            $lockerData = $this->getLockerDetailByLockerId($lockerId);
        } else if (!empty($filter)) {
            $lockerData = $this->getLockerDetailNotById($filter);
        }

        return $lockerData;
    }

    public function getLockerDetailByLockerId($lockerId = null) {

        $lockerDb = self::join('districts','districts.id','=','locker_locations.district_id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->leftJoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->leftJoin('tb_locker_availability_report', 'tb_locker_availability_report.locker_id', '=', 'locker_locations.locker_id')
            ->leftJoin('locker_availability', 'locker_availability.id_locker', '=', 'locker_locations.locker_id')
            ->where('locker_locations.locker_id',$lockerId)
            ->where('locker_locations.latitude', '!=', "")
            ->select('locker_locations.locker_id', 'locker_locations.name', 'locker_locations.address', 'locker_locations.address_2', 'locker_locations.operational_hours',
                'locker_locations.latitude', 'locker_locations.longitude', 'locker_locations.zip_code', 'locker_locations.kecamatan',
                'locker_locations.image', 'locker_locations.floor_map',
                'districts.district', 'provinces.province', 'countries.country', 'buildingtypes.building_type',
                'tb_locker_availability_report.status', 'tb_locker_availability_report.availability',
                'tb_locker_availability_report.capacity', 'tb_locker_availability_report.occupied',
                'locker_availability.xs', 'locker_availability.s', 'locker_availability.m', 'locker_availability.l', 'locker_availability.xl')
            ->first();

        $result = $this->formatResult($lockerDb);

        return $result;
    }

    public function getLockerDetailNotById($filter) {
        $countryName = empty($filter['country_name']) ? null : $filter['country_name'];
        $provinceName = empty($filter['province_name']) ? null : $filter['province_name'];
        $cityName = empty($filter['city_name']) ? null : $filter['city_name'];
        $kecamatan = empty($filter['kecamatan']) ? null : $filter['kecamatan'];
        $building_type = empty($filter['building_type']) ? null : $filter['building_type'];
        $online_status = empty($filter['online_status']) ? null : $filter['online_status'];

        $lockerDb = self::join('districts','districts.id','=','locker_locations.district_id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->leftJoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->leftJoin('tb_locker_availability_report', 'tb_locker_availability_report.locker_id', '=', 'locker_locations.locker_id')
            ->leftJoin('locker_availability', 'locker_availability.id_locker', '=', 'locker_locations.locker_id')
            ->when($online_status, function ($query) use ($online_status){
                if ($online_status == 'true') {
                    $query->where('tb_locker_availability_report.status', '=', 'ENABLE');
                } else if ($online_status == 'false') {
                    $query->where('tb_locker_availability_report.status', '!=', 'ENABLE');
                }
            })
            ->when($building_type, function ($query) use ($building_type){
                $query->where('buildingtypes.building_type',$building_type);
            })
            ->when($kecamatan, function ($query) use ($kecamatan){
                $query->where('locker_locations.kecamatan',$kecamatan);
            })
            ->when($cityName, function ($query) use ($cityName){
                $query->where('districts.district',$cityName);
            })
            ->when($provinceName,function ($query) use ($provinceName){
                $query->where('provinces.province',$provinceName);
            })
            ->when($countryName,function ($query) use ($countryName){
                $query->where('countries.country',$countryName);
            })
            // ->where('locker_locations.latitude', '!=', "")
            ->orderBy('locker_locations.name', 'asc')
            ->select('locker_locations.locker_id', 'locker_locations.name', 'locker_locations.address', 'locker_locations.address_2', 'locker_locations.operational_hours',
                'locker_locations.latitude', 'locker_locations.longitude', 'locker_locations.zip_code', 'locker_locations.kecamatan',
                'locker_locations.image', 'locker_locations.floor_map',
                'districts.district', 'provinces.province', 'countries.country', 'buildingtypes.building_type',
                'tb_locker_availability_report.status', 'tb_locker_availability_report.availability',
                'tb_locker_availability_report.capacity', 'tb_locker_availability_report.occupied',
                'locker_availability.xs', 'locker_availability.s', 'locker_availability.m', 'locker_availability.l', 'locker_availability.xl')
            ->get();

        $lockerList = [];
        foreach ($lockerDb as $item) {
            $lockerList[] = $this->formatResult($item);
        }

        return $lockerList;
    }

    public function formatResult($locker) {
        $lockerData = null;
        if (empty($locker)){
            return $lockerData;
        }

        if (!empty($locker)) {

            $avail = 0;
            $capaci = 0;
            $occu = 0;
            $xs = 0;
            $s = 0;
            $m = 0;
            $l = 0;
            $xl = 0;

            $urlDashboard = env('LOCKER_IMAGE_URL','http://dashboard.popbox.asia/uploads/images/locker/');

            if ($locker->status == 'ENABLE') {
                $locker->online_status = true;
            } else if ($locker->status == '') {
                $locker->online_status = true;
            } else {
                $locker->online_status = false;
            }

            if (!empty($locker->availability)) {
                $avail = $locker->availability;
            }

            if (!empty($locker->capacity)) {
                $capaci = $locker->capacity;
            }

            if (!empty($locker->occupied)) {
                $occu = $locker->occupied;
            }

            if (!empty($locker->xs)) {
                $xs = $locker->xs;
            }

            if (!empty($locker->s)) {
                $s = $locker->s;
            }

            if (!empty($locker->m)) {
                $m = $locker->m;
            }

            if (!empty($locker->l)) {
                $l = $locker->l;
            }

            if (!empty($locker->xl)) {
                $xl = $locker->xl;
            }

            $locker->image = empty($locker->image) ? "" : $urlDashboard.$locker->image;
            $locker->floor_map = empty($locker->floor_map) ? "" : $urlDashboard.$locker->floor_map;

            unset($locker->status);
            unset($locker->xs);
            unset($locker->s);
            unset($locker->m);
            unset($locker->l);
            unset($locker->xl);

            $locker->availability = $avail;
            $locker->capacity = $capaci;
            $locker->occupied = $occu;
            $locker->size_xs = $xs;
            $locker->size_s = $s;
            $locker->size_m = $m;
            $locker->size_l = $l;
            $locker->size_xl = $xl;
        }

        $lockerDetail = new \stdClass();
        $lockerDetail->name = $locker->name;
        $lockerDetail->address = $locker->address;
        $lockerDetail->address_detail = $locker->address_2;
        $lockerDetail->operational_hours = $locker->operational_hours;
        $lockerDetail->building_type = $locker->building_type;

        $lockerLocation = new \stdClass();
        $lockerLocation->latitude = $locker->latitude;
        $lockerLocation->longitude = $locker->longitude;
        $lockerLocation->country = $locker->country;
        $lockerLocation->province = $locker->province;
        $lockerLocation->district = $locker->district;
        $lockerLocation->kecamatan = $locker->kecamatan;
        $lockerLocation->zipcode = $locker->zip_code;

        $lockerStatus = new \stdClass();
        $lockerStatus->online_status = $locker->online_status;
        $lockerStatus->capacity = $locker->capacity;
        $lockerStatus->availability = $locker->availability;
        $lockerStatus->occupied = $locker->occupied;
        $lockerStatus->XS = $locker->size_xs;
        $lockerStatus->S = $locker->size_s;
        $lockerStatus->M = $locker->size_m;
        $lockerStatus->L = $locker->size_l;
        $lockerStatus->XL = $locker->size_xl;

        $lockerImage = new \stdClass();
        $lockerImage->image = $locker->image;
        $lockerImage->floor_map= $locker->floor_map;

        $result = new \stdClass();
        $result->locker_id = $locker->locker_id;
        $result->locker_detail = $lockerDetail;
        $result->locker_location = $lockerLocation;
        $result->locker_status = $lockerStatus;
        $result->locker_image = $lockerImage;

        return $result;
    }
}


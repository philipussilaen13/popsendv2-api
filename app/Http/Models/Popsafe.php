<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use App\User;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Popsafe extends Model
{
    protected $table = 'popsafes';
    private $statusList = ['CREATED','IN STORE','COMPLETE','OVERDUE','EXPIRED'];

    /**
     * Create Order
     * @param $userId
     * @param $lockerId
     * @param $lockerSize
     * @param $price
     * @param int $autoExtend
     * @param string $notes
     * @param null $photo
     * @param null $categoryId
     * @param null $itemDescription
     * @return \stdClass
     */
    public function createOrder($userId,$lockerId,$lockerSize,$price,$autoExtend=0,$notes="",$photo=null,$categoryId= null, $itemDescription=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->popSafeId = null;
        $response->pathPhoto = null;
        $response->data = [];

        // get user ID
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }
        $country = $userDb->country;
        $userName = $userDb->name;

        $lockerModel = new LockerLocation();
        $lockerDb = $lockerModel->getLockerByLockerId($lockerId);
        if (empty($lockerDb)){
            $response->errorMsg = 'Invalid Locker';
            return $response;
        }

        if ($categoryId){
            // get category Name
            $categoryDb = CategoryList::find($categoryId);
            if (!$categoryDb){
                $response->errorMsg = 'Invalid Category';
                return $response;
            }
        } else {
            $categoryId = null;
        }

        $lockerName = $lockerDb->name;
        $lockerAddress = $lockerDb->address;
        $lockerDetailAddress = $lockerDb->address_2;
        $lockerOperational = $lockerDb->operational_hours;
        $lockerLatitude = $lockerDb->latitude;
        $lockerLongitude = $lockerDb->longitude;

        // create invoice Code
        $invoiceCode = "PDS".$userId.Helper::generateRandomString(4);
        if ($country != 'ID'){
            $invoiceCode = "PDS$country".$userId.Helper::generateRandomString(4);
        }

        // cancellation time & expired time
        $transactionDate = date('Y-m-d H:i:s');
        $cancellationTime = date('Y-m-d H:i:s',strtotime("+1 hour"));
        $expiredTime = date('Y-m-d H:i:s',strtotime("+25 hours"));

        // create file name photo
        $pathPhoto = null;
        if($photo){
            $fileName = "$invoiceCode.jpg";
            $structure = 'media_/popsafe_/'.date('Y-M');
            $pathPhoto = $structure."/".$fileName;
        }

        // insert to database
        $popSafeDb = new self();
        $popSafeDb->user_id = $userId;
        $popSafeDb->locker_name = $lockerName;
        $popSafeDb->locker_address = $lockerAddress;
        $popSafeDb->locker_address_detail = $lockerDetailAddress;
        $popSafeDb->operational_hours = $lockerOperational;
        $popSafeDb->latitude = $lockerLatitude;
        $popSafeDb->longitude = $lockerLongitude;
        $popSafeDb->locker_size = $lockerSize;
        $popSafeDb->transaction_date = $transactionDate;
        $popSafeDb->cancellation_time = $cancellationTime;
        $popSafeDb->expired_time = $expiredTime;
        $popSafeDb->invoice_code = $invoiceCode;
        $popSafeDb->status = 'CREATED';
        $popSafeDb->status_taken_by = null;
        $popSafeDb->auto_extend = $autoExtend;
        $popSafeDb->notes = $notes;
        $popSafeDb->total_price = $price;
        $popSafeDb->item_photo = $pathPhoto;
        $popSafeDb->category_id = $categoryId;
        $popSafeDb->description = $itemDescription;
        $popSafeDb->save();

        // insert history
        $insertHistory = PopsafeHistory::insertPopsafeHistory($popSafeDb->id,'CREATED',$userName);

        $data = [
            'order_id' => $popSafeDb->id,
            'invoice_id' => $popSafeDb->invoice_code,
            'total_price' => $popSafeDb->total_price,
            'order_date' => $popSafeDb->updated_at,
            'cancellation_time' => $popSafeDb->cancellation_time,
            'expired_time' => $popSafeDb->expired_time,
            'locker_size' => $popSafeDb->locker_size,
            'locker_name' => $popSafeDb->locker_name,
            'locker_address' => $popSafeDb->locker_address,
            'locker_address_detail' => $popSafeDb->locker_address_detail,
            'operational_hours' => $popSafeDb->operational_hours,
            'is_cancel' => 1,
            'is_expired' => 1,
            'is_collect' => 0,
            'locker_number' => 0,
            'code_pin' => '',
            'amount' => (float)$popSafeDb->total_price,
        ];

        $response->isSuccess = true;
        $response->popSafeId = $popSafeDb->id;
        $response->pathPhoto = $pathPhoto;
        $response->data = $data;

        return $response;
    }

    /**
     * Extend PopSafe
     * @param $invoiceCode
     * @param boolean $isAuto
     * @return \stdClass
     */
    public function extendPopSafe($invoiceCode,$isAuto = false){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->popSafeId = null;

        // get popsafe db
        $popSafeDb = self::where('invoice_code',$invoiceCode)->first();
        if (!$popSafeDb){
            $response->errorMsg = 'PopSafe Invoice Not Found';
            return $response;
        }
        $status = $popSafeDb->status;
        $expiredTime = $popSafeDb->expired_time;
        $autoExtend = $popSafeDb->auto_extend;
        $numberOfExtend = $popSafeDb->number_of_extend;

        
        // check expired / overdue time
        // -- disable for temporary : 4 okt2018
        // $nowTime = time();
        // if ($nowTime < strtotime($expiredTime)){
        //     $response->errorMsg = 'PopSafes not EXPIRED';
        //     return $response;
        // }
        
        // check if auto extend
        if (!$isAuto){
            if (!empty($autoExtend)){
                $response->errorMsg = 'Auto Extend Already Enabled';
                return $response;
            }
        }

        // check if number of extend already passed limit
//        if ($numberOfExtend) {
//            if ($numberOfExtend == 2) {
//                $response->errorMsg = 'Limit of Extend already reached. Please contact our CS for further information';
//                return $response;
//            }
//        }

        // add expired time
        $newExpired = new \DateTime($expiredTime);
        $newExpired->modify("+24 hours");
        $newExpired = $newExpired->format("Y-m-d H:i:s");

        // update expired on popsafe
        $popSafeDb = self::find($popSafeDb->id);
        $popSafeDb->status = 'IN STORE';
        $popSafeDb->expired_time = $newExpired;
        $popSafeDb->last_extended_datetime = date('Y-m-d H:i:s');
        $numberOfExtend = $popSafeDb->number_of_extend;
        $popSafeDb->number_of_extend = $numberOfExtend+1;
        $popSafeDb->save();

        $response->isSuccess = true;
        $response->popSafeId = $popSafeDb->id;

        // insert history
        $user = 'User';
        $remarks = 'Extend by User';
        if ($autoExtend){
            $user = 'System';
            $remarks = 'Auto Extend';
        }
        $insertHistory = PopsafeHistory::insertPopsafeHistory($popSafeDb->id,'IN STORE',$user,$remarks);

        return $response;
    }

    /**
     * Get List Of Order PopSafe
     * @param $userId
     * @param null $invoiceId
     * @return \stdClass
     */
    public static function getList($userId, $invoiceId=null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $historyDb = self::where('user_id','=',$userId)
            ->select('id', 'locker_name','locker_address','locker_address_detail','operational_hours','locker_number','code_pin','latitude','longitude','locker_size','transaction_date','cancellation_time',
                'expired_time','invoice_code','status','notes','total_price','item_photo')
            ->when($invoiceId,function ($query) use ($invoiceId){
                $query->where('invoice_code',$invoiceId);
            })
            ->orderby('created_at','DESC')
            ->paginate(10);

        if ($historyDb->isEmpty()){
            $response->isSuccess = true;
            $response->errorMsg = "You don't have any transactions";
            return $response;
        }
        $nowTime = time();
        foreach ($historyDb as $item) {
            $isCancel = 1;
            $isUse = 1;
            $isCollect = 0;
            $isExpired = 0;
            $status = $item->status;
            $imageName = $item->item_photo;
            $paid_price = 0;
            $promo_price = 0;

            // get promo details
            $promoUsed = DB::table('transactions')
                ->select('paid_amount', 'promo_amount', 'id')
                ->where('transaction_id_reference', $item->id)
                ->where('transaction_type', 'popsafe')
                ->first();

            if (!empty($promoUsed)) {
                $paid_price = (int)$promoUsed->paid_amount;
                $promo_price = (int)$promoUsed->promo_amount;
            }

            if ($item->status == 'CREATED'){
                // check cancellation time
                if ($nowTime > strtotime($item->cancellation_time)) $isCancel = 1;
                // check expired time
                if ($nowTime > strtotime($item->expired_time)){
                    $isUse = 0;
                    $isCancel = 0;
                    $isExpired = 0;
                }
            } elseif ($item->status == 'IN STORE'){
                $isCancel = 0;
                $isCollect = 1;
                // check expired time
                if ($nowTime > strtotime($item->expired_time)) {
                    $isUse = 0;
                    $isCollect = 0;
                    $status = 'OVERDUE';
                    $isExpired = 0;
                }
            } elseif ($item->status == 'COMPLETE'){
                $isCancel = 0;
            } elseif ($item->status == 'OVERDUE' || $item->status == 'EXPIRED' || $item->status == 'CANCEL'){
                $isUse = 0;
                $isCollect = 0;
                $isCancel = 0;
                $isExpired = 0;
            }

            if (!empty($imageName)){
                $imageName = url($imageName);
            }

            $item->is_cancel = $isCancel;
            $item->is_expired = $isUse;
            $item->is_collect = $isCollect;
            $item->is_expired = $isExpired;
            $item->status = $status;
            $item->item_photo = $imageName;
            $item->paid_price = $paid_price;
            $item->promo_price = $promo_price;
        }

        $response->isSuccess = true;
        $response->pagination = (int)ceil($historyDb->total()/$historyDb->perPage());
        $response->data = $historyDb->items();

        return $response;
    }

    /**
     * Get Detail of PopSafe Order
     * @param $invoiceId
     * @return \stdClass
     */
    public function listDetail($invoiceId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        DB::enableQueryLog();
        $history = self::where('invoice_code','=',$invoiceId)->first();
        if(!$history){
            $response->errorMsg = 'History Not Found';
            return $response;
        }

        // get history
        $popsafeHistory = PopsafeHistory::where('popsafe_id',$history->id)
            ->select('id as popsafe_id', 'user','status','remarks','created_at','updated_at')
            ->orderBy('id','desc')
            ->get();

        $nowTime = time();

        $isCancel = 1;
        $isUse = 1;
        $isCollect = 0;
        $isExpired = 1;
        $status = $history->status;
        $imageName = $history->item_photo;
        $storeTime = null;
        $takeTime = null;
        $durationInStore = 0;
        $paid_price = 0;
        $promo_price = 0;
        $promo_name = "";
        $promo_description = "";

        // get promo details
        $promoUsed = DB::table('transactions')
            ->select('paid_amount', 'promo_amount', 'id')
            ->where('transaction_id_reference', $history->id)
            ->where('transaction_type', 'popsafe')  // check if transaction_type == popsafe, because one transaction_id_reference have two transaction_type
            ->first();

        if (!empty($promoUsed)) {
            $paid_price = (int)$promoUsed->paid_amount;
            $promo_price = (int)$promoUsed->promo_amount;
            $promoDetails = DB::table('campaign_usages')
                ->join('campaigns', 'campaign_usages.campaign_id', '=', 'campaigns.id')
                ->select('campaigns.name', 'campaigns.description')
                ->where('campaign_usages.transaction_id', $promoUsed->id)
                ->first();

            if (!empty($promoDetails)){
                $promo_name = $promoDetails->name;
                $promo_description = $promoDetails->description;
            }
        }

        if ($history->status == 'CREATED'){
            // check cancellation time
            if ($nowTime > strtotime($history->cancellation_time)) $isCancel = 0;
            // check expired time
            if ($nowTime > strtotime($history->expired_time)){
                $isUse = 0;
                $isExpired = 0;
            }
        } elseif ($history->status == 'IN STORE'){
            $isCancel = 0;
            $isCollect = 1;
            // check expired time
            if ($nowTime > strtotime($history->expired_time)) {
                $isUse = 0;
                $isCollect = 0;
                $status = 'OVERDUE';
                $isExpired = 0;
            }
        } elseif ($history->status == 'COMPLETE' ){
            $isCancel = 0;
            $isCollect = 0;
            $isExpired = 0;
        } elseif ($history->status == 'OVERDUE' || $history->status == 'EXPIRED' || $history->status == 'CANCEL'){
            $isUse = 0;
            $isCollect = 0;
            $isCancel = 0;
            $isExpired = 0;
        }

        if (!empty($imageName)){
            $imageName = url($imageName);
        }

        // get popsafe history with in store status
        $storeHistory = PopsafeHistory::where('popsafe_id',$history->id)
            ->where('status','IN STORE')
            ->first();
        if ($storeHistory){
            $storeTime = date('Y-m-d H:i:s',strtotime($storeHistory->created_at));
            $durationInStore = time() - strtotime($storeTime);
        }

        $takeHistory = PopsafeHistory::where('popsafe_id',$history->id)
            ->where('status','COMPLETE')
            ->first();
        if ($takeHistory){
            $takeTime = date('Y-m-d H:i:s',strtotime($takeHistory->created_at));
            $durationInStore = strtotime($takeTime) - strtotime($storeTime);
        }

        $history->category_name = "";

        //adjust category name
        if (empty($history->category_id)){
            $history->category_id = 0;
        } else {
            $history->category_id = (int)$history->category_id;
            $history->category_name = DB::table('category_lists')
                ->select('name')
                ->where('id',$history->category_id)
                ->first()->name;
        }

        $history->is_cancel = $isCancel;
        $history->is_expired = $isUse;
        $history->is_collect = $isCollect;
        $history->is_expired = $isExpired;
        $history->status = $status;
        $history->item_photo = $imageName;
        $history->store_time = $storeTime;
        $history->take_time = $takeTime;
        $history->duration_in_store = $durationInStore;
        // $history->popsafe_history = $popsafeHistory;
        $history->popsafe_history = $popsafeHistory;
        $history->paid_price = $paid_price;
        $history->promo_price = $promo_price;
        $history->promo_name = $promo_name;
        $history->promo_description = $promo_description;

        $history->id;
        $history->user_id;
        unset($history->express_id);
        unset($history->parcel_id);

        $response->isSuccess = true;
        $response->data = $history;
        return $response;
    }

    /**
     * Update PopSafe Status
     * @param $invoiceId
     * @param $status
     * @param string $remarks
     * @param string $lockerNumber
     * @param string $codePIN
     * @return \stdClass
     */
    public function updatePopsafeStatus($invoiceId,$status,$remarks='',$lockerNumber='',$codePIN='',$dataOperator=null, $locker_time=null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $status_taken_by = null;
        if(strpos($status, '|') !== false) {
            $tmp_status = explode('|',$status);
            $status = $tmp_status[0];
            $status_taken_by = $tmp_status[1];
        }

        $popsafe = self::where('invoice_code','=',$invoiceId)->first();
        if (!$popsafe){
            $response->errorMsg = 'Invalid Invoice Code';
            return $response;
        }
        $popsafe->status = $status;
        $popsafe->status_taken_by = $status_taken_by;
        $date = date('Y-m-d H:i:s');

        if ($status == 'IN STORE'){
            if (!empty($lockerNumber)) $popsafe->locker_number = $lockerNumber;
            if (!empty($codePIN)) $popsafe->code_pin = $codePIN;
        }

        $popsafe->save();
        
        // by pass user operator
        if ($dataOperator != null) {
            $op = json_decode(json_decode($dataOperator));
            $userName = $op->op_name;
            $remarks = 'Taken by Operator '.$userName.' ( '.$op->op_username.' )';
        } else {
            $userDb = User::find($popsafe->user_id);
            if (!$userDb){
                $response->errorMsg = 'Invalid User';
                return $response;
            }
            $userName = $userDb->name;
        }

        //Update History Popsafe
        $popsafeId =  $popsafe->id;
        $PopsafeHistory = new PopsafeHistory();
        $PopsafeHistory = $PopsafeHistory->insertPopsafeHistory($popsafeId,$status,$userName,$remarks,$locker_time);

        if(!$PopsafeHistory->isSuccess){
            $response->errorMsg = $PopsafeHistory->errorMsg;
            return $response;
        }
        $response->isSuccess = true;
        $response->status = $popsafe->status;
        return $response;
    }

    /*=========================Relationship=========================*/
    public function transactions (){
        return $this->belongsTo(Transaction::class);
    }

    public function popsafeHistory(){
        return $this->hasMany(PopsafeHistory::class);
    }
}

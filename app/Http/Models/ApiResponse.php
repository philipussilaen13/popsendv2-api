<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ApiResponse extends Model
{
    protected $table = 'api_response';
}

<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 17/06/18
 * Time: 22.09
 */

namespace App\Http\Models;

use App\Http\Library\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Users extends Model {

    protected $table = 'users';

    public static function getSessionIdByPhone($phoneNo){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->userId = null;
        $response->sessionId = null;
        $response->memberId = null;

        $userDb = DB::table('users')
            ->join('user_sessions', 'users.id', '=', 'user_sessions.user_id')
            ->select('users.id', 'users.member_id', 'user_sessions.session_id' )
            ->where('users.phone',$phoneNo)
            ->orderBy('user_sessions.created_at', 'DESC')
            ->first();

        if (!$userDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }

        $response->memberId = $userDb->member_id;
        $response->sessionId = $userDb->session_id;
        $response->userId = $userDb->id;
        $response->isSuccess = true;
        return $response;
    }

}
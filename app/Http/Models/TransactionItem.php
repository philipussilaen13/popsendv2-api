<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    protected $table = 'transaction_items';

    /**
     * Insert Transaction Items
     * @param $transactionId
     * @param $items
     * @return \stdClass
     */
    public function insertTransactionItems($transactionId,$items){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        foreach ($items as $item) {
            $itemReference = !empty($item->itemReference) ? $item->itemReference : null;
            $description = !empty($item->description) ? $item->description : null;
            $price = !empty($item->price) ? $item->price : 0;
            $paidAmount = !empty($item->paidAmount) ? $item->paidAmount : 0;
            $promoAmount = !empty($item->promoAmount) ? $item->promoAmount : 0;
            $refundAmount = !empty($item->refundAmount) ? $item->refundAmount : 0;
            $status = !empty($item->status) ? $item->status : 'PAID';
            $remarks = !empty($item->remarks) ? $item->remarks : null;

            if (empty($itemReference)){
                $response->errorMsg = 'Reference Cannot be null';
                return $response;
            }

            // save to DB
            $transactionItemDb = new self();
            $transactionItemDb->transaction_id = $transactionId;
            $transactionItemDb->item_reference = $itemReference;
            $transactionItemDb->description = $description;
            $transactionItemDb->price = $price;
            $transactionItemDb->paid_amount = $paidAmount;
            $transactionItemDb->promo_amount = $promoAmount;
            $transactionItemDb->refund_amount = $refundAmount;
            $transactionItemDb->status = $status;
            $transactionItemDb->remarks = $remarks;
            $transactionItemDb->save();

        }

        $response->isSuccess = true;
        return $response;
    }
}

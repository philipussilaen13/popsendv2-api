<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BalanceRecord extends Model
{
    protected $table = 'balance_records';

    /**
     * Credit Deposit
     * @param $userId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public function creditDeposit($userId,$amount,$transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $currentBalance = 0;
        // get userDB
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }
        if (!empty($userDb->balance)){
            $currentBalance = $userDb->balance;
        }
        // credit balance
        $newBalance = $currentBalance + $amount;

        // update to user
        $userDb->balance = $newBalance;
        $userDb->save();

        $credit = $amount;
        $debit = 0;
        $previousBalance = $currentBalance;

        // insert into balance record
        $balanceRecord = $this->insertRecord($userId,$transactionId,$credit,$debit,$previousBalance,$newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Deduct / Debit Deposit
     * @param $userId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public function debitDeposit($userId,$amount,$transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $currentBalance = 0;
        // get userDB
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }
        if (!empty($userDb->balance)){
            $currentBalance = $userDb->balance;
        }

        if ($currentBalance < $amount){
            $response->errorMsg = 'Insufficient Deposit';
            return $response;
        }
        // debit balance
        $newBalance = $currentBalance - $amount;

        // update to user
        $userDb->balance = $newBalance;
        $userDb->save();

        $credit = 0;
        $debit = $amount;
        $previousBalance = $currentBalance;

        // insert into balance record
        $balanceRecord = $this->insertRecord($userId,$transactionId,$credit,$debit,$previousBalance,$newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /*===============Private Function===============*/

    /**
     * Insert Balance Record
     * @param $userId
     * @param $transactionId
     * @param int $creditAmount
     * @param int $debitAmount
     * @param $previousBalance
     * @param $newBalance
     */
    private function insertRecord($userId,$transactionId,$creditAmount=0,$debitAmount=0,$previousBalance,$newBalance){
        // insert to database
        $balanceRecordDb = new self();
        $balanceRecordDb->user_id = $userId;
        $balanceRecordDb->transaction_id = $transactionId;
        $balanceRecordDb->credit = $creditAmount;
        $balanceRecordDb->debit = $debitAmount;
        $balanceRecordDb->previous_balance = $previousBalance;
        $balanceRecordDb->current_balance = $newBalance;
        $balanceRecordDb->date = date('Y-m-d');
        $balanceRecordDb->time = date('H:i:s');
        $balanceRecordDb->save();
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralCampaign extends Model
{
    protected $table = 'referral_campaigns';

    /**
     * Get Regular / No Special Active Campaign
     * @return \stdClass
     */
    public function getRegularActiveReferral(){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $nowDate = date('Y-m-d H:i:s');

        $referralCampaignDb = ReferralCampaign::where('status',1)
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->whereNull('code')
            ->orderBy('created_at','desc')
            ->first();

        if (!$referralCampaignDb){
            $response->errorMsg = 'No Active Referral Campaign';
            return $response;
        }

        $response->isSuccess = true;
        $response->data = $referralCampaignDb;
        return $response;
    }
}

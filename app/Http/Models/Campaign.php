<?php

namespace App\Http\Models;

use App\Http\Models\PopExpress\Locker;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Campaign extends Model
{
    protected $table = 'campaigns';

    public static function getActiveCampaign($isVoucherRequired = 0, $promoType = 'potongan')
    {
        $nowDate = date('Y-m-d H:i:s');
        DB::enableQueryLog();
        $campaignDb = self::where('status', '1')
            ->where('start_time', '<', $nowDate)
            ->where('end_time', '>', $nowDate)
            ->where('promo_type', $promoType)
            ->where('voucher_required', "$isVoucherRequired")
            ->orderBy('priority', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
        return $campaignDb;
    }

    /**
     * get active voucher
     * @param null $promoCode
     * @return mixed
     */
    public static function getVoucherActive($promoCode = null, $promoType = 'potongan')
    {
        $nowDate = date('Y-m-d H:i:s');
        $voucherDb = DB::table('vouchers')
            ->join('campaigns', 'campaigns.id', '=', 'vouchers.campaign_id')
            ->where('campaigns.start_time', '<', $nowDate)
            ->where('campaigns.end_time', '>', $nowDate)
            ->where('campaigns.status', 1)
            ->where('campaigns.promo_type', $promoType)
            ->orderBy('campaigns.priority', 'DESC')
            ->orderBy('campaigns.created_at', 'DESC')
            ->where('vouchers.code', $promoCode)
            ->get();
        return $voucherDb;
    }

    /**
     * check rule campaign
     * @param null $campaignId
     * @param array $param
     * @return \stdClass
     */
    public static function checkRuleCampaign($campaignId = null, $param = [])
    {
        // build response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->totalPrice = 0;
        $response->campaignName = null;
        $response->campaignDescription = null;
        $response->campaignId = null;
        $response->voucherId = null;
        $response->transAmount = 0;
        $response->totalDisc = 0;

        self::LogData("checkRuleCampaign campaignId $campaignId " . json_encode($param)); #log

        // check if voucher required first
        $campaignDb = self::find($campaignId);
        if (!$campaignDb) return $response;
        $response->campaignName = $campaignDb->name;
        $response->campaignDescription = $campaignDb->description;
        $response->campaignId = $campaignDb->id;
        //get member data
        $memberDb = User::where('phone', $param['cust_phone'])->first();

        if (!$memberDb) {
            $response->errorMsg = 'Member Not Found';
            return $response;
        }

        // check available
        // if voucher required
        if ($campaignDb->voucher_required == 1) {
            self::LogData("checkRuleCampaign promoCode"); #log
            // check if exist param promoCode
            if (empty($param['promo_code'])) {
                $response->errorMsg = 'Promo Code Required';
                return $response;
            }
            // check if promoCode exist in Db
            $promoCode = $param['promo_code'];
            self::LogData("checkRuleCampaign promoCode $promoCode"); #log

            $voucherDb = Voucher::where('code', $promoCode)->where('campaign_id', $campaignId)->first();
            if (!$voucherDb) {
                $response->errorMsg = 'Promo Code Not Found';
                return $response;
            }
            // check available
            self::LogData("checkRuleCampaign promoCode available"); #log
            $isAvailable = self::checkAvailable($campaignDb, $voucherDb, $memberDb, $param);

            if (!$isAvailable) {
                $response->errorMsg = 'Promo Not Available';
                return $response;
            }
            // set if voucher required
            $response->voucherId = $voucherDb->id;
        } else {
            // if voucher not required
            // check available
            self::LogData("checkRuleCampaign available"); #log
            $isAvailable = self::checkAvailable($campaignDb, null, $memberDb, $param);
            if (!$isAvailable) {
                $response->errorMsg = 'Promo Not Available';
                return $response;
            }
        }

        // check rule
        self::LogData("checkRuleCampaign rules"); #log
        $checkRules = self::checkRules($campaignId, $param);

        if (!$checkRules->isPass) {
            $response->errorMsg = $checkRules->errorMsg;
            return $response;
        }
        // calculate amount discount
        if (empty($param['transaction_amount'])) {
            $response->errorMsg = 'Transaction Amount or Price is Required';
            return $response;
        }

        $totalAmount = self::calculateAmount($campaignDb, $param['transaction_amount']);
        /*if ($totalAmount->finalAmount == 0){
            $response->errorMsg = 'Calculation is 0';
            return $response;
        }*/

        self::LogData("checkRuleCampaign total amount $totalAmount->finalAmount disc Amount $totalAmount->discAmount"); #log
        $response->isSuccess = true;
        $response->totalPrice = $totalAmount->finalAmount;
        $response->totalDisc = $totalAmount->discAmount;
        $response->transAmount = $totalAmount->transAmount;
        $response->campaignName = $campaignDb->name;
        $response->campaignId = $campaignDb->id;
        return $response;
    }


    public static function bookCampaignUsage($campaignData, $invoiceId, $memberPhone, $timeLimit = '')
    {
        // default response
        $result = new \stdClass();
        $result->isSuccess = false;
        $result->timeLimit = null;

        if (empty($campaignData->campaignId) && empty($invoiceId) && empty($memberPhone)) {
            return $result;
        }
        if (empty($timeLimit)) {
            $timeLimit = date('Y-m-d H:i:s', strtotime('+1 hour'));
        }
        // get member detail
        $memberDb = User::where('phone', $memberPhone)->first();
        if (!$memberDb) return $result;

        // insert into DB campaign usage
        $insert = DB::table('campaign_usages')->insertGetId([
            'campaign_id' => $campaignData->campaignId,
            'voucher_id' => $campaignData->voucherId,
            'user_id' => $memberDb->id,
            'transaction_id' => $invoiceId,
            'disc_amount' => $campaignData->totalDisc,
            'final_amount' => $campaignData->totalPrice,
            'transaction_amount' => $campaignData->transAmount,
            'status' => '1',
            'time_limit' => $timeLimit,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $data = DB::table('campaign_usages')->find($insert);
        // dd($data);

        $result->isSuccess = true;
        $result->timeLimit = $timeLimit;
        return $result;
    }

    /**
     * @param null $transactionId
     * @param null $memberPhone
     * @return \stdClass
     */
    public static function claimCampaignUsage($transactionId = null, $memberPhone = null)
    {
        // default result
        $result = new \stdClass();
        $result->isSuccess = false;
        $result->errorMsg = null;
        // get member DB
        $memberDb = User::where('phone', $memberPhone)->first();
        if (!$memberDb) {
            $result->errorMsg = 'Member not Found';
            return $result;
        }

        // get campaign usage detail
        $campaignUsageDb = CampaignUsage:: where('transaction_id', $transactionId)
            ->first();
        if (!$campaignUsageDb) {
            $result->errorMsg = 'Campaign Usage not Found';
            return $result;
        }
        $nowDateTime = time();
        if ($nowDateTime > strtotime($campaignUsageDb->time_limit)) {
            $result->errorMsg = 'Campaign Book is Expired';
            return $result;
        }

        // change status to 1
        DB::table('campaign_usages')
            ->where('id', $campaignUsageDb->id)
            ->update(['status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);

        $result->isSuccess = true;
        return $result;
    }

    /*=================================Private Function=================================*/

    /**
     * calculate disc amount and total amount
     * @param $campaignDb
     * @param $amountPrice
     * @return \stdClass
     */
    private static function calculateAmount($campaignDb, $amountPrice)
    {
        $finalAmount = 0;
        $discAmount = 0;
        $resultAmount = new \stdClass();
        // get type
        $type = $campaignDb->type;
        $amount = $campaignDb->amount;
        // calculate discAmount
        if ($type == 'fixed') {
            $discAmount = $amount;
        } elseif ($type == 'percent') {
            $discAmount = $amount / 100 * $amountPrice;
            if (!empty($campaignDb->max_amount) && $discAmount > $campaignDb->max_amount) {
                $discAmount = $campaignDb->max_amount;
            } elseif (!empty($campaignDb->min_amount) && $discAmount < $campaignDb->min_amount) {
                $discAmount = $campaignDb->min_amount;
            }
        }
        $promoType = $campaignDb->promo_type;
        if ($promoType == 'potongan') {
            // get category discount
            $category = $campaignDb->category;
            if ($category == 'fixed') {
                $finalAmount = $discAmount;
            } elseif ($category == 'cutback') {
                $finalAmount = $amountPrice - $discAmount;
            }
        } elseif ($promoType == 'deposit') {
            // get category discount
            $category = $campaignDb->category;
            if ($category == 'fixed') {
                $finalAmount = $discAmount;
            } elseif ($category == 'cutback') {
                $finalAmount = $amountPrice + $discAmount;
            }
        }

        if ($finalAmount < 0) {
            $finalAmount = 0;
        }

        self::LogData("calculateAmount type:$type disc:$discAmount amount:$amountPrice final:$finalAmount"); #log
        $resultAmount->finalAmount = $finalAmount;
        $resultAmount->discAmount = abs($amountPrice - $finalAmount);
        $resultAmount->transAmount = $amountPrice;
        return $resultAmount;
    }

    /**
     * check rules on campaign
     * @param $campaignId
     * @param array $param
     * @return \stdClass
     */
    private static function checkRules($campaignId, $param = [])
    {
        // build response
        $response = new \stdClass();
        $response->isPass = false;
        $response->errorMsg = null;

        // get rules list
        $rulesDb = DB::table('campaign_parameters')
            ->join('available_parameters', 'available_parameters.id', '=', 'campaign_parameters.available_parameter_id')
            ->where('campaign_id', $campaignId)
            ->get();
        $tmp = count($rulesDb);
        self::LogData("checkRules getRules $campaignId rulesCount : $tmp"); #log
        if (count($rulesDb) == 0) {
            $response->isPass = true;
            return $response;
        }
        $missingParam = [];
        $failedParam = [];
        foreach ($rulesDb as $rule) {
            // check if param and rule exist
            if (empty($param['destination_kec'])) $param['destination_kec'] = 'Palmerah';
            if (!isset($param[$rule->param_name])) {
                $missingParam[] = $rule->param_name;
            } else { // check rule pass
                $value = $param[$rule->param_name];
                $checkRulePass = self::checkRulePass($rule->name, $rule->operator, $rule->value, $param[$rule->param_name]);
                if (!$checkRulePass) $failedParam[] = $rule->name;
                self::LogData("checkRules $rule->name $rule->operator $rule->value == $value result : $checkRulePass"); #log
            }
        }
        // check if missingParam and failed Param not empty then failed
        if (!empty($missingParam) || !empty($failedParam)) {
            $errorMsg = '';
            if (!empty($missingParam)) {
                $errorMsg .= "Missing Parameter : " . implode(', ', $missingParam);
                self::LogData("checkRules Missing Param $errorMsg"); #log
                $errorMsg .= "You are not Eligible for this promo. Please Update Your Apps.";
            }
            if (!empty($failedParam)) {
                //$errorMsg .= "Failed Rule : ".implode(', ',$failedParam);
                $errorMsg .= "No Active Campaign";
            }
            self::LogData("checkRules FAIL $errorMsg"); #log

            $response->errorMsg = $errorMsg;
            return $response;
        }
        self::LogData("checkRules SUCCESS"); #log
        $response->isPass = true;
        return $response;
    }

    /**
     * checking passing rule parameter
     * @param $ruleName
     * @param $ruleOperator
     * @param $ruleValue - yang di set di DB
     * @param $paramValue - yang dikirim oleh client
     * @return bool
     */
    private static function checkRulePass($ruleName, $ruleOperator, $ruleValue, $paramValue)
    {
        $isPass = false;
        $valueArr = [];
        $multiple = ['between', 'exist', 'except'];
        if (in_array($ruleOperator, $multiple)) {
            // split rule value
            $valueArr = json_decode($ruleValue);
        }
        switch ($ruleName) {
            /*============ transaction_datetime ============*/
            case 'transaction_datetime' :
                $paramValueTime = strtotime($paramValue);
                if ($ruleOperator == '<') {
                    if ($paramValueTime < strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueTime <= strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueTime == strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueTime >= strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueTime > strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = strtotime($valueArr[0]);
                        $endTime = strtotime($valueArr[1]);
                        if ($paramValueTime > $beginTime && $paramValueTime < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            case 'transaction_date' :
                $paramValueDate = date('Y-m-d', strtotime($paramValue));
                $paramValueDate = strtotime($paramValueDate);
                if ($ruleOperator == '<') {
                    if ($paramValueDate < strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueDate <= strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueDate == strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueDate >= strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueDate > strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = strtotime($valueArr[0]);
                        $endTime = strtotime($valueArr[1]);
                        if ($paramValueDate > $beginTime && $paramValueDate < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    $paramValueDate = date('Y-m-d', strtotime($paramValue));
                    if (in_array($paramValueDate, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    $paramValueDate = date('Y-m-d', strtotime($paramValue));
                    if (!in_array($paramValueDate, $valueArr)) $isPass = true;
                }
                break;
            case 'transaction_time' :
                $paramValueTime = date('H:i', strtotime($paramValue));
                $paramValueTime = strtotime($paramValueTime);
                if ($ruleOperator == '<') {
                    if ($paramValueTime < strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueTime <= strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueTime == strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueTime >= strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueTime > strtotime($ruleValue)) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = strtotime($valueArr[0]);
                        $endTime = strtotime($valueArr[1]);
                        if ($paramValueTime > $beginTime && $paramValueTime < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    $paramValueTime = date('H:i', strtotime($paramValue));
                    if (in_array($paramValueTime, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    $paramValueTime = date('H:i', strtotime($paramValue));
                    if (!in_array($paramValueTime, $valueArr)) $isPass = true;
                }
                break;
            case 'transaction_day_date' :
                $paramValueDayDate = date('d', strtotime($paramValue));
                if ($ruleOperator == '<') {
                    if ($paramValueDayDate < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueDayDate <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueDayDate == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueDayDate >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueDayDate > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = $valueArr[0];
                        $endTime = $valueArr[1];
                        if ($paramValueDayDate > $beginTime && $paramValueDayDate < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValueDayDate, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValueDayDate, $valueArr)) $isPass = true;
                }
                break;
            case 'transaction_month' :
                $paramValueMonthDate = date('m', strtotime($paramValue));
                if ($ruleOperator == '<') {
                    if ($paramValueMonthDate < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueMonthDate <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueMonthDate == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueMonthDate >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueMonthDate > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = $valueArr[0];
                        $endTime = $valueArr[1];
                        if ($paramValueMonthDate > $beginTime && $paramValueMonthDate < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValueMonthDate, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValueMonthDate, $valueArr)) $isPass = true;
                }
                break;
            case 'transaction_year' :
                $paramValueYearDate = date('Y', strtotime($paramValue));
                if ($ruleOperator == '<') {
                    if ($paramValueYearDate < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueYearDate <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueYearDate == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueYearDate >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueYearDate > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = $valueArr[0];
                        $endTime = $valueArr[1];
                        if ($paramValueYearDate > $beginTime && $paramValueYearDate < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValueYearDate, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValueYearDate, $valueArr)) $isPass = true;
                }
                break;
            case 'transaction_day' :
                $paramValueDayDate = date('N', strtotime($paramValue));
                if ($ruleOperator == '<') {
                    if ($paramValueDayDate < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValueDayDate <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValueDayDate == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValueDayDate >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValueDayDate > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        $beginTime = $valueArr[0];
                        $endTime = $valueArr[1];
                        if ($paramValueDayDate > $beginTime && $paramValueDayDate < $endTime) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValueDayDate, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValueDayDate, $valueArr)) $isPass = true;
                }
                break;
            /*============= item_amount =============*/
            case 'item_amount' :
                if ($ruleOperator == '<') {
                    if ($paramValue < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValue <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValue >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValue > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        if ($paramValue > $valueArr[0] && $paramValue < $valueArr[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*================= transaction_amount =================*/
            case 'transaction_amount' :
                if ($ruleOperator == '<') {
                    if ($paramValue < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValue <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValue >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValue > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        if ($paramValue > $valueArr[0] && $paramValue < $valueArr[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*================== origin_locker_name ==================*/
            case 'origin_locker_name' :
                if ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            case 'origin_locker_city' :
                // get locker data
                $lockerDb = Locker::join('international_codes', 'international_codes.id', '=', 'lockers.international_id')
                    ->where('name', $paramValue)
                    ->select('lockers.id', 'lockers.name', 'lockers.address', 'lockers.address2', 'lockers.latitude', 'lockers.longitude', 'international_codes.province', 'international_codes.county', 'international_codes.district')
                    ->first();
                if ($lockerDb) {
                    if ($ruleOperator == '=') {
                        if ($lockerDb->county == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        if (in_array($lockerDb->county, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($lockerDb->county, $valueArr)) $isPass = true;
                    }
                }
                break;
            case 'origin_locker_province' :
                // get locker data
                $lockerDb = Locker::join('international_codes', 'international_codes.id', '=', 'lockers.international_id')
                    ->where('name', $paramValue)
                    ->select('lockers.id', 'lockers.name', 'lockers.address', 'lockers.address2', 'lockers.latitude', 'lockers.longitude', 'international_codes.province', 'international_codes.county', 'international_codes.district')
                    ->first();
                if ($lockerDb) {
                    if ($ruleOperator == '=') {
                        if ($lockerDb->province == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        if (in_array($lockerDb->province, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($lockerDb->province, $valueArr)) $isPass = true;
                    }
                }
                break;
            case 'origin_locker_country' :
                // get locker data
                $lockerDb = Locker::join('international_codes', 'international_codes.id', '=', 'lockers.international_id')
                    ->where('name', $paramValue)
                    ->select('lockers.id', 'lockers.name', 'lockers.address', 'lockers.address2', 'lockers.latitude', 'lockers.longitude', 'international_codes.province', 'international_codes.county', 'international_codes.district')
                    ->first();
                if ($lockerDb) {
                    if ($ruleOperator == '=') {
                        if ($lockerDb->country == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        if (in_array($lockerDb->country, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($lockerDb->country, $valueArr)) $isPass = true;
                    }
                }
                break;
            case 'origin_locker_category' :
                // get locker data
                $lockerDb = DB::table('locker_locations')
                    ->join('buildingtypes', 'buildingtypes.id_building', '=', 'locker_locations.building_type_id')
                    ->where('name', $paramValue)
                    ->select('buildingtypes.building_type')
                    ->first();
                if ($lockerDb) {
                    if ($ruleOperator == '=') {
                        if ($lockerDb->building_type == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        if (in_array($lockerDb->building_type, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($lockerDb->building_type, $valueArr)) $isPass = true;
                    }
                }
                break;
            /*================= user_phone =================*/
            case 'user_phone' :
                if ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*============ destination_kec ============*/
            /*case 'destination_province' :
                $popsendTarifDb = DB::table('tb_popsend_tarif_kec')->where('destination_kec',$paramValue)->first();
                if ($popsendTarifDb){
                    if ($ruleOperator=='='){
                        if($popsendTarifDb->destination_prov == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator=='exist'){
                        if (in_array($popsendTarifDb->destination_prov,$valueArr)) $isPass = true;
                    } elseif ($ruleOperator=='except'){
                        if (!in_array($popsendTarifDb->destination_prov,$valueArr)) $isPass = true;
                    }
                }
                break;
            case 'destination_city' :
                $popsendTarifDb = DB::table('tb_popsend_tarif_kec')->where('destination_kec',$paramValue)->first();
                if ($popsendTarifDb){
                    if ($ruleOperator=='='){
                        if($popsendTarifDb->destination_city == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator=='exist'){
                        if (in_array($popsendTarifDb->destination_city,$valueArr)) $isPass = true;
                    } elseif ($ruleOperator=='except'){
                        if (!in_array($popsendTarifDb->destination_city,$valueArr)) $isPass = true;
                    }
                }
                break;
            case 'destination_district' :
                $popsendTarifDb = DB::table('tb_popsend_tarif_kec')->where('destination_kec',$paramValue)->first();
                if ($popsendTarifDb){
                    if ($ruleOperator=='='){
                        if($popsendTarifDb->destination_kec == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator=='exist'){
                        if (in_array($popsendTarifDb->destination_kec,$valueArr)) $isPass = true;
                    } elseif ($ruleOperator=='except'){
                        if (!in_array($popsendTarifDb->destination_kec,$valueArr)) $isPass = true;
                    }
                }
                break;*/
            /*============== popsend_type ==============*/
            case 'popsend_type' :
                if ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*============== topup_amount ==============*/
            case 'topup_amount':
                if ($ruleOperator == '<') {
                    if ($paramValue < $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '<=') {
                    if ($paramValue <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>=') {
                    if ($paramValue >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>') {
                    if ($paramValue > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between') {
                    if (!empty($valueArr[0]) && !empty($valueArr[1])) {
                        if ($paramValue > $valueArr[0] && $paramValue < $valueArr[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist') {
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*============== topup_method ==============*/
            case 'topup_method':
                if ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'exist') {
                    $valueArr = (array)$valueArr;
                    $valueArr = array_values($valueArr);
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*=============transaction_type=============*/
            case 'transaction_type':
                if ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'exist') {
                    $valueArr = (array)$valueArr;
                    $valueArr = array_values($valueArr);
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*================pickup_type================*/
            case 'pickup_type':
                if ($ruleOperator == '=') {
                    if ($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'exist') {
                    $valueArr = (array)$valueArr;
                    $valueArr = array_values($valueArr);
                    if (in_array($paramValue, $valueArr)) $isPass = true;
                } elseif ($ruleOperator == 'except') {
                    if (!in_array($paramValue, $valueArr)) $isPass = true;
                }
                break;
            /*=============origin_district_id=============*/
            case 'origin_country':
                // get district
                $districtDb = District::with(['city','city.province','city.province'])
                    ->where('id',$paramValue)
                    ->first();
                $countryName = Country::where('id', $districtDb->city->province->country_id)
                    ->select('country_name')
                    ->first();
                $districtDb->city->province->country_name = $countryName->country_name;
                if ($districtDb){
                    if ($ruleOperator == '=') {
                        if ($districtDb->city->province->country_name == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        $valueArr = (array)$valueArr;
                        $valueArr = array_values($valueArr);
                        if (in_array($districtDb->city->province->country_name, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($districtDb->city->province->country_name, $valueArr)) $isPass = true;
                    }
                }
                break;
            case 'origin_province':
                // get district
                $districtDb = District::with(['city','city.province'])
                    ->where('id',$paramValue)
                    ->first();
                if ($districtDb){
                    if ($ruleOperator == '=') {
                        if ($districtDb->city->province->province_name == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        $valueArr = (array)$valueArr;
                        $valueArr = array_values($valueArr);
                        if (in_array($districtDb->city->province->province_name, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($districtDb->city->province->province_name, $valueArr)) $isPass = true;
                    }
                }
                break;
            case 'origin_city':
                // get district
                $districtDb = District::with(['city'])
                    ->where('id',$paramValue)
                    ->first();
                if ($districtDb){
                    if ($ruleOperator == '=') {
                        if ($districtDb->city->city_name == $ruleValue) $isPass = true;
                    } elseif ($ruleOperator == 'exist') {
                        $valueArr = (array)$valueArr;
                        $valueArr = array_values($valueArr);
                        if (in_array($districtDb->city->city_name, $valueArr)) $isPass = true;
                    } elseif ($ruleOperator == 'except') {
                        if (!in_array($districtDb->city->city_name, $valueArr)) $isPass = true;
                    }
                }
                break;
        }
        return $isPass;
    }

    /**
     * check remaining available campaign/voucher usage
     * @param $campaignDb
     * @param null $voucherDb
     * @param $memberDb
     * @param array $param
     * @return bool
     */
    private static function checkAvailable($campaignDb, $voucherDb = null, $memberDb, $param = [])
    {
        // get limit type
        $limitType = $campaignDb->voucher_type;
        // campaign limit
        $campaignLimit = $campaignDb->limit_usage;
        $limit = $campaignLimit;
        if (empty($limit)) return true;
        // if voucher required
        if ($campaignDb->voucher_required == 1) {
            if ($limitType == 'all') {
                // count usage all
                $count = self::countUsage($campaignDb->id, null);
            } elseif ($limitType == 'member') {
                $count = self::countUsage($campaignDb->id, null, $memberDb->id);
            } elseif ($limitType == 'voucher') {
                $count = self::countUsage($campaignDb->id, $voucherDb->id);
            }

            self::LogData("checkAvailable $limitType $limit"); #log
            // if there is no limit, return true

            self::LogData("checkAvailable $limitType $limit $count"); #log
            if ($count >= $limit) {
                return false;
            }
        } // if voucher not required
        else {
            // check limit type
            if ($limitType == 'all') {
                // count usage all
                $count = self::countUsage($campaignDb->id, null);
            } elseif ($limitType == 'member') {
                $count = self::countUsage($campaignDb->id, null, $memberDb->id);
            }

            self::LogData("checkAvailable $limitType $limit $count"); #log
            if ($count >= $limit) {
                return false;
            }
        }
        return true;
    }

    /**
     * count current usage on campaign / voucher based on member or voucher
     * @param $campaignId
     * @param null $voucherId
     * @param null $memberId
     * @return mixed
     */
    private static function countUsage($campaignId, $voucherId = null, $memberId = null)
    {
        $nowDateTime = date('Y-m-d H:i:s');
        // count used
        $usedDb = CampaignUsage::where('campaign_id', $campaignId);
        if (!empty($voucherId)) $usedDb->where('voucher_id', $voucherId);
        if (!empty($memberId)) $usedDb->where('user_id', $memberId);
        $usedDb->where('status', 1);
        $countUsed = $usedDb->count();
        // count booked
        $bookedDb = CampaignUsage::where('campaign_id', $campaignId);
        if (!empty($voucherId)) $bookedDb->where('voucher_id', $voucherId);
        if (!empty($memberId)) $bookedDb->where('user_id', $memberId);
        $bookedDb->where('status', 0)->where('time_limit', '>', $nowDateTime);
        $countBooked = $bookedDb->count();

        $result = $countBooked + $countUsed;
        return $result;
    }

    /**
     * log data to file
     * @param $msg
     */
    private static function LogData($msg)
    {
        $unique = uniqid();
        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $msg \n";
        $f = fopen(storage_path() . '/logs/campaign/campaign.' . $date . '.log', 'a');
        fwrite($f, $msg);
        fclose($f);
    }
}

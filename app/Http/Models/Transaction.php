<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    protected $table = 'transactions';

    /**
     * Create Referral Transactions
     * @param $userId
     * @param $amount
     * @param $referralTransactionId
     * @param null $remarks
     * @return \stdClass
     */
    public function createReferralTransaction($userId,$amount,$referralTransactionId,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionInvoice = null;
        $response->transactionAmount = null;

        // get user DB
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = "User Not Valid";
            return $response;
        }
        // generate invoice id
        $invoiceId = "REF-".$userId.date('ymdh').Helper::generateRandomString(4);

        // save to table Transaction
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_id_reference = $referralTransactionId;
        $transactionDb->transaction_type = 'referral';
        $transactionDb->description = $remarks;
        $transactionDb->total_price = $amount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // create items transaction
        $items = [];
        $tmpItem = new \stdClass();
        $tmpItem->itemReference = $referralTransactionId;
        $tmpItem->description = $remarks;
        $tmpItem->price = $amount;
        $tmpItem->status = 'PAID';
        $tmpItem->remarks = $remarks;
        $items[] = $tmpItem;

        // initiate model item
        $transactionItemModel = new TransactionItem();

        // save to items
        $itemsDb = $transactionItemModel->insertTransactionItems($transactionDb->id,$items);

        if (!$itemsDb->isSuccess){
            $response->errorMsg = $itemsDb->errorMsg;
            return $response;
        }

        // insert transaction history
        $historyModel = new TransactionHistory();
        $transactionId = $transactionDb->id;
        $userName = $userDb->name;
        $description = $remarks;
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$amount,$amount,0,0,'PAID','');

        $response->transactionId = $transactionDb->id;
        $response->transactionInvoice = $invoiceId;
        $response->transactionAmount = $amount;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Delivery Transaction
     * @param $userId
     * @param $deliveryId
     * @param int $promoAmount
     * @return \stdClass
     */
    public function createDeliveryTransaction($userId,$deliveryId,$promoAmount=0){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionInvoice = null;
        $response->transactionAmount = null;

        // get user DB
        $userDb = User::find($userId);
        if (!$userId){
            $response->errorMsg = "User Not Valid";
            return $response;
        }
        $country = $userDb->country;

        // generate invoice id
        $invoiceId = "TRX-".$userId.date('ymdh').Helper::generateRandomString(4);

        // get delivery DB
        $deliveryDb = Delivery::find($deliveryId);

        $totalPrice = $deliveryDb->total_price;
        $remarks = 'Delivery '.$deliveryDb->invoice_code;

        $paidAmount = $totalPrice - $promoAmount;

        // save to table Transaction
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_id_reference = $deliveryId;
        $transactionDb->transaction_type = 'delivery';
        $transactionDb->description = $remarks;
        $transactionDb->total_price = $totalPrice;
        $transactionDb->paid_amount = $paidAmount;
        $transactionDb->promo_amount = $promoAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // create items transaction
        $destinations = $deliveryDb->destinations;

        $items = [];
        $destinationCount = $destinations->count();
        $promoItem = 0;
        if (!empty($promoAmount)){
            if ($country == 'ID') $promoItem = round(( $promoAmount /  $destinationCount),0);
            else $promoItem = $promoAmount /  $destinationCount;
        }
        foreach ($destinations as $item) {
            $paidItem =  $item->price - $promoItem;
            $tmpItem = new \stdClass();
            $tmpItem->itemReference = $item->id;
            $description = "Delivery $deliveryDb->invoice_code sub $item->invoice_sub_code";
            $tmpItem->description = $description;
            $tmpItem->price = $item->price;
            $tmpItem->paidAmount = $paidItem;
            $tmpItem->promoAmount = $promoItem;
            $tmpItem->status = 'PAID';
            $tmpItem->remarks = $remarks;
            $items[] = $tmpItem;
        }

        // initiate model item
        $transactionItemModel = new TransactionItem();
        // save to items
        $itemsDb = $transactionItemModel->insertTransactionItems($transactionDb->id,$items);

        if (!$itemsDb->isSuccess){
            $response->errorMsg = $itemsDb->errorMsg;
            return $response;
        }

        // insert transaction history
        $historyModel = new TransactionHistory();
        $transactionId = $transactionDb->id;
        $userName = $userDb->name;
        $description = $remarks;
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$totalPrice,$paidAmount,$promoAmount,0,'PAID','');

        $response->transactionId = $transactionDb->id;
        $response->transactionInvoice = $invoiceId;
        $response->transactionAmount = $totalPrice;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create PopSafe Transaction
     * @param $userId
     * @param $popSafeId
     * @param int $promoAmount
     * @param null $remarks
     * @return \stdClass
     */
    public function createPopsafeTransaction ($userId,$popSafeId,$promoAmount=0,$remarks = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionInvoice = null;
        $response->transactionAmount = null;

        // get user DB
        $userDb = User::find($userId);
        if (!$userId){
            $response->errorMsg = "User Not Valid";
            return $response;
        }

        $popSafeDb = Popsafe::find($popSafeId);
        if (!$popSafeDb){
            $response->errorMsg = 'Invalid PopSafe Data';
            return $response;
        }
        $popSafeCode = $popSafeDb->invoice_code;
        $lockerName = $popSafeDb->locker_name;

        $totalPrice = $popSafeDb->total_price;
        $paidAmount = $totalPrice - $promoAmount;

        if (empty($remarks)) $remarks = "PopSafe $popSafeCode @ $lockerName";

        // generate invoice id
        $invoiceId = "TRX-".$userId.date('ymdh').Helper::generateRandomString(4);

        // save to table Transaction
        // save to table Transaction
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_id_reference = $popSafeId;
        $transactionDb->transaction_type = 'popsafe';
        $transactionDb->description = $remarks;
        $transactionDb->total_price = $totalPrice;
        $transactionDb->paid_amount = $paidAmount;
        $transactionDb->promo_amount = $promoAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // create items transaction
        $items = [];
        $tmpItem = new \stdClass();
        $tmpItem->itemReference = $popSafeId;
        $tmpItem->description = $remarks;
        $tmpItem->price = $totalPrice;
        $tmpItem->paidAmount = $paidAmount;
        $tmpItem->promoAmount = $promoAmount;
        $tmpItem->status = 'PAID';
        $tmpItem->remarks = $remarks;

        $items[] = $tmpItem;

        // initiate model item
        $transactionItemModel = new TransactionItem();
        // save to items
        $itemsDb = $transactionItemModel->insertTransactionItems($transactionDb->id,$items);

        if (!$itemsDb->isSuccess){
            $response->errorMsg = $itemsDb->errorMsg;
            return $response;
        }

        // insert transaction history
        $historyModel = new TransactionHistory();
        $transactionId = $transactionDb->id;
        $userName = $userDb->name;
        $description = $remarks;
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$totalPrice,$paidAmount,$promoAmount,0,'CREATED',$remarks);

        $response->transactionId = $transactionDb->id;
        $response->transactionInvoice = $invoiceId;
        $response->transactionAmount = $totalPrice;
        $response->isSuccess = true;
        return $response;
    }

    public function createPopsafeLockerTransaction ($invoiceId, $userId,$popSafeId,$promoAmount=0,$remarks = null, $paymentMethodId, $companies, $paymentAmount=null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionInvoice = null;
        $response->transactionAmount = null;

        // get user DB
        if ($userId != "extend") {
            $userDb = User::find($userId);
            if (!$userId){
                $response->errorMsg = "User Not Valid";
                return $response;
            }
        }

        $popSafeDb = Popsafe::find($popSafeId);
        if (!$popSafeDb){
            $response->errorMsg = 'Invalid PopSafe Data';
            return $response;
        }
        $popSafeCode = $popSafeDb->invoice_code;
        $lockerName = $popSafeDb->locker_name;

        $totalPrice = ($paymentAmount == null) ? $popSafeDb->total_price : $paymentAmount;
        $paidAmount = $totalPrice - $promoAmount;

        $getMdr = $this->getMdr($companies->client_id, $paymentMethodId);
        
        if (!$getMdr->isSuccess) {
            $service_charge = null;
            $id_mdr = null;
        } else { 
            $id_mdr = $getMdr->data->id;
            if ($getMdr->data->type == 'percent') {
                $service_charge = ($paidAmount * $getMdr->data->mdr_charge) / 100;
            } else {
                $service_charge = $getMdr->data->mdr_charge;
            }
        }

        if (empty($remarks)) $remarks = "[Direct Order] PopSafe $popSafeCode @ $lockerName";

        $trxLockerOrder = "TRX-".$userId.date('ymdh').Helper::generateRandomString(4);
        // save to table Transaction
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->client_id_companies = $companies->client_id;
        $transactionDb->invoice_id = $trxLockerOrder;
        $transactionDb->transaction_id_reference = $popSafeId;
        $transactionDb->transaction_type = 'popsafe';
        $transactionDb->description = $remarks;
        $transactionDb->total_price = $totalPrice;
        $transactionDb->paid_amount = $paidAmount;
        $transactionDb->promo_amount = $promoAmount;
        $transactionDb->mdr_id = $id_mdr;
        $transactionDb->service_charge = $service_charge;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // create items transaction
        $items = [];
        $tmpItem = new \stdClass();
        $tmpItem->itemReference = $popSafeId;
        $tmpItem->description = $remarks;
        $tmpItem->price = $totalPrice;
        $tmpItem->paidAmount = $paidAmount;
        $tmpItem->promoAmount = $promoAmount;
        $tmpItem->status = 'PAID';
        $tmpItem->remarks = $remarks;

        $items[] = $tmpItem;

        // initiate model item
        $transactionItemModel = new TransactionItem();
        // save to items
        $itemsDb = $transactionItemModel->insertTransactionItems($transactionDb->id,$items);

        if (!$itemsDb->isSuccess){
            $response->errorMsg = $itemsDb->errorMsg;
            return $response;
        }

        // insert transaction history
        $historyModel = new TransactionHistory();
        $transactionId = $transactionDb->id;
        $userName = $userDb->name;
        $description = $remarks;
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$totalPrice,$paidAmount,$promoAmount,0,'CREATED',$remarks);

        // Insert Payment Transaction

        $response->transactionId = $transactionDb->id;
        $response->transactionInvoice = $invoiceId;
        $response->transactionAmount = $totalPrice;
        $response->isSuccess = true;
        return $response;
    }

    private function getMdr($client_id, $payment_id) {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        $mdr = MerchantDiscountRate::where('client_id', $client_id)->where('payment_method_id', $payment_id)->first();
        if (!$mdr) {
            return $response;
        }

        $response->isSuccess = true;
        $response->data = $mdr;
        return $response;
    }

    /**
     * Create TopUp Transaction
     * @param $userId
     * @param $referralTransactionId
     * @param $invoiceId
     * @param $amount
     * @param $paidAmount
     * @param int $promoAmount
     * @param null $remarks
     * @return \stdClass
     */
    public function createTopupTransaction ($userId,$referralTransactionId,$invoiceId,$amount,$paidAmount,$promoAmount=0,$remarks=null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionInvoice = null;
        $response->transactionAmount = null;

        // get user DB
        $userDb = User::find($userId);
        if (!$userId){
            $response->errorMsg = "User Not Valid";
            return $response;
        }

        // save to table Transaction
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_id_reference = $referralTransactionId;
        $transactionDb->transaction_type = 'topup';
        $transactionDb->description = $remarks;
        $transactionDb->total_price = $amount;
        $transactionDb->status = 'CREATE';
        $transactionDb->promo_amount = $promoAmount;
        $transactionDb->paid_amount = $paidAmount;
        $transactionDb->refund_amount = 0;
        $transactionDb->save();

        // create items transaction
        $items = [];
        $tmpItem = new \stdClass();
        $tmpItem->itemReference = $referralTransactionId;
        $tmpItem->description = $remarks;
        $tmpItem->price = $amount;
        $tmpItem->status = 'CREATE';
        $tmpItem->paidAmount = $amount;
        $tmpItem->remarks = $remarks;

        $items[] = $tmpItem;

        // initiate model item
        $transactionItemModel = new TransactionItem();
        // save to items
        $itemsDb = $transactionItemModel->insertTransactionItems($transactionDb->id,$items);

        if (!$itemsDb->isSuccess){
            $response->errorMsg = $itemsDb->errorMsg;
            return $response;
        }

        // insert transaction history
        $historyModel = new TransactionHistory();
        $transactionId = $transactionDb->id;
        $userName = $userDb->name;
        $description = $remarks;
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$amount,$paidAmount,$promoAmount,0,'CREATE','');

        $response->transactionId = $transactionDb->id;
        $response->transactionInvoice = $invoiceId;
        $response->transactionAmount = $amount;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Get Transaction history
     * @param $userId
     * @param $transaction_type
     * @return \stdClass
     */
    public function getHistory ($userId,$transaction_type)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->orderId = null;

        // get user ID
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }

        $historyDb = self::where('transaction_type','=',$transaction_type)->with(
            array(
                'transactionItems'  => function ($query) {
                    $query->select(
                        'id','transaction_id','description','price','paid_amount','promo_amount','refund_amount',
                        'status','remarks'
                    );
                }
            )
        )
            ->select(
                'id','user_id','invoice_id','transaction_id_reference','transaction_type','description','total_price',
                'status','paid_amount','promo_amount','refund_amount','created_at'
            )
            ->where('user_id','=',$userId)
            ->orderBy('created_at','desc')
            ->paginate(10);

        if ($historyDb->isEmpty()){
            $response->errorMsg = "You don't have any transactions";
            return $response;
        }
        $response->isSuccess = true;
        $response->pagination = (int)ceil($historyDb->total()/$historyDb->perPage());
        $response->data = $historyDb->items();

        return $response;
    }

    public function refundTransaction($transactionId,$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get transaction
        $transactionDb = Transaction::with('items')
            ->find($transactionId);

        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        // get balance record debit
        $balanceRecordDb = BalanceRecord::where('transaction_id',$transactionId)->first();
        if (!$balanceRecordDb){
            $response->errorMsg = 'Balance Record Not Found';
            return $response;
        }

        $debitAmount = $balanceRecordDb->debit;
        $refundAmount = $debitAmount;
        // create refund transaction
        $userId = $transactionDb->user_id;
        $user = User::find($userId);
        $description = "Refund Transaction ".$transactionDb->description." by ".$user->name;

        // create refund Transaction
        $createRefundTransaction = $this->createRefundTransaction($userId,$description,$transactionDb->id,$refundAmount);
        if (!$createRefundTransaction->isSuccess){
            $response->errorMsg = 'Failed Create Refund Transaction';
            return $response;
        }
        $refundTransactionId = $createRefundTransaction->transactionId;

        // change transaction to refund
        $transactionDb->status = 'REFUND';
        $transactionDb->refund_amount = $refundAmount;
        $transactionDb->save();

        // change transaction item to refund
        $transactionItems = $transactionDb->items;
        foreach ($transactionItems as $item) {
            $itemDb = TransactionItem::find($item->id);
            $itemDb->status = 'REFUND';
            $itemDb->refund_amount = $itemDb->paid_amount;
            $itemDb->save();
        }

        // create transaction history
        $userName = "PopBox: ".$user->name;
        $transactionHistoryModel = new TransactionHistory();
        $insertHistory = $transactionHistoryModel->insertHistory($transactionId,$userName,$description,$transactionDb->total_price,$transactionDb->paid_amount,$transactionDb->promo_amount,$transactionDb->refund_amount,'REFUND',$remarks);

        // refund user balance
        $balanceModel = new BalanceRecord();
        $creditBalance = $balanceModel->creditDeposit($userId,$refundAmount,$refundTransactionId);
        if (!$creditBalance->isSuccess){
            $response->errorMsg = $creditBalance->errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Refund Transaction
     * @param $userId
     * @param $description
     * @param $previousTransactionId
     * @param $refundAmount
     * @return \stdClass
     */
    private function createRefundTransaction($userId,$description,$previousTransactionId,$refundAmount){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        // create transaction Reference
        $reference = "REF-".$userId.date('ymdh').Helper::generateRandomString(4);

        // save to Database
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $reference;
        $transactionDb->transaction_id_reference = $previousTransactionId;
        $transactionDb->transaction_type = 'refund';
        $transactionDb->description = $description;
        $transactionDb->total_price = $refundAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->paid_amount = $refundAmount;
        $transactionDb->save();

        // get refunded transactionDb
        $refundedTransactionDb = self::with('items')->find($previousTransactionId);

        // create item transaction
        foreach ($refundedTransactionDb->items as $item) {
            // insert transaction item
            $transactionItemDb = new TransactionItem();
            $transactionItemDb->transaction_id = $transactionDb->id;
            $transactionItemDb->item_reference = $item->id;
            $transactionItemDb->description = "Refund Transaction ".$item->description;
            $transactionItemDb->price = 0;
            $transactionItemDb->status = 'PAID';
            $transactionItemDb->remarks = $description;
            $transactionItemDb->save();
        }

        $response->isSuccess = true;
        $response->transactionId = $transactionDb->id;
        return $response;
    }
    
    /**
     * Create Credit Transaction
     * @param $type
     * @param $userId
     * @param $remarks
     * @param $amount
     * @return \stdClass
     */
    public function createTransaction($type, $userDb, $remarks="", $amount){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionRef = null;
        
        // create invoice ID
        $invoiceId = 'TRX-' . $userDb->id . date('ymdhi') . Helper::generateRandomString(3);
        
        $transactionDb = new self();
        $transactionDb->user_id = $userDb->id;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_id_reference = 0;
        $transactionDb->transaction_type = $type;
        $transactionDb->description = "$remarks Action from : " . $userDb->name;
        // create total price
        $transactionDb->total_price = $amount;
        $transactionDb->paid_amount = $amount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();
        
        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        
        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->item_reference = null;
        $itemsDb->description = "$remarks Action from : " . $userDb->name;
        $itemsDb->price = $amount;
        $itemsDb->paid_amount = $amount;
        $itemsDb->status = 'PAID';
        $itemsDb->remarks = $remarks;
        $itemsDb->save();
        
        // insert history
        $history = new TransactionHistory();
        $userName = "System : ".$userDb->name;
        $description = "$remarks Action from : ".$userDb->name;
        $insertHistory = $history->insertHistory($transactionId,$userName,$description,$amount,$amount,0,0,'PAID',$remarks);
        
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $invoiceId;
        return $response;
    }


    /*====================Relationship====================*/
    public function transactionItems ()
    {
        return $this->hasMany(TransactionItem::class,'transaction_id','id');
    }

    public function campaignUsage(){
        return $this->hasOne(CampaignUsage::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function items(){
        return $this->hasMany(TransactionItem::class);
    }

    public function popsafe(){
        return $this->hasOne(Popsafe::class,'id','transaction_id_reference');
    }

    public function delivery(){
        return $this->hasOne(Delivery::class,'id','transaction_id_reference');
    }

    public function histories(){
        return $this->hasMany(TransactionHistory::class);
    }

    public function campaign(){
        return $this->hasOne(CampaignUsage::class);
    }
}

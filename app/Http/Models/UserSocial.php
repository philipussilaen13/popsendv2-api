<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    public static function checkExistingUser ($provider_name,$providerid)
    {

        $response = new \stdClass();
        $response->isFound = false;

        $userSocialDb = self::where([
            ['provider','=',$provider_name],
            ['provider_id','=',$providerid]
        ])->first();

        if ($userSocialDb){
            $response->isFound = true;
            $response->user_id = $userSocialDb->user_id;
            return $response;
        }

        return $response;
    }

    public static function insertSocialMedia ($memberId,$provider_id,$provider_name)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $userSocialDb = new self();
        $userSocialDb->user_id = $memberId;
        $userSocialDb->provider_id = $provider_id;
        $userSocialDb->provider = $provider_name;
        $userSocialDb->status = 'ENABLE';
        $userSocialDb ->save();

        $response->isSuccess = true;

        return $response;
    }
}

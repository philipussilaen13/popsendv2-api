<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PopsafeHistory extends Model
{
    /**
     * Insert Popsafe history
     * @param $deliveryId
     * @param $status
     * @param null $user
     * @param null $remarks
     */

    protected $table = 'popsafe_histories';

    public static function insertPopsafeHistory($popsafeId,$status,$user=null,$remarks=null,$locker_time=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $historyDb = new self();
        $historyDb->popsafe_id = $popsafeId;
        $historyDb->status = $status;
        $historyDb->user = $user;
        $historyDb->remarks = $remarks;
        $historyDb->locker_time = $locker_time;
        $historyDb->save();

        $response->isSuccess = true;
        return $response;
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function choices ()
    {
        return $this->hasMany(Choise::class, 'question_id', 'id');
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryDestinationHistory extends Model
{
    protected $table = 'delivery_destination_histories';
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FreeGiftCategory extends Model
{

    /**
     * check avaliable category
     * @param $title
     * @return \stdClass
     */
    public function checkCategoryCode ($title)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $categoryCode = str_slug($title, '-');
        $cagory = self::where('category_code',$categoryCode)->first();

        if ($cagory) {
            $errorMsg = 'Category All Ready In Database';
            $response->errorMsg = $errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * cek post category
     * @param $request
     * @param $imageUrl
     * @return \stdClass|string
     */
    public function postCategory ($request,$imageUrl)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        /*generate category code baseOn title*/
        $title = $request->input('title');
        $categoryCode = str_slug($title, '-');


        /*Insert */
        $category = new self();
        $category->title = $title;
        $category->category_code = $categoryCode;
        $category->icon = $imageUrl;
        $category->avaliable_country = json_encode($request->input('avaliable_country'));
        $category->save();

        if (!$category) {
            /*Error*/
            $errorMsg = 'Insert Wrong';
            $response->errorMsg = $errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        $response->data = $category;
        return $response;
    }
    /**
     * get All Category By Country User
     * @param $userCountry
     * @return \stdClass
     */
    public function getAllCategory ($userCountry)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $categories = self::all();
        $categories_ = [];
        foreach ($categories as $category) {
            foreach (json_decode($category->avaliable_country) as $country) {
                if ($country == $userCountry) {
                    $categories_[] = $category;
                }
            }
            $icon = url('images/free_gift/categories/')."/".$category->icon;
            $category->icon = $icon;
        }

        if (empty($categories_)) {
            $errorMsg = 'Category Not Found';
            $response->errorMsg = $errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        $response->data = $categories_;
        return $response;
    }

    public function getQuestions ($request,$countryUser)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $questions  = self:: where('category_code',$request->input('category_code'))
            ->with('questions','questions.choices')->get();

        $questions_ = [];
        foreach ($questions as $question) {
            foreach ($question->questions as $value) {
                foreach (json_decode($value->avaliable_country) as $country) {
                    if ($country == $countryUser) {
                        $questions_ = $question;
                    }
                }
            }
        }
        if (empty($questions_)) {
            $errorMsg = 'Questions Not Found';
            $response->errorMsg = $errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        $response->data = $questions_;
        return $response;
    }

    /*RELATIONS*/

    public function questions ()
    {
        return $this->hasMany(Question::class, 'category_id', 'id');
    }
}

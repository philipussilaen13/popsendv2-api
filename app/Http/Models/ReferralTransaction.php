<?php

namespace App\Http\Models;

use App\Http\Models\ReferralCampaign;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ReferralTransaction extends Model
{
    protected $table = 'referral_transactions';

    /**
     * Checking Active Referral
     * @param $referralCode
     * @return \stdClass
     */
    public function checkActiveCampaign($referralCode){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->campaignId = null;

        $nowDateTime = date('Y-m-d H:i:s');
        // check if exist on special code
        $campaignSpecialDb = ReferralCampaign::where('code',$referralCode)
            ->where('start_date','<=',$nowDateTime)
            ->where('end_date','>=',$nowDateTime)
            ->first();
        // if exist campaign special
        if ($campaignSpecialDb){
            $response->isSuccess = true;
            $response->campaignId = $campaignSpecialDb->id;
        }

        // check if code valid
        $userDb = User::where('referral_code',$referralCode)->first();
        if (!$userDb){
            $response->errorMsg = 'Invalid Referral Code';
            return $response;
        }

        // if not exist then get regular campaign
        $campaignRegularDb = ReferralCampaign::where('status',1)
            ->where('start_date','<=',$nowDateTime)
            ->where('end_date','>=',$nowDateTime)
            ->whereNull('code')
            ->orderBy('created_at','desc')
            ->first();

        if (!$campaignRegularDb){
            $response->errorMsg = 'No Active Referral Campaign';
            return $response;
        }

        $response->isSuccess = true;
        $response->campaignId = $campaignRegularDb->id;
        return $response;
    }

    /**
     * Create Referral Transaction
     * @param $referralCode
     * @param $campaignId
     * @param $toMemberId
     * @return \stdClass
     */
    public function createReferralTransaction($referralCode,$campaignId,$toMemberId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get Referral Campaign
        $campaignDb = ReferralCampaign::find($campaignId);
        if (!$campaignDb){
            $response->errorMsg = 'Campaign Referral Not Found';
            return $response;
        }
        $fromMemberId = null;

        // find from member id if exist
        $userDb = User::where('referral_code',$referralCode)->first();
        if ($userDb){
            $fromMemberId = $userDb->id;
        }

        // find new registered member Id
        $toMemberDb = User::where('member_id',$toMemberId)->first();
        if (!$toMemberId){
            $response->errorMsg = 'New Member Not Found';
            return $response;
        }
        $expiredDate = date('Y-m-d H:i:s',strtotime("+$campaignDb->expired days"));
        // insert to database
        $transactionDb = new self();
        $transactionDb->referral_campaign_id = $campaignDb->id;
        $transactionDb->code = $referralCode;
        $transactionDb->from_user_id = $fromMemberId;
        $transactionDb->to_user_id = $toMemberDb->id;
        $transactionDb->status = 'PENDING';
        $transactionDb->submit_date = date('Y-m-d H:i:s');
        $transactionDb->expired_date = $expiredDate;
        $transactionDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Checking Referral Transaction Rule Pass
     * @param $toUserId
     * @param $type
     * @param null $rule
     * @return \stdClass
     */
    public function checkRulePass($toUserId,$type,$rule=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->isFailed = false;

        if ($type == 'topup'){
            // check if user has Top Up Transaction
            $userDb = User::find($toUserId);
            if (!$userDb){
                $response->errorMsg = "To Member ID $toUserId not Found";
                return $response;
            }
            // check topup transaction
            $transactionTopUpDb = Transaction::where('user_id',$userDb->id)
                ->where('transaction_type','topup')
                ->where('status','PAID')
                ->orderBy('id','asc')
                ->first();
            if (!$transactionTopUpDb){
                $response->errorMsg = "First Top Up user $userDb->member_id Not Found";
                return $response;
            }
            if (!empty($rule) && !empty($transactionTopUpDb)){
                $topUpAmount = $transactionTopUpDb->total_price;
                $schemaTransactionRule = json_decode($rule);
                $transactionRuleValue = reset($schemaTransactionRule);
                $transactionRuleOperator = key($schemaTransactionRule);
                if ($transactionRuleOperator == 'between'){
                    $transactionRuleValue = explode('-',$transactionRuleValue);
                    if ($topUpAmount >= $transactionRuleValue[0] && $topUpAmount <= $transactionRuleValue[1]){
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed TopUp Rule Between '.$rule." - $topUpAmount";
                        $response->isFailed = true;
                    }
                }
                elseif ($transactionRuleOperator == '>'){
                    if ($topUpAmount > $transactionRuleValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule > '."$rule - $topUpAmount";
                        $response->isFailed = true;
                    }
                } elseif ($transactionRuleOperator == '>='){
                    if ($topUpAmount >= $transactionRuleValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule >= '."$rule - $topUpAmount";
                        $response->isFailed = true;
                    }
                } elseif ($transactionRuleOperator == '<'){
                    if ($topUpAmount < $transactionRuleValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule < '."$rule - $topUpAmount";
                        $response->isFailed = true;
                    }
                } elseif ($transactionRuleOperator == '<='){
                    if ($topUpAmount <= $transactionRuleValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule <= '."$rule - $topUpAmount";
                        $response->isFailed = true;
                    }
                }
            }
            else {
                $response->errorMsg = 'First Top Up And Rule Not Found';
            }
        }
        elseif ($type == 'register'){
            // get member destination
            $userDb = User::find($toUserId);
            if ($userDb){
                $response->isSuccess = true;
            } else {
                $response->errorMsg = 'Member Not Found';
            }
        } else {
            $response->errorMsg = 'Invalid Type';
        }

        return $response;
    }

    /**
     * Process Referral Transaction
     * @param null $fromUserId
     * @param $toUserId
     * @param int $fromAmount
     * @param int $toAmount
     * @param $code
     * @param $referralTransactionId
     * @return \stdClass
     */
    public function processReferral($fromUserId=null,$toUserId,$fromAmount=0,$toAmount=0,$code,$referralTransactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $location = storage_path()."/logs/cron/";

        $toUserDb = User::find($toUserId);
        if (!$toUserDb){
            $response->errorMsg = 'To User Not Found';
            return $response;
        }

        // initiate Transaction model
        $transactionModel = new Transaction();

        $message = "Process From User ID $fromUserId";
        Logs::logFile($location,'referralTransaction',$message);
        if (!empty($fromUserId)){
            $fromUserDb = User::find($fromUserId);
            if (!$fromUserDb){
                $response->errorMsg = 'From User Not Found';
                return $response;
            }
            $remarks = "Referral $code From $fromUserDb->name to $toUserDb->name";

            // create transaction referral
            $insertFromTransaction = $transactionModel->createReferralTransaction($fromUserId,$fromAmount,$referralTransactionId,$remarks);
            if (!$insertFromTransaction->isSuccess){
                $message = "Failed Create Transaction ".$insertFromTransaction->errorMsg;
                Logs::logFile($location,'referralTransaction',$message);
                $response->errorMsg = $message;
                return $response;
            }

            // credit deposit
            $balanceRecordModel = new BalanceRecord();
            $creditDeposit = $balanceRecordModel->creditDeposit($fromUserId,$fromAmount,$insertFromTransaction->transactionId);
            if (!$creditDeposit->isSuccess){

                $message = "Failed Create Transaction ".$creditDeposit->errorMsg;
                Logs::logFile($location,'referralTransaction',$message);
                $response->errorMsg = $message;
                return $response;
            }
        }
        else {
            $message = 'From User Empty '.$fromUserId;
            Logs::logFile($location,'referralTransaction',$message);
        }

        // create transaction for to user
        $userDb = User::find($toUserId);
        if (!$userDb){
            $message = 'To User Not Found';
            Logs::logFile($location,'referralTransaction',$message);
            $response->errorMsg = $message;
            return $response;
        }

        if ($fromUserId != null) {
            $remarks = "Referral $code From $fromUserDb->name to $toUserDb->name";
        } else {
            $remarks = "Referral $code to $toUserDb->name";
        }

        // create transaction referral
        $insertToTransaction = $transactionModel->createReferralTransaction($toUserId,$toAmount,$referralTransactionId,$remarks);
        if (!$insertToTransaction->isSuccess){
            $message = "Failed Create Transaction ".$insertToTransaction->errorMsg;
            Logs::logFile($location,'referralTransaction',$message);
            $response->errorMsg = $message;
            return $response;
        }
        // credit deposit
        $balanceRecordModel = new BalanceRecord();
        $creditDeposit = $balanceRecordModel->creditDeposit($toUserId,$toAmount,$insertToTransaction->transactionId);
        if (!$creditDeposit->isSuccess){
            $message = "Failed Create Transaction ".$creditDeposit->errorMsg;
            Logs::logFile($location,'referralTransaction',$message);
            $response->errorMsg = $message;
            return $response;
        }

        // update referral transaction
        $referralDb = self::find($referralTransactionId);
        $referralDb->used_date = date('Y-m-d H:i:s');
        $referralDb->status = 'USED';
        $referralDb->save();

        $response->isSuccess = true;
        $message = 'Success Process Referral';
        Logs::logFile($location,'referralTransaction',$message);
        return $response;
    }

    /*=====================Relationship=====================*/

    public function campaign(){
        return $this->belongsTo(ReferralCampaign::class,'referral_campaign_id','id');
    }
}

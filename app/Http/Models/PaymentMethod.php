<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    //table 
    protected $table = 'payment_methods';


    public function getMethod($method)
    {
        $res = new \stdClass();
        $res->isSuccess = false;
        $res->data = null;
        $res->message = "Payment CLOSE";

        // if ($method == "EMONEY"){
        //     $method = "MANDIRI-EMONEY";
        // }

        $data = PaymentMethod::where('code', $method)->first();
        
        if (isset($data->status_channel) == "OPEN") {
            $res->isSuccess = true;
            $res->data = $data;
            $res->message = "Payment OPEN";
            return $res;
        } else {
            $res->message = "Payment Undefined";
        }

        return $res;
    }
}

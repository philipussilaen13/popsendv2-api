<?php

namespace App\Http\Models\Partner;

use Illuminate\Database\Eloquent\Model;

class PartnerHowTo extends Model {

    protected $table = 'how_to';

    public function partner() {
        return $this->belongsTo(Partner::class);
    }
}
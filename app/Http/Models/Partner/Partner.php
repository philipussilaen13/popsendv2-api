<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 26/06/18
 * Time: 22.50
 */

namespace App\Http\Models\Partner;


use Illuminate\Database\Eloquent\Model;

class Partner extends Model {

    protected $table = 'partner';

    public function howto() {
        return $this->hasMany(PartnerHowTo::class);
    }
}
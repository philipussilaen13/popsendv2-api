<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 28/12/2017
 * Time: 09.20
 */

namespace App\Http\Models;


use Illuminate\Support\Facades\DB;

class ApiToken
{
    protected $table = 'api_tokens';

    public static function checkToken($token){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->tokenId = null;

        $apiTokenDb = DB::table('api_tokens')
            ->where('token',$token)
            ->first();

        if (!$apiTokenDb){
            $response->errorMsg = 'Invalid Token';
            return $response;
        }

        $response->isSuccess = true;
        $response->tokenId = $apiTokenDb->id;
        return $response;
    }

    public function checkClientByToken($token)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errMessage = null;

        $data = DB::table('api_tokens')
                ->join('companies', 'api_tokens.id', '=', 'companies.id_api_token')
                ->where('api_tokens.token', $token)
                ->first();
        
        if (!$data) {
            $errMessage = "Client Not Found";
            $response->errMessage = $errMessage;
            return $response;
        } elseif ($data->status == 'NOT_ACTIVE') {
            $errMessage = "Client Not Active";
            $response->errMessage = $errMessage;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'id_api_token', 'id');
    }
}
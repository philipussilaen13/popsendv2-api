<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 27/12/2017
 * Time: 11.41
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Logs extends Model
{
    protected $table = 'logs';

    /**
     * Insert Log Request to DB
     * @param $moduleName
     * @param $apiTokenId
     * @param $sessionId
     * @param Request $request
     * @param null $userId
     * @return mixed
     */
    public static function insertLog($moduleName,$apiTokenId,$sessionId,Request $request,$userId = null){
        $ipAddress = $request->ip();
        $input = $request->except('password');
        if (!empty($input['item_photo'])){
            unset($input['item_photo']);
        }

        $request = json_encode($input);

        $logsDb = new self();
        $logsDb->modules = $moduleName;
        $logsDb->user_id = $userId;
        $logsDb->api_token_id = $apiTokenId;
        $logsDb->session_id = $sessionId;
        $logsDb->ip_address = $ipAddress;
        $logsDb->request = $request;
        $logsDb->latency = 0;
        $logsDb->save();

        $id = $logsDb->id;

        return $id;
    }

    /**
     * Insert Response Log to DB
     * @param $logId
     * @param $response
     * @param float $latency
     */
    public static function updateResponseLog($logId,$response,$latency=0.0){
        $logsDb = self::find($logId);
        $logsDb->response = $response->content();
        $logsDb->latency = $latency;
        $logsDb->save();
    }
    /**
     * @param string $location
     * @param string $module
     * @param string $message
     */
    public static function logFile($location="",$module="",$message=""){
        if (empty($location)){
            $location = storage_path()."/logs/";
        }
        if (!is_dir($location)){
            mkdir($location,0777,true);
        }
        $user = get_current_user();
        $msg = " $message\n";
        $i = 1;
        $fileName = $location."$module.".date('Y.m.d-').$user."-$i.log";

        if (file_exists($fileName)){
            while (file_exists($fileName) && filesize($fileName) > 10000000){
                $i++;
                $fileName = $location."$module.".date('Y.m.d-').$user."-$i.log";
            }
        }

        $f = fopen($fileName,'a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
        return;
    }

}
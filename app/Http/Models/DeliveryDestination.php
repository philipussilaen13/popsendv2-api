<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use Illuminate\Database\Eloquent\Model;

class DeliveryDestination extends Model
{
    /**
     * Input Delivery Destinations
     * @param $deliveryId
     * @param $destinations
     * @param array $inputDestination
     * @param $userName
     * @return \stdClass
     */
    public function insertDestinations($deliveryId,$destinations,$inputDestination=[],$userName=null,$pickupType,$country){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        foreach ($destinations as $index => $destination) {
            $type = $destination->destination_type;
            $name = $destination->destination_name;
            $address = $destination->destination_address;
            $latitude = $destination->destination_latitude;
            $longitude = $destination->destination_longitude;
            $districtId = $destination->districtId;
            $weight = $destination->weight;
            $price = $destination->price;

            $recipientName = $inputDestination[$index]['recipient_name'];
            $recipientPhone = $inputDestination[$index]['recipient_phone'];
            $categoryId = empty($inputDestination[$index]['category_id']) ? null : $inputDestination[$index]['category_id'];
            $detailAddress = empty($inputDestination[$index]['address_detail']) ? null : $inputDestination[$index]['address_detail'];

            // CHECK IF NOTES ISEMPTY
//            $notes = empty($inputDestination[$index]['item_description']) ? "-" : $inputDestination[$index]['item_description'];
            $notes = '-';
            if (!empty($inputDestination[$index]['item_description'])) {
                $notes = $inputDestination[$index]['item_description'];
            } else if (!empty($inputDestination[$index]['notes'])) {
                $notes = $inputDestination[$index]['notes'];
            }

            if (!$destination->isValid){
                $response->errorMsg = "Destination $type $name is Not Valid";
                return $response;
            }

            // check category
            $categoryName = null;
            if (!empty($categoryId)){
                $categoryDb = CategoryList::find($categoryId);
                if (!$categoryDb){
                    $response->errorMsg = 'Invalid Category';
                    return $response;
                }
                $categoryName = $categoryDb->name;
            }

            // $invoiceId  = 'SEND-S'.$deliveryId.date('ymdhi').Helper::generateRandomString(3);
            $invoiceId  = Helper::generateInvoiceCode($pickupType,'destinations',$destination->destination_type,$country);

            // save to DB
            $destinationDb = new self();
            $destinationDb->delivery_id = $deliveryId;
            $destinationDb->district_id = $districtId;
            $destinationDb->destination_type = $type;
            $destinationDb->invoice_sub_code = $invoiceId;
            $destinationDb->destination_name = $name;
            $destinationDb->destination_address = $address;
            $destinationDb->destination_address_detail = $detailAddress;
            $destinationDb->destination_latitude = $latitude;
            $destinationDb->destination_longitude = $longitude;
            $destinationDb->status = 'CREATED';
            $destinationDb->phone_recipient = $recipientPhone;
            $destinationDb->name_recipient = $recipientName;
            $destinationDb->category = $categoryName;
            $destinationDb->notes = $notes;
            $destinationDb->price = $price;
            $destinationDb->weight = $weight;
            $destinationDb->save();

            $destinationId = $destinationDb->id;

            // insert history
            $this->insertDestinationHistory($destinationId,'CREATED',$userName);
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Insert Destination History
     * @param $destinationId
     * @param $status
     * @param null $user
     * @param null $remarks
     */
    public function insertDestinationHistory($destinationId,$status,$user=null,$remarks=null, $expressDate=null){
        $historyDb = new DeliveryDestinationHistory();
        $historyDb->delivery_destination_id = $destinationId;
        $historyDb->status = $status;
        $historyDb->user = $user;
        $historyDb->remarks = $remarks;
        $historyDb->express_date = $expressDate;
        $historyDb->save();

        return;
    }

    /**
     * @param $recipientPhone
     * @param $orderNumber
     * @return \stdClass
     */
    public function getList($recipientPhone, $orderNumber){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $orderList = [];
        $destinationDb = self::where('phone_recipient',$recipientPhone)
            ->get();

        foreach ($destinationDb as $item) {
            $lockerName = null;
            $lockerName = $item->destination_name;
            $status = 'ON PROCESS';

            if ($item->status == 'CREATED') continue;

            // put to data and orderNumber Array
            $tmp = new \stdClass();
            $tmp->id = $item->id;
            $tmp->order_number = $item->invoice_sub_code;
            $tmp->type = 'popsend';
            $tmp->merchant = 'popsend';
            $tmp->locker = $lockerName;
            $tmp->status = $item->status;
            $tmp->last_update = date('Y-m-d H:i:s',strtotime($item->updated_at));
            $tmp->pin = null;

            $orderList[] = $tmp;
        }

        $response->isSuccess = true;
        $response->data = $orderList;
        return $response;
    }

    /**
     * Update Delivery Status
     * @param $subInvoice
     * @param $status
     * @param $remarks
     * @return \stdClass
     */
    public function updateDeliveryStatus($subInvoice,$status,$remarks, $expressDate){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get delivery Destination DB
        $destinationDb = self::with('delivery')
            ->where('invoice_sub_code',$subInvoice)
            ->first();
        if (!$destinationDb){
            $response->errorMsg = 'Invalid Invoice Code';
            return $response;
        }
        $destinationId = $destinationDb->id;

        $statusList = [
            'ON_DELIVERY' => 'ON_PROCESS',
            'IN_STORE' => 'ON_PROCESS',
            'DELIVERED' => 'COMPLETED',
            'RETURN_ON_PROCESS' => 'ON_PROCESS',
            'RETURN_COMPLETED' => 'COMPLETED',
            'CANCEL' => 'CANCELED',
            'ON_PROCESS' => 'ON_PROCESS',
            'COMPLETED' => 'COMPLETED'
        ];

        $statusText = 'ON_PROCESS';
        if (array_key_exists($status,$statusList)){
            $statusText = $statusList[$status];
        }

        // update self data
        $dataDb = self::find($destinationId);
        $dataDb->status = $statusText;
        $dataDb->save();

        // insert history
        $history = $this->insertDestinationHistory($destinationId,$statusText,'System',$remarks, $expressDate);

        // get delivery DB
        $deliveryDb = $destinationDb->delivery;

        // check if the other destination status is the same. if same, change the master
        $isAll = true;
        $destinationDb = $deliveryDb->destinations;
        foreach ($destinationDb as $item) {
            if ($item->status != $statusText){
                $isAll = false;
                break;
            }
        }
        $deliveryId = $deliveryDb->id;

        // Update Parent delivery
        if ($isAll){
            $deliveryDb = Delivery::find($deliveryId);
            $deliveryDb->status = $statusText;
            $deliveryDb->save();

            // insert history
            $deliveryModel = new Delivery();
            $insertHistory = $deliveryModel->insertDeliveryHistory($deliveryId,$statusText,'System',$remarks);
        }

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/

    public function histories(){
        return $this->hasMany(DeliveryDestinationHistory::class,'delivery_destination_id','id');
    }

    public function delivery(){
        return $this->belongsTo(Delivery::class);
    }
}

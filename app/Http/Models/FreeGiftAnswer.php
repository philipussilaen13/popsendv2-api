<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FreeGiftAnswer extends Model
{
    //

    /*RELATIONS*/

    public function questions ()
    {
        return $this->belongsTo(Question::class,'question_id','id');
    }

    public function free_gift_answer_details ()
    {
        return $this->hasMany(FreeGiftAnswerDetail::class, 'free_gift_answer_id', 'id');
    }
}

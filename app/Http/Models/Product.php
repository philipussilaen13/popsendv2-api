<?php

namespace App\Http\Models;
use App\Http\Library\Helper;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public static function getEnableProduct(){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        // validasi data status = 1 yang artinya enable
        $productDb = self::where('status','=',1)->get();
        if ($productDb->isEmpty()){
            $response->errorMsg = 'Product Not Found';
            return $response;
        }

        foreach($productDb as $i => $product) {
            $data[] = [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description
            ];
        }
    
        $response->isSuccess = true;
        $response->data = $data;

        return $response;
    }
}

<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    /**
     * Payment Transaction
     * @param string $method
     * @param string $country
     * @param $phone
     * @param string $description
     * @param int $amount
     * @param int $paidAmount
     * @return \stdClass
     */
    public static function createPayment ($userId,$method='BNI',$country='ID',$phone,$description="TopUp PopSend",$amount=0,$paidAmount=0){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->paymentId = null;
        $response->reference = null;

        $prefix = "BAL";
        // create reference
        $randomString = Helper::generateRandomString(2);
        $nowDateTime = date('ymdhis');
        $reference = "$prefix-$nowDateTime$randomString";

        $data = new self();
        $data->user_id = $userId;
        $data->reference = $reference;
        $data->method = $method;
        $data->phone = $phone;
        $data->country = $country;
        $data->description = $description;
        $data->amount = $amount;
        $data->paid_amount = $paidAmount;
        $data->status = 'CREATED';
        $data->save();

        $response->isSuccess = true;
        $response->paymentId = $data->id;
        $response->status = $data->status;
        $response->reference = $reference;
        return $response;
    }

    /*Relationship*/
    public function transaction(){
        return $this->hasOne(Transaction::class,'transaction_id_reference','id');
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FreeGiftAnswerDetail extends Model
{
    //

    /*RELATIONS*/
    public function free_gift_answers ()
    {
        return $this->belongsTo(FreeGiftAnswer::class, 'free_gift_answer_id', 'id');
    }
}

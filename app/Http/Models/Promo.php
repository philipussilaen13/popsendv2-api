<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    public static function checkPromo ($phone = '',$promoType='potongan',$param=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->totalPrice = 0;
        $response->totalDiscount = 0;
        $response->campaignName = null;
        $response->campaignDescription = null;

        if (empty($phone)){
            $response->errorMsg = '[Check Process] Empty Phone';
            return $response;
        }

        $param['cust_phone'] = $phone;
        $param['user_phone'] = $phone;
        if(isset($param['locker_name'])) {
            $param['origin_locker_name'] = $param['locker_name'];
        }
        $issetPromoCode = 0;
        if (!empty($param['promo_code'])) $issetPromoCode = 1;

        // get all active campaign, in date start / end, order by priority and date created
        $activeCampaign = Campaign::getActiveCampaign($issetPromoCode,$promoType);
        $isGet = false;
        $campaignData = null;
        if (count($activeCampaign)==0){
            $response->errorMsg = '[Check Process] No Active Campaign';
            return $response;
        }

        // check only promo code
        if ($issetPromoCode==1){
            // check promo code on table
            $voucherDb = Campaign::getVoucherActive($param['promo_code'],$promoType);
            if (count($voucherDb)>0){
                foreach ($voucherDb as $campaign) {
                    $checkCampaign = Campaign::checkRuleCampaign($campaign->campaign_id,$param);
                    $campaignData = $checkCampaign;
                    if ($checkCampaign->isSuccess==true){
                        $isGet = true;
                        break;
                    }
                }
            } else {
                $errorMsg = '[Check Process] Promo Code Not Found';
                $response->errorMsg = $errorMsg;
                return $response;
            }
        } else {
            // check rule foreach campaign
            foreach ($activeCampaign as $campaign) {
                $checkCampaign = Campaign::checkRuleCampaign($campaign->id,$param);
                $campaignData = $checkCampaign;
                if ($checkCampaign->isSuccess==true){
                    $isGet = true;
                    break;
                }
            }
        }

        // if not get any campaign
        if ($isGet==false){
            //$errorMsg = $campaignData->campaignName.' = '.$campaignData->errorMsg;
            $errorMsg = $campaignData->errorMsg;
            $response->errorMsg = $errorMsg;
            return $response;
        }
        $result = array();
        $response->totalPrice = $campaignData->totalPrice;
        $response->totalDiscount = $campaignData->totalDisc;
        $response->campaignName = $campaignData->campaignName;
        $response->campaignDescription = $campaignData->campaignDescription;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Book Promo
     * @param string $phone
     * @param string $promoType
     * @param string $invoiceId
     * @param string $timeLimit
     * @param array $param
     * @return \stdClass
     */
    public static function bookPromo($phone='',$promoType='potongan',$invoiceId = '', $timeLimit='', $param=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->totalPrice = 0;
        $response->totalDiscount = 0;
        $response->campaignName = null;
        $response->campaignDescription = null;
        $response->timeLimit = null;

        if (empty($phone) || empty($invoiceId)){
            $response->errorMsg = '[Book Process] Empty Phone or Invoice Id';
            return $response;
        }

        $param['cust_phone'] = $phone;
        $param['user_phone'] = $phone;
        $issetPromoCode = 0;
        if (!empty($param['promo_code'])) $issetPromoCode = 1;

        // get all active campaign, in date start / end, order by priority and date created
        $activeCampaign = Campaign::getActiveCampaign($issetPromoCode,$promoType);
        if (count($activeCampaign)==0){
            $response->errorMsg = '[Book Process] Campaign Not Available';
            return $response;
        }
        $isGet = false;
        $campaignData = null;
        // check only promo code
        if ($issetPromoCode==1){
            // check promo code on table
            $voucherDb = Campaign::getVoucherActive($param['promo_code'],$promoType);
            if (count($voucherDb)>0){
                foreach ($voucherDb as $campaign) {
                    $checkCampaign = Campaign::checkRuleCampaign($campaign->campaign_id,$param);
                    $campaignData = $checkCampaign;
                    if ($checkCampaign->isSuccess==true){
                        $isGet = true;
                        break;
                    }
                }
            } else {
                $errorMsg = '[Book Process] Promo Code Not Found';
                $response->errorMsg = $errorMsg;
                return $response;
            }
        } else {
            // check rule foreach campaign
            foreach ($activeCampaign as $campaign) {
                $checkCampaign = Campaign::checkRuleCampaign($campaign->id,$param);
                $campaignData = $checkCampaign;
                if ($checkCampaign->isSuccess==true){
                    $isGet = true;
                    break;
                }
            }
        }

        // if not get any campaign
        if ($isGet==false){
            $errorMsg = $campaignData->campaignName.' = '.$campaignData->errorMsg;
            $response->errorMsg = $errorMsg;
            return $response;
        }

        // book campaign
        $bookCampaign = Campaign::bookCampaignUsage($campaignData,$invoiceId,$phone,$timeLimit);
        if (!$bookCampaign->isSuccess){
            $response->errorMsg = '[Book Process] Failed to Book Promo';
            return $response;
        }

        $response->totalPrice = $campaignData->totalPrice;
        $response->totalDiscount = $campaignData->totalDisc;
        $response->campaignName  = $campaignData->campaignName;
        $response->campaignDescription = $campaignData->campaignDescription;
        $response->timeLimit = $bookCampaign->timeLimit;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Claim Promo
     * @param $phone
     * @param $transactionId
     * @return \stdClass
     */
    public static function claimPromo($phone,$transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $claimCampaign = Campaign::claimCampaignUsage($transactionId,$phone);
        if ($claimCampaign->isSuccess == false){
            $response->errorMsg = $claimCampaign->errorMsg;
            return $response;
        }
        $result = array();
        $response->isSuccess = true;
        return $response;
    }
}

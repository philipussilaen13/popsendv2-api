<?php

namespace App\Http\Models;

use App\Http\Library\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PasswordReset extends Model
{
    /**
     * Insert reset passwrd Code
     * @param $code
     * @param $user
     * @return \stdClass
     */
    public static function insertCode ($code, $user)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->code = null;
        $response->urlCode = null;

        $userId = $user->id;
        $userEmail = $user->email;
        $userPhone = $user->phone;

        $nowDateTime = date('Y-m-d H:i:s');
        // check if have valid and active
        $check = self::where('user_id',$userId)->where('status',0)->where('valid_time','>',$nowDateTime)->first();
        $timeLimit = date('Y-m-d H:i:s',strtotime("+150 minutes"));
        if ($check){
            $check = self::find($check->id);
            $check->valid_time = $timeLimit;
            $check->save();

            $response->isSuccess = true;
            $response->code = $check->code;
            $response->urlCode = $check->url_code;
            return $response;
        }

        // change status to 2 = expired
        $update = self::where('user_id',$userId)->where('status',0)
            ->update(['status'=>2]);

        // create generate url code
        $urlCode = Helper::generateRandomString(15,'AlphaNumMix');

        $reset = new self();
        $reset->user_id = $user->id;
        $reset->email = $user->email;
        $reset->phone = $user->phone;
        $reset->code = $code;
        $reset->url_code = $urlCode;
        $reset->status = 0;
        $reset->valid_time = $timeLimit;
        $reset->save();

        $response->isSuccess = true;
        $response->code = $code;
        $response->urlCode = $urlCode;

        return $response;
    }

    public static function checkCode ($code)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        //Check Valid Code
        $validCode = self::where('code','=',$code)->first();
        if(!$validCode){
            //send old code
            $response->isSuccess = false;
            $response->errorMsg = 'Code Not Valid';
            return $response;
        }

        //Check Valid Time
        $validTime = $validCode->valid_time;
        $nowDateTime = date('Y-m-d H:i:s');
        if ( $nowDateTime > $validCode || $validCode->status==2) {
            $response->isSuccess = false;
            $response->errorMsg = 'Code Expired';
            return $response;
        }

        $response->isSuccess = true;
        $response->user_id = $validCode->user_id;
        return $response;
    }
}

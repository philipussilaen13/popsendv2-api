<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class MemberReferral extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_member_referral';
    protected $primaryKey = 'id_member_referral';

    public function getMemberReferral($idMember){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->referralCode = null;

        $referralDb = self::where('id_member',$idMember)
            ->where('id_product','2')
            ->first();

        if (!$referralDb){
            $response->errorMsg = 'Referral Not Found';
            return $response;
        }


        $response->isSuccess = true;
        $response->referralCode = $referralDb->referral_code;
        return $response;

    }
}

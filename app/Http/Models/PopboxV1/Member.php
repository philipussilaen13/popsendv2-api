<?php

namespace App\Http\Models\PopboxV1;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Member extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_member';
    protected $primaryKey = 'id_member';
    public $timestamps = false;

    /**
     * Check Member Table
     * @param $field
     * @param $input
     * @param null $stringPassword
     * @return \stdClass
     */
    public function checkMember($field,$input,$stringPassword=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->memberId = null;

        $memberDb = self::where($field,$input)->first();
        if (!$memberDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }
        if (!empty($stringPassword)){
            // check on Password
            $hashedPassword = hash('sha256',$stringPassword);
            if ($hashedPassword != $memberDb->member_password){
                $response->errorMsg = 'Invalid Credential';
                return $response;
            }
        }

        $response->isSuccess = true;
        $response->memberId = $memberDb->id_member;
        return $response;
    }

    /**
     * Migrate Member
     * @param $memberId
     * @param $stringPassword
     * @return \stdClass
     */
    public function migrateMember($memberId, $stringPassword){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->userId = null;

        $memberDb = self::find($memberId);
        if (!$memberDb){
            $response->errorMsg = "Invalid Member";
            return $response;
        }
        $phone = $memberDb->phone;

        // get last balance
        $memberBalanceModel = new MemberBalance();
        $getLastBalance = $memberBalanceModel->getMemberBalance($phone);
        if (!$getLastBalance->isSuccess){
            $response->errorMsg = $getLastBalance->errorMsg;
            return $response;
        }
        $lastBalance = $getLastBalance->lastBalance;
        $country = $getLastBalance->country;
        $name = $memberDb->member_name;
        $email = $memberDb->email;

        // get member referral
        $referralCode = null;
        $memberReferralModel = new MemberReferral();
        $getReferral = $memberReferralModel->getMemberReferral($memberId);
        if ($getReferral){
            $referralCode = $getReferral->referralCode;
        }

        // insert into user
        $userDb = User::insertNewUser($name,$phone,$email,$stringPassword,$country,$lastBalance,$referralCode);
        if (!$userDb->isSuccess){
            $response->errorMsg = $userDb->errorMsg;
            return $response;
        }

        // update flag migration
        $this->updateFlags($memberId);

        $response->userId = $userDb->id;
        $response->memberId = $userDb->memberId;
        $response->isSuccess = true;
        return $response;
    }

    public function updateFlags ($member_id)
    {
        $update = self::where('id_member', $member_id)
            ->update([
                'flag_migration' => 1
            ]);
    }
}
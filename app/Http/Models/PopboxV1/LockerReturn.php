<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class LockerReturn extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_activities_return';

    public function getList($phone,$orderNumber=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $listData = [];

        $lockerReturnDb = self::where('phone_number',$phone)->get();

        foreach ($lockerReturnDb as $item) {
            if (in_array($item->tracking_no,$orderNumber)) continue;
            $id = $item->id;
            $invoiceId = $item->tracking_no;
            $lockerName = $item->locker_name;
            $status = 'ON PROCESS';
            $lastUpdate = $item->storetime;
            if ($item->status =='COMPLETED'){
                $lastUpdate = $item->taketime;
                $status = 'COMPLETED';
            }

            $tmp = new \stdClass();
            $tmp->id = $id;
            $tmp->order_number = $invoiceId;
            $tmp->type = 'return';
            $tmp->merchant = $item->merchant_name;
            $tmp->locker = $lockerName;
            $tmp->status = $status;
            $tmp->last_update = $lastUpdate;
            $tmp->pin = null;

            $listData[] = $tmp;
        }

        $response->isSuccess = true;
        $response->data = $listData;

        return $response;
    }
}

<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;

class PopSendTariff extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_popsend_tarif_kec';
    public $timestamps = false;
}

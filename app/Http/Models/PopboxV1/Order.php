<?php

namespace App\Http\Models\PopboxV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_member_pickup';

    /**
     * Get Member PopShop Order
     * @param $phone
     * @return \stdClass
     */
    public function getMemberOrder($phone){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $orderDb = self::where('phone',$phone)->get();

        $orderList = [];
        foreach ($orderDb as $order) {
            $id = $order->invoice_id;
            $invoiceId = $order->invoice_id;
            $lockerName = null;
            $status = null;
            $lastUpdate = null;
            $pin = null;

            if ($order->deliver_status==0){
                $status = 'ON PROCESS';
                $lastUpdate = $order->completed_date;
            }
            elseif ($order->deliver_status ==1){
                // query to locker activities
                $lockerActivitiesDb = DB::connection('popbox_db')->table('locker_activities')
                    ->where('barcode',$invoiceId)
                    ->first();
                if ($lockerActivitiesDb){
                    if ($lockerActivitiesDb->status == 'IN_STORE'){
                        $pin = $lockerActivitiesDb->validatecode;
                        $status = 'READY FOR PICKUP';
                    } elseif ($lockerActivitiesDb->status == 'CUSTOMER_TAKEN') {
                        $status = 'COMPLETED';
                        $lastUpdate = $order->deliver_date;
                    }
                }
            }

            if ($order->deliver_status==0){
                $status = 'ON PROCESS';
                $lastUpdate = $order->completed_date;
            }
            elseif ($order->deliver_status ==1){
                // query to locker activities
                $lockerActivitiesDb = DB::connection('popbox_db')->table('locker_activities')
                    ->where('barcode',$invoiceId)
                    ->first();
                if ($lockerActivitiesDb){
                    if ($lockerActivitiesDb->status == 'IN_STORE'){
                        $pin = $lockerActivitiesDb->validatecode;
                        $status = 'READY FOR PICKUP';
                    } elseif ($lockerActivitiesDb->status == 'CUSTOMER_TAKEN') {
                        $status = 'COMPLETED';
                        $lastUpdate = $order->deliver_date;
                    }
                }
            }

            // check on locker location for locker name
            $lockerLocationDb = DB::connection('popbox_db')->table('locker_locations')
                ->where('name',$order->address)
                ->first();
            if ($lockerLocationDb){
                $lockerName = $lockerLocationDb->name;
            }

            $tmp = new \stdClass();
            $tmp->id = (string)$id;
            $tmp->order_number = $invoiceId;
            $tmp->type = 'popshop';
            $tmp->merchant = "PopShop";
            $tmp->locker = $lockerName;
            $tmp->status = $status;
            $tmp->last_update = $lastUpdate;
            $tmp->pin = $pin;

            $orderList[] = $tmp;
        }

        $response->isSuccess = true;
        $response->data = $orderList;
        return $response;
    }
}
